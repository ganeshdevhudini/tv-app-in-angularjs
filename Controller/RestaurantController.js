app.controller("RestaurantController", [
    "restaurantService",
    "$location",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
      restaurantService,
      $location,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.showDetail = false;
      $scope.imageSize = configService.ImageSize;
      $scope.imgURL = configService.imageUrl;
      $scope.restaurant = [];
     // $scope.offersTypes = ['All'];
      $scope.randomId = '';
      
    
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.randomId = 's' + $scope.getRandomInt(0, 100000);
        sessionStorage.setItem('offerDetail', JSON.stringify(''));
        $scope.getrestaurantInfo();
      };
  
          $scope.getrestaurantInfo = function () {
        // $scope.allOffers = [];
        // $scope.offersTypes = ['All'];
        $scope.restaurant = [];
  
        $('.loader-wrapper').show();
        restaurantService.getrestaurantInfo().then(function success(responseData) {
            $('.loader-wrapper').hide();
            var res = responseData.data.ResponseData.RestauarantBar;
              $scope.restaurant = res;
            console.log($scope.restaurant);
               
             if(JSON.parse(sessionStorage.getItem('restId'))!==null )
               {
                 setTimeout(function () {
             //       // var item = $('.focusable-offer').eq(0);
             //       // focusController.focus(item);
                    focusController.focus($('#'+JSON.parse(sessionStorage.getItem('restId'))))
                }, 200);
              
               }
                else{
                 setTimeout(function(){
                   var item = $('.focusable-restaurant').eq(0);
                     focusController.focus(item);
                 },200)
            }
         
              }, function error(response) {
                $('.loader-wrapper').hide();
                  console.log(response);
                  $location.path('/');
                  // chromeGlobalService.focus = true;
                  setTimeout(function () {
                      debugger;
                      // $location.path('/');
  
                      // window.location.reload()
  
                      location.reload();
                      // focusController.setDepth(0, "home");
                      // focusController.focus('hometile0')
                  }, 200);
              });
      };
  
      $scope.offerFocused = function (event) {
        var parent = $('.cityguide-list');
        var child = $(event.currentTarget);
        $scope.scrollHorizontal(parent, child);
      };
  
      $scope.restaurantSelected = function (event) {
        var actualChild = $(event.currentTarget);
        var id = $(actualChild).attr('id');
        sessionStorage.setItem('restDetail', JSON.stringify($scope.restaurant.filter(function(i) {return i.Id === id} )));
        sessionStorage.setItem('restId', "\""+id+"\"");
        $location.path('/restaurantDetails');
      };
      
        focusController.addAfterKeydownHandler(function (context, controller) {
          context.event.stopImmediatePropagation();
          var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='restaurant')
          {
          if (context.event.keyCode) {
            var currentFocusableGroup = controller.getCurrentGroup();
            var currenturl = window.location.href;
            currenturl = currenturl.split("#/").pop();
            if (currenturl=="restaurant" && (currentFocusableGroup == "restaurant" || currentFocusableGroup == "menu-item")) {
              switch (context.event.keyCode) {
                
               
                  case 461:
                    
                      
                       
                       
                       
                         $location.path('/');
    
                      
                    
                 
                  break;
                  case 9:
                    
                       
                       
                       
                         $location.path('/');
    
                     
                  break;
                default:
                  // console.log('Key code : ' + context.event.keyCode);
                  break;
              }
            }
          }
        }
        });
      
      
    
      $scope.offerlifocus=function(event)
      {
        if($('.focusable-cityguide').hasClass('focused'))
        {
          $(".cityguide-list").scrollCenter(".focused", 700);
        }
      }
      // $scope.offerTypeSelected = function (event) {
      //   $('.offerType').removeClass('selected');
      //   $(event.currentTarget).addClass('selected');
      //   var actualChild = $(event.currentTarget);
      //   var id = $(actualChild).attr('id');
      //   $scope.offers = [];
      //   if (id == 'All') {
      //     $scope.showAllOffers();
      //   } else {
      //     $scope.offers = $scope.allOffers.find((i) => i.Name === id).Details;
      //   }
  
      //   // $scope.$apply();
  
      //   setTimeout(function () {
      //     var item = $('.focusable-offer').eq(0);
      //     focusController.focus(item);
      //   }, 20);
      // };
  
      $scope.scrollHorizontal = function (parent, child) {
        parent.animate(
          { scrollLeft: $(child).index() * $(child).width() - $(child).width() },
          500
        );
        return false;
      };
  
      $scope.scroll = function () {
        var outerContent = $('.offer-scroll-list');
        var innerContent = $('.offer3');
        outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);
      };
  
      $scope.showAllOffers = function () {
        $scope.allOffers.forEach(function(element)  {
          element.Details.forEach(function(item) {
            $scope.offers.push(item);
          });
        });
      };
  
      $scope.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
      };
  
          $scope.init();
    },
  ]);
  
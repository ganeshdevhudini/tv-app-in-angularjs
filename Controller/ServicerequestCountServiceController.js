app.controller("ServicerequestCountServiceController", [
    "servicerequestService",
    "$location",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
    servicerequestService,
      $location,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.imgURL = configService.imageUrl;
      $scope.serviceQty = 1;
      $rootScope.authLocalStorageToken = configService.appVersion + '-' + configService.USERDATA_KEY;
    
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.randomId = $scope.getRandomInt(1, 1000);
        $scope.details = $scope.housekeepingService.housekeepingDetail;
        if (!$scope.details) {
          $location.path('/serviceRequest');
        }
        $scope.getOffers();
      };

      $scope.minusButtonSelected = function (event) {
        if ($scope.serviceQty < 1) {
          $scope.serviceQty = 0;
        } else {
          $scope.serviceQty -= 1;
        }
      };

      $scope.plusButtonSelected = function (event) {
        $scope.serviceQty += 1;
      };

      $scope.cancelButtonSelected = function (event) {
        $location.path('/serviceRequest');
      };

      $scope.submitButtonSelected = function (event) {
        var input = {
          RoomNo: data.RoomNumber,
          BookingId: '',
          AdditionalRequest: '',
          Services: {
            ServiceId: '',
            Quantity: '',
            AdditionalNote: '',
            AvailTime: '',
          }
        };


      };

      $scope.minusButtonSelected = function (event) {
        
      };

      $scope.minusButtonSelected = function (event) {
        
      };



      $scope.init();
    },
  ]);
  
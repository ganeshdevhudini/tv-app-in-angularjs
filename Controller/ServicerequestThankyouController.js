app.controller("ServicerequestController", [
    "servicerequestService",
    "$location",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
    servicerequestService,
      $location,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.nextItem = '';
      $rootScope.authLocalStorageToken = configService.appVersion + '-' + configService.USERDATA_KEY;
    
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.gethousekeepinginfo();
      };

      $scope.gethousekeepinginfo = function () {
        var data = JSON.parse(
          sessionStorage.getItem($rootScope.authLocalStorageToken)
        );

        if(!data) {
          return false;
        }
        
        var input = {
          ImageSize: configService.ImageSize,
          RoomNo: data.RoomNumber,
          BookingId: data.GuestInfo.ReservationNo,
        };

        $('.loader-wrapper').show();
        servicerequestService.getServiceInfo(input).then(function success(responseData) {
          $('.loader-wrapper').hide();
          var res = responseData.data.ResponseData;
          $scope.facilityTypes = res;
          if (res && res[0]) {
            $scope.serviceTypes = res[0].RoomServices;
            if( $scope.facilityTypes.length>1){
              $scope.nextItem=res[1].FacilityId;
            }
            else{
              $scope.nextItem=res[0].FacilityId;
            }
            setTimeout(() => {
              $('.facility-type').eq(0).addClass('selected');
              if ($('.facility-button').length > 0) {
                focusController.focus($('.facility-button').eq(0));
              } else {
               focusController.focus($('.facility-type').eq(0));
              }
            }, 500);

          }
        }, function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
        });
      }

      $scope.facilityTypeSelected = function (event) {
        $('.facility-type').removeClass('selected');
        $(event.currentTarget).addClass('selected');
        var facilityId = $(event.currentTarget).attr('id');
        var x = $scope.facilityTypes.findIndex((i) => i.FacilityId === facilityId);
        $scope.serviceTypes = $scope.facilityTypes[x].RoomServices;
          if(x==$scope.facilityTypes.length - 1){
            $scope.nextItem=$scope.facilityTypes[0].FacilityId;
          }
          else{
            $scope.nextItem=$scope.facilityTypes[x+1].FacilityId;
          }
      };

      $scope.facilityButtonFocused = function (event) {
        var parent = $('.facility-scroll-list');
        var child = $('.card-facility-' + $(event.currentTarget).attr('id'));
        $scope.scroll(parent, child);
        setTimeout(() => {
          anime({
            targets: '.' + $(child).attr('class'),
            translateY: -20,
          });
        }, 450);

        var actualParent = $('.facility-name-list');
        var actualChild = $(event.currentTarget).parent();
        $scope.scroll(actualParent, actualChild);
      };

      $scope.facilityButtonBlurred = function (event) {
        var child = $('.card-facility-' + $(event.currentTarget).attr('id'));
          setTimeout(() => {
            anime({
              targets: '.' + $(child).attr('class'),
              translateY: 0,
            });
          }, 500);
      };

      $scope.facilityButtonSelected = function (event) {
        var serviceId = $(event.currentTarget).attr('id');
          var service = $scope.serviceTypes.find((i) => i.ServiceId === serviceId);
          if (service) {
            var serviceType = service.ServiceType;
            $scope.housekeepingService.housekeepingDetail = service;
            // if (serviceType == 'COUNT') {
            //   $scope._ngZone.run(() => {
            //     $scope._router.navigate(['housekeeping/countservice']);
            //   });
            // } else if (serviceType == 'PICK UP' || serviceType == 'REQUEST') {
            //   $scope._ngZone.run(() => {
            //     $scope._router.navigate(['housekeeping/timeservice']);
            //   });
            // }
          }
      };

      $scope.scroll = function (parent, child) {
        parent.animate(
          { scrollLeft: $(child).index() * $(child).width() - $(child).width() },
          500
        );
        return false;
      };


    
      $scope.init();
    },
  ]);
  
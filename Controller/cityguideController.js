app.controller("cityguideController", [
    "cityguideService",
    "$location",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
      cityguideService,
      $location,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.showDetail = false;
      $scope.imageSize = configService.ImageSize;
      $scope.imgURL = configService.imageUrl;
      $scope.cityguide = [];
      $scope.offersTypes = ['All'];
      $scope.randomId = '';
      
    
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.randomId = 's' + $scope.getRandomInt(0, 100000);
        sessionStorage.setItem('offerDetail', JSON.stringify(''));
        $scope.getcityguideInfo();
      };
  
          $scope.getcityguideInfo = function () {
        // $scope.allOffers = [];
        // $scope.offersTypes = ['All'];
        $scope.allcityguide = [];

        $('.loader-wrapper').show();
  
        cityguideService.getcityguideInfo().then(function success(responseData) {
            $('.loader-wrapper').hide();
            debugger;
            var res = responseData.data.ResponseData;
            for(i=0;i<res.length;i++)
            {
              $scope.allcityguide = $scope.allcityguide.concat(res[i].Details);
            }
             
            
               
             if(JSON.parse(sessionStorage.getItem('cityId'))!==null )
              {
                setTimeout(function () {
            //       // var item = $('.focusable-offer').eq(0);
            //       // focusController.focus(item);
                   focusController.focus($('#'+JSON.parse(sessionStorage.getItem('cityId'))))
                }, 200);
              
               }
               else{
                setTimeout(function(){
                  var item = $('.focusable-cityguide').eq(0);
                    focusController.focus(item);
                },200)
           }
         
              }, function error(response) {
                  $('.loader-wrapper').hide();
                  console.log(response);
                  $location.path('/');
                  // chromeGlobalService.focus = true;
                  setTimeout(function () {
                      debugger;
                      // $location.path('/');
  
                      // window.location.reload()
  
                      location.reload();
                      // focusController.setDepth(0, "home");
                      // focusController.focus('hometile0')
                  }, 200);
              });
      };
  
      $scope.offerFocused = function (event) {
        var parent = $('.offer-list-wrapper');
        var child = $(event.currentTarget);
        $scope.scrollHorizontal(parent, child);
      };
  
      $scope.cityguideSelected = function (event) {     
        debugger;   
        var actualChild = $(event.currentTarget);
        var id = $(actualChild).attr('id');
        sessionStorage.setItem('CityDetail', JSON.stringify($scope.allcityguide.filter(function(i) {return i.Id === id } )));
        sessionStorage.setItem('cityId', "\""+id+"\"");
        $location.path('/cityguideDetails');
      };
      
        focusController.addAfterKeydownHandler(function (context, controller) {          context.event.stopImmediatePropagation();
          var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='cityguide')
          {
          if (context.event.keyCode) {
            var currentFocusableGroup = controller.getCurrentGroup();
            var currenturl = window.location.href;
            currenturl = currenturl.split("#/").pop();
            if (currenturl=="cityguide" && (currentFocusableGroup == "cityguide" || currentFocusableGroup == "menu-item")) {
              switch (context.event.keyCode) {
                
               
                  case 461:
                    
                      
                       
                       
                       
                         $location.path('/');
    
                      
                    
                 
                  break;
                  case 9:
                    
                       
                       
                       
                         $location.path('/');
    
                     
                  break;
                default:
                  // console.log('Key code : ' + context.event.keyCode);
                  break;
              }
            }
          }
        }
        });
      
      
    
      $scope.offerlifocus=function(event)
      {  
             
         if($('.focusable-cityguide').hasClass('focused'))
        {
          $(".cityguide-list").scrollCenter(".focused", 200);
        }
      }
      $scope.offerTypeSelected = function (event) {
        $('.offerType').removeClass('selected');
        $(event.currentTarget).addClass('selected');
        var actualChild = $(event.currentTarget);
        var id = $(actualChild).attr('id');
        $scope.offers = [];
        if (id == 'All') {
          $scope.showAllOffers();
        } else {
          $scope.offers = $scope.allOffers.filter(function(i) {return i.Name === id} ).Details;
        }
  
        // $scope.$apply();
  
        setTimeout(function () {
          var item = $('.focusable-offer').eq(0);
          focusController.focus(item);
        }, 20);
      };
  
      $scope.scrollHorizontal = function (parent, child) {
        parent.animate(
          { scrollLeft: $(child).index() * $(child).width() },
          500
        );
        return false;
      };
  
      $scope.scroll = function () {
        var outerContent = $('.offer-scroll-list');
        var innerContent = $('.offer3');
        outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);
      };
  
      $scope.showAllOffers = function () {
        $scope.allOffers.forEach(function(element)  {
          element.Details.forEach(function(item)  {
            $scope.offers.push(item);
          });
        });
      };
  
      $scope.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
      };
  
          $scope.init();
    },
  ]);
  
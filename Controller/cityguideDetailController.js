app.controller("cityguideDetailsController", [
    "cityguideService",
    "$location",
    "$filter",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
      cityguideService,
      $location,
      $filter,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.imgURL = configService.imageUrl;
      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
      $scope.citybgIndex=0;
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.setData();
        setTimeout(function () {
          var item = $("#backbutton");
          focusController.focus(item);
        }, 20);
      };
  
      $scope.convertToDate = function (datestring, isYear) {
        var date = new Date(datestring);
        var result;
        if (isYear) {
          // result = $scope.datepipe.transform(date, 'dd MMM YYYY');
          result = $filter("date")(date, "dd MMM YYYY");
        } else {
          // result = $scope.datepipe.transform(date, 'dd MMM');
          result = $filter("date")(date, "dd MMM");
        }
        return result;
      };
  
      $scope.setData = function () {        
        debugger;
        
        var cityguideDetail = JSON.parse(sessionStorage.getItem('CityDetail'));
  
        if (cityguideDetail && cityguideDetail != '') {
          $scope.citydata = cityguideDetail;
          $scope.data= $scope.citydata;
          console.log( $scope.data);
          if($scope.data[0].Images.length>0)
          {
            $scope.image =
            $scope.imgURL +
            "/Image/Cityguide/" +
            $scope.data[0].Images[0].ImageName;
          }
          else{
            $scope.image="";
          }
         
          setTimeout(function(){
              focusController.focus($('#citybackbutton'))
          },100)
        }
        else {
          $location.path('/cityguide');
        }
      };
      
    focusController.addAfterKeydownHandler(function (context, controller) {        context.event.stopImmediatePropagation();
        var currenturl = window.location.href;
        currenturl = currenturl.split("#/").pop();
            if(currenturl=='cityguideDetails')
            {
              if (context.event.keyCode) {
                var currentFocusableGroup = controller.getCurrentGroup();
                var currenturl = window.location.href;
                currenturl = currenturl.split("#/").pop();
                 if (currenturl == "cityguideDetails" ) {
                  switch (context.event.keyCode) {
                    
                   
                      case 461:
                        
                          
                           
                           
                           
                             $location.path('/cityguide');
        
                          
                        
                     
                      break;
                      case 9:
                        
                           
                           
                           
                             $location.path('/cityguide');
        
                         
                      break;
                    default:
                      // console.log('Key code : ' + context.event.keyCode);
                      break;
                  }
                }
              }
            }
        
      });
     
      $scope.rightArrowSelected = function () {
        debugger;
        var length = $scope.citydata.length;
        if ($scope.citybgIndex >= length - 1) {
          $scope.citybgIndex = 0;
        } else {
          $scope.citybgIndex += 1;
        }
        $scope.data = $scope.citydata[$scope.citybgIndex];
        if($scope.data.Images.length>0)
        {
          if (($scope.image = $scope.data.Images[0])) {
            $scope.image =
              $scope.imgURL +
              "/Image/Cityguide/" +
              $scope.data.Images[0].ImageName;
             // $('#citytitle').html($scope.data[$scope.citybgIndex].Title);
              //$('#citydesc').html($scope.data[$scope.citybgIndex].Overview);
          } else {
            $scope.image = "";
          }
        
        }
        else{
          $scope.image = "";
        }
       // $scope.$apply();
       // $scope.scrollBackground($scope.citybgIndex);
      };

      $scope.leftArrowSelected = function () {
        var length = $scope.citydata.length;
        if ($scope.citybgIndex <= 0) {
          $scope.citybgIndex = length - 1;
        } else {
          $scope.citybgIndex -= 1;
        }
        $scope.data = $scope.citydata[$scope.citybgIndex];
        //$scope.info = $scope.subAmenities[$scope.bgIndex];
        if($scope.data.Images.length>0)
        {
          if (($scope.image = $scope.data.Images[0])) {
            $scope.image =
              $scope.imgURL +
              "/Image/Cityguide/" +
              $scope.data.Images[0].ImageName;
              // $('#citytitle').html($scope.data[$scope.citybgIndex].Title);
              // $('#citydesc').html($scope.data[$scope.citybgIndex].Overview);
          } else {
            $scope.image = "";
          }
        }
    
        // $scope.$apply();
        // $scope.scrollBackground($scope.citybgIndex);
      };
      $scope.backButtonClicked = function () {        $location.path('/cityguide');
      
      };
    
      focusController.addAfterKeydownHandler(function (context, controller) {        context.event.stopImmediatePropagation();
        var currenturl = window.location.href;
        currenturl = currenturl.split("#/").pop();
            if(currenturl=='offerDetails')
            {
              if (context.event.keyCode) {
                var currentFocusableGroup = controller.getCurrentGroup();
                var currenturl = window.location.href;
                currenturl = currenturl.split("#/").pop();
                 if (currenturl == "offerDetails" ) {
                  switch (context.event.keyCode) {
                    
                   
                      case 461:
                        
                          
                           
                           
                           
                             $location.path('/offers');
        
                          
                        
                     
                      break;
                      case 9:
                        
                           
                           
                           
                             $location.path('/offers');
        
                         
                      break;
                    default:
                      // console.log('Key code : ' + context.event.keyCode);
                      break;
                  }
                }
              }
            }
        
      });
     
      
    
      $scope.init();
    },
  ]);
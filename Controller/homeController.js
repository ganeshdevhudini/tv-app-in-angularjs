app.controller("homeController", [
  "GlobalService",
  "offersService",
  "$route",
  "$rootScope",
  "$scope",
  "$location",
  "$timeout",
  "focusController",
  "configService",
  "$translate",
  function (
    GlobalService,
    offersService,
    $route,
    $rootScope,
    $scope,
    $location,
    $timeout,
    focusController,
    configService,
    $translate
  ) {
    $scope.time = Date.now();
    $scope.imgURL = configService.imageUrl;
    $rootScope.authToken = configService.AuthToken;
    $rootScope.authLocalStorageToken = configService.appVersion + '-' + configService.USERDATA_KEY;
    $scope.isQRCodeVisible = false;
    var currenturl = window.location.href;
     $rootScope.isNavMenuRequired = false;
    currenturl = currenturl.split("#/").pop();
    $scope.init = function () { 
      debugger;    
       if (sessionStorage.getItem($rootScope.authToken)) {
        $rootScope.isNavMenuRequired = true;
        
      
        $scope.randomId = "s" + $scope.getRandomInt(0, 10000);
        $scope.checkQRCodeVisible();
        $scope.getOffersLanding();
        $rootScope.$emit("stopCurrentMediaIPTV", {});
        // alert('session started !');
      } else {
        $location.path("/landing");
        setTimeout(function () {
          $route.reload();
        }, 20);
      }
    };

    $scope.checkQRCodeVisible = function () {
      var data = JSON.parse(
        sessionStorage.getItem($scope.authLocalStorageToken)
      );

      if (data && data.IsAppQRVisible) {
        $scope.isQRCodeVisible = data.IsAppQRVisible;
      }
    };
   
    $scope.gotoOffers = function () {
      $(".menu-button").removeClass("selected");
      $(".menu-button").removeClass("focused");
      $('#tile4').addClass("selected");
      $(".icon").css("filter", "invert(0)");
      $(".left-menu").removeClass("expanded");
      $(".default-hide").hide(150);
      setTimeout(function () {
        $location.path("/offers");
        $scope.$apply();
        setTimeout(function(){
          focusController.focus($('#All'));
        },200)
      }, 20);
    };
    
    
    focusController.addAfterKeydownHandler(function (context, controller) {      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
      context.event.stopImmediatePropagation();
      if(currenturl=='')
   {
    if (context.event.keyCode) {
      var currentFocusableGroup = controller.getCurrentGroup();
     
       if (currenturl == "" ) {
        switch (context.event.keyCode) {
          
         
            case 461:
              
                
                 
                 
                 
                   $location.path('/');

                
              
           
            break;
            case 9:
              
                 
                 
                 
                   $location.path('/');

               
            break;
          default:
            // console.log('Key code : ' + context.event.keyCode);
            break;
        }
      }
    }
   }
    
    });
 
   
$scope.changestandby=function()
{
    hcap.property.getProperty({
        "key" : "instant_power",
        "onSuccess" : function(s) {
            //log("instant_power = " + s.value);
            $('#stdby').html(s.value);
            if(s.value == 0)
                hcap.property.getProperty({
                    "key" : "wol_m",
                    "onSuccess" : function(s) {
                       // log("wol_m = " + s.value);
                        $('#wol').html(s.value);
                    }, 
                    "onFailure" : function(f) {
                       // log("onFailure : errorMessage = " + f.errorMessage);
                        $('#wol').html("wolonFailure : errorMessage = " + f.errorMessage)
                    }
                });
            else
               // log("WOL is disalbed because Instant mode is on");
                $('#wol').html("WOL is disalbed because Instant mode is on");
            
        }, 
        "onFailure" : function(f) {
            //log("onFailure : errorMessage = " + f.errorMessage);
            $('#stdby').html("stdbyonFailure : errorMessage = " + f.errorMessage)
        }
   });

    hcap.power.getPowerMode({
        "onSuccess" : function(s) {
           // log("power mode " + s.mode);
            $('#power').html(s.mode)
        }, 
        "onFailure" : function(f) {
           // log("onFailure : errorMessage = " + f.errorMessage);
            $('#power').html("onFailure : errorMessage = " + f.errorMessage)
        }
   });

}

    // Show_current_status();
    //  setInterval(function(){
    //   Show_current_status();
    //  },1000)

    $scope.getOffersLanding = function () {    
      
      debugger;
      $('.loader-wrapper').show();
      offersService.getOffers().then(
        function success(responseData) {
          debugger;
          $('.loader-wrapper').hide();
          var res = responseData.data.ResponseData;
          $scope.offers = res;
          console.log($scope.offers);
          var currenturl = window.location.href;
          currenturl = currenturl.split("#/").pop();
          if(currenturl=="")
          {
            setTimeout(function () {
              focusController.setDepth(0, "menu-item");
              focusController.focus("menu-item-0");
            }, 20);
          }
         

      
          // setTimeout(function () {
          //   focusController.focus("offer-item-" + $scope.randomId + '0');
          // }, 500);
          // setTimeout(function(){

          //   $('.left-menu').removeClass('expand');
          //   $scope.$apply();  
          // },200)
        },
        function error(response) {
          debugger;
          $('.loader-wrapper').hide();
          console.log(response);
        }
      );
    };

    $scope.onOfferFocused = function (event) {
      var parent = $(".home-scroll-section");
      var child = $(event.currentTarget);
      $scope.scrollHorizontal(parent, child);
    };

    $scope.onOfferSelected = function () {
      $location.path("/offers");
    };

    $scope.scrollHorizontal = function (parent, child) {
      parent.animate(
        { scrollLeft: $(child).index() * $(child).width() - $(child).width() },
        500
      );
      return false;
    };

    $scope.getRandomInt = function (min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    };

    $scope.init();
  },
]);

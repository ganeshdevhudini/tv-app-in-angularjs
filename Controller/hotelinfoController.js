﻿//app.controller("hotelinfoController", ["$location", "$scope",  "focusController", "hotelinfoService", "GlobalService", "Service", function ($location, $scope, focusController, hotelinfoService,GlobalService, Service) {
app.controller("hotelinfoController", [
  "hotelinfoService",
  "GlobalService",
  "$route",
  "$scope",
  "$location",
  "$timeout",
  "focusController",
  "configService",
  "$translate",
  "$http",
  "$rootScope",
  "$route",
  function (
    hotelinfoService,
    GlobalService,
    $route,
    $scope,
    $location,
    $timeout,
    focusController,
    configService,
    $translate,
    $http,
    $rootScope,
    $route
  ) {
    $scope.imgURL = configService.imageUrl;
    $scope.showHotelInfo = true;
    $scope.showRoomInfo = false;
    $scope.showMeetingInfo = false;
    $scope.roomInfoIndex = 0;
    $scope.bgIndex = 0;
    
    $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.randomId='s'+$scope.getRandomInt(0,1000);
        $scope.getHotelInfo();
        $scope.getAmenities();
    };

    $scope.getHotelInfo = function () {
      $('.loader-wrapper').show();
      hotelinfoService.getHotelInfo().then(
        function success(responseData) {
          $('.loader-wrapper').hide();
          var res = responseData.data.ResponseData;
          $scope.hotelinfo = res[0].Brands[0].Hotels[0];
          debugger;
          $scope.info = $scope.hotelinfo;
          if ($scope.hotelinfo.Images[0]) {
            $scope.image =
              $scope.imgURL +
              "/Image/Hotel/" +
              $scope.hotelinfo.Images[0].ImageName;
          }
          setTimeout(function(){
            $scope.$apply();
            focusController.focus($('#hotelinfo'));
          },200)
          
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
          $location.path('/');
          // chromeGlobalService.focus = true;
          setTimeout(function () {
              debugger;
              // $location.path('/');

              // window.location.reload()

              location.reload();
              // focusController.setDepth(0, "home");
              // focusController.focus('hometile0')
          }, 200);
        }
      );
    };

    $scope.getAmenities = function () {
      $('.loader-wrapper').show();
      hotelinfoService.getAmenities().then(
        function success(responseData) {
          $('.loader-wrapper').hide();
          var res = responseData.data.ResponseData;
          $scope.amenities = res;
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
          $location.path('/');
          // chromeGlobalService.focus = true;
          setTimeout(function () {
              debugger;
              // $location.path('/');

              // window.location.reload()

              location.reload();
              // focusController.setDepth(0, "home");
              // focusController.focus('hometile0')
          }, 200);
        }
      );
    };

    $scope.leftArrowSelected = function () {
      var length = $scope.subAmenities.length;
      if ($scope.bgIndex - 1 < 0) {
        $scope.bgIndex = length - 1;
      } else {
        $scope.bgIndex -= 1;
      }
      $scope.info = $scope.subAmenities[$scope.bgIndex];
      if (($scope.image = $scope.subAmenities[$scope.bgIndex].Images[0])) {
        $scope.image =  $scope.imgURL + "/Image/Amenities/" +$scope.subAmenities[$scope.bgIndex].Images[0].ImageName;
      } else {
        $scope.image = "";
      }
      $scope.$apply();
      $scope.scrollBackground($scope.bgIndex);
    };

    $scope.rightArrowSelected = function () {
      var length = $scope.subAmenities.length;
      if ($scope.bgIndex >= length - 1) {
        $scope.bgIndex = 0;
      } else {
        $scope.bgIndex += 1;
      }
      $scope.info = $scope.subAmenities[$scope.bgIndex];
      if (($scope.image = $scope.subAmenities[$scope.bgIndex].Images[0])) {
        $scope.image =
          $scope.imgURL +
          "/Image/Amenities/" +
          $scope.subAmenities[$scope.bgIndex].Images[0].ImageName;
      } else {
        $scope.image = "";
      }
      $scope.$apply();
      $scope.scrollBackground($scope.bgIndex);
    };
   
    focusController.addAfterKeydownHandler(function (context, controller) {
      context.event.stopImmediatePropagation();
      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='hotelinfo')
          {
            if (context.event.keyCode) {
              var currentFocusableGroup = controller.getCurrentGroup();
              var currenturl = window.location.href;
              currenturl = currenturl.split("#/").pop();
               if (currenturl == "hotelinfo" ) {
                switch (context.event.keyCode) {
                  
                 
                    case 461:
                      
                        
                         
                         
                         
                           $location.path('/');
      
                        
                      
                   
                    break;
                    case 9:
                      
                         
                         
                         
                           $location.path('/');
      
                       
                    break;
                  default:
                    // console.log('Key code : ' + context.event.keyCode);
                    break;
                }
              }
            }
          }
    
    });
   
      
    
   
    $scope.facilityButtonSelected = function (event) {
      var name = $(event.currentTarget).attr("id");
      $(".facility-button").removeClass("active");
      $(event.currentTarget).addClass("active");
      if (name == "hotelinfo") {
        $scope.info = $scope.hotelinfo;
        $scope.type = "hotel";
        $scope.image = $scope.imgURL + "/Image/Hotel/" + $scope.hotelinfo.Images[0].ImageName;
        $scope.subAmenities = [];
        $scope.$apply();
        $scope.firsttime = true;
      } else {
        $scope.type = "amenity";
        $scope.bgIndex = 0;
        $scope.subAmenities = [];
        setTimeout(function(){
          $scope.subAmenities = $scope.amenities.filter(function(i) {return  i.Type === name})[0].Amenities;
          $scope.info = $scope.subAmenities[0];
          $scope.image = "";
          if ($scope.subAmenities[0].Images.length > 0) {
            $scope.image = $scope.imgURL + "/Image/Amenities/" + $scope.subAmenities[0].Images[0].ImageName;
          }
          $scope.$apply();
          if ($scope.firsttime) {
            $scope.firsttime = false;
          } else {
            focusController.focus($(".right-arrow")[0]);
          }
        }, 300);
      }
    };

    $scope.scrollBackground = function (index) {
        $('.dot-wrapper').find('.dot').removeClass('active');
        $('.dot-wrapper').find('.dot').eq(index).addClass('active');
        $('.carousel-wrapper').animate(
          { scrollLeft: index * $('.carousel-wrapper .item').eq(0).width() },
          500
        );
        return false;
    };

    $scope.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    };

    $scope.init();
  },
]);

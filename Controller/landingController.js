app.controller("landingController", [
  "landingService",
  "$location",
  "$scope",
  "$rootScope",
  "focusController",
  "GlobalService",
  "configService",
  function (
    landingService,
    $location,
    $scope,
    $rootScope,
    focusController,
    GlobalService,
    configService
  ) {
    $scope.buildNumber = configService.BuildVersion;

    $rootScope.authLocalStorageToken =
      configService.appVersion + "-" + configService.USERDATA_KEY;
    $rootScope.authToken = configService.AuthToken;
    $rootScope.unitTemperature = configService.TemperatureUnit;
    $scope.time = Date.now();
    $scope.register = {};
    $('body').css('visibility','visible');
    $scope.init = function () {
      sessionStorage.clear();
      $rootScope.isNavMenuRequired = false;
      sessionStorage.setItem("macId", JSON.stringify($scope.getRandomId()));
      $scope.getUdid();
      // $scope.getRoomInfo();
      $rootScope.$emit("stopCurrentMediaIPTV", {});
    };
    //   SerialNo="123456";
    // $scope.newUdid=SerialNo
    // SeialNo = hcap.property.getProperty ({

    //   "key": "serial_number",
    //   "onSuccess": function (s) {

    //  
    //  console.log("onSuccess : property value = " + s.value);

    // SerialNo = s.value;

    //   //$('#slno').html(SerialNo);
    //    //$('.roomno').append(SerialNo);

    //   setTimeout(function () {
    //   getDeviceStatus();
    //   getDeviceConfig();
    //   }, 100);
    //   return SerialNo;
    //   },
    //  "onFailure": function (f) {
    //    
    //   console.log("onFailure : errorMessage = " + f.errorMessage);
    //   return "12-23-34-56-57-7811113";
    //  }
    //  })

    $scope.getRoomInfo = function () {
      debugger;
      
      var payload = {
        UDID: $scope.newUdid,
        ImageSize: configService.ImageSize,
      };
      $('.loader-wrapper').show();
      landingService.getRoomInfo(payload).then(
        function success(res) {
          debugger;
         
          $scope.roomDeviceInfo = res.data.ResponseData;
      
          if (res.data.ResponseStatus) {
            if (res.data.ResponseStatus.ResponseCode == "-101") {
              $scope.getHotelInfo();
            } else if (res.data.ResponseStatus.ResponseCode == "0") {
              // Oops ! Checkin details not found for this room
            } else {
              $rootScope.brndcountry= $scope.roomDeviceInfo.Country;
              $rootScope.brndcity= $scope.roomDeviceInfo.City;
              $rootScope.Roomnumber=$scope.roomDeviceInfo.RoomNumber;
              $rootScope.hotelcode=$scope.roomDeviceInfo.HotelProperty.HotelId;
              sessionStorage.setItem(
                "RoomNumber",
                JSON.stringify($scope.roomDeviceInfo.RoomNumber)
              );
              sessionStorage.setItem(
                "BookingId",
                JSON.stringify($scope.roomDeviceInfo.GuestInfo.ReservationNo)
              );
              sessionStorage.setItem(
                "GuestInfo",
                JSON.stringify($scope.roomDeviceInfo.GuestInfo)
              );
              $rootScope.currency=$scope.roomDeviceInfo.Symbol;
              $scope.languages = res.data.ResponseData.Languages;
              debugger;
              if ($scope.languages && $scope.languages.length > 0) {
                $scope.selectedLanguage = $scope.languages[0];
              } else {
                $scope.selectedLanguage ={'LanguageName':configService.LanguageName,'LanguageCode':configService.LanguageCode};
              }

              setTimeout(function () {
                $scope.getWeather();
              }, 500);
            }
          }

          $scope.setAuthFromLocalStorage(res.data.ResponseData);

          $('.loader-wrapper').hide();
          setTimeout(function () {
            focusController.setDepth(0, "welcome");
            focusController.focus("arrow-button");
          }, 20);
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
        }
      );
    };
    $rootScope.$on("stopCurrentMediaIPTV", function () {
      debugger;
      stopChannel();
      stopMedia();
    });

    function stopMedia() {
      debugger;
      if ($scope._media != null) {
        $scope._media.stop({
          onSuccess: function () {
            console.log("onSuccess");
            $scope._media.destroy({
              onSuccess: function () {
                console.log("onSuccess");
                hcap.Media.shutDown({
                  onSuccess: function () {
                    console.log("Media stopped succesfully");
                    $scope._media_status = 0; //stop
                  },
                  onFailure: function (f) {
                    console.log("onFailure : errorMessage = " + f.errorMessage);
                  },
                });
              },
              onFailure: function (f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
              },
            });
          },
          onFailure: function (f) {
            console.log("onFailure : errorMessage = " + f.errorMessage);
          },
        });
      }
       
     
    }

    //----- Stop current channel -----
    function stopChannel() {
      hcap.channel.stopCurrentChannel({
        onSuccess: function () {
          console.log("onSuccess : stopCurrentChannel");
        },
        onFailure: function (f) {
          console.log("onFailure : errorMessage = " + f.errorMessage);
        },
      });
    }
    $scope.setAuthFromLocalStorage = function (ResponseData) {
      // store auth accessToken/refreshToken/epiresIn in local storage to keep user logged in between page refreshes
      if (ResponseData ) {
        sessionStorage.setItem(
          $rootScope.authLocalStorageToken,
          JSON.stringify(ResponseData)
        );
        sessionStorage.setItem(
          $rootScope.authToken,
          JSON.stringify(ResponseData.Token)
        );
        sessionStorage.setItem("aid", JSON.stringify(ResponseData.HudiniRefId));
        sessionStorage.setItem(
          "pish",
          JSON.stringify(ResponseData.GuestInfo.MembershipId)
        );
        sessionStorage.setItem("Token", JSON.stringify(ResponseData.Token));
        sessionStorage.setItem(
          "HotelId",
          JSON.stringify(ResponseData.HotelProperty.HotelId)
        );
        sessionStorage.setItem(
          "BrandId",
          JSON.stringify(ResponseData.HotelProperty.BrandId)
        );
        sessionStorage.setItem(
          "GroupId",
          JSON.stringify(ResponseData.HotelProperty.GroupId)
        );

        GlobalService.pish = ResponseData.GuestInfo.MembershipId;
      }
    };

    $scope.getHotelInfo = function () {
      $('.loader-wrapper').show();
      landingService.getHotelInfo().then(
        function success(res) {
          $('.loader-wrapper').hide();
          console.log(res.data.ResponseData[0].Brands[0].Hotels);
          sessionStorage.setItem(
            "BrandId",
            '"' + res.data.ResponseData[0].Brands[0].BrandId + '"'
          );
          sessionStorage.setItem(
            "GroupId",
            '"' + res.data.ResponseData[0].GroupId + '"'
          );
          $scope.hotelInfo = [];
          $scope.hotelInfo = res.data.ResponseData[0].Brands[0].Hotels;
          // res.map((x) => {
          //   x.Brands.map((y) => {
          //     y.Hotels.map((z) => {
          //       $scope.hotelInfo.push(z);
          //     })
          //   });
          // });

          // $scope.selectHotel($scope.hotelInfo[0].Id);

          $(".udid-popup").show();
          $(".welcome-screen-wrapper").hide();
          setTimeout(function () {
            focusController.setDepth(7, "hotelcode");
            focusController.focus($("#txtrmno"));
          }, 500);
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
        }
      );
    };

    // $scope.selectHotel = function (id) {
    //   $scope.selectedHotel = $scope.hotelInfo.filter((x) => x.Id == id)[0];

    //   sessionStorage.setItem('HotelId', $scope.selectedHotel.Id);
    //   sessionStorage.setItem('BrandId', $scope.selectedHotel.BrandId);
    //   sessionStorage.setItem('GroupId', $scope.selectedHotel.GroupId);
    // };

    $scope.getWeather = function () {
      var data = JSON.parse(
        sessionStorage.getItem($rootScope.authLocalStorageToken)
      );

      if (!data) {
        return false;
      }

      var payload = {
        CountryCode: data.CountryCode,
        City: data.City,
        Unit: configService.TemperatureUnit,
      };

      $('.loader-wrapper').show();
      landingService.getWeather(payload).then(
        function success(weather) {
          $('.loader-wrapper').hide();
          $scope.currentTemp =
            weather.data.ResponseData.Weather.Current.Temperature;
          $rootScope.currentWeather = $scope.currentTemp;
          var location = {
            longitude: weather.data.ResponseData.Location.Longitude,
            latitude: weather.data.ResponseData.Location.Latitude,
          };
          sessionStorage.setItem("GeoLocation", JSON.stringify(location));
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
        }
      );
    };
    $scope.focusroom = function ($event) {
      $($event.currentTarget).addClass("active_room");
      // $($event.currentTarget).css('background', '#0f1b2a !important');
    };
    $scope.getshows = function () {
      $("#drope_box").show();
      setTimeout(function () {
        focusController.setDepth(8, "hotel");
        focusController.focus($("#hotel0"));
      }, 20);
    };
    $scope.closeshows = function () {
      currItem = focusController.getCurrentGroup();
      if ($("")) {
        setTimeout(function () {
          //focusController.setDepth(8, 'hotel')
          focusController.focus($("#btnSend"));
        }, 20);
        $("#drope_box").hide();
      }
    };
    $scope.openKeyboard = function (textId) {
      // global variable for textboxid
      //window.value = textId;
      //window.depth = focusController.getCurrentDepth();
      //window.group = focusController.getCurrentGroup();
      //setTimeout(function () {
      //    focusController.setDepth(10, 'keyboardGroup');
      //}, 20);
      //$('#headerkeyboard').show();
      //$('html, body').animate({
      //    scrollTop: $("#btn0").offset().top - 500
      //}, 500);

      window.value = textId;
      window.depth = focusController.getCurrentDepth();
      window.group = focusController.getCurrentGroup();
      $(".keypart").removeClass("hide");
      $("#SlnoKeyboard").slideDown();
      setTimeout(function () {
        focusController.setDepth(0, "keyboardGroup");
        focusController.focus($("#btn1"));
      }, 200);
      $("html, body").animate(
        {
          scrollTop: $("#" + textId).offset().top - 500,
        },
        500
      );
    };
    $scope.keyFocused = function ($event) {
      $($event.currentTarget).addClass("active");
    };
    $scope.keyBlurred = function ($event) {
      $($event.currentTarget).removeClass("active");
    };
    $scope.keySelected = function (id) {
      if ($(".key-btn button").hasClass("upper")) {
        var text = $("#" + id)
          .text()
          .toUpperCase()
          .trim();
      } else {
        var text = $("#" + id)
          .text()
          .trim();
      }
      var value = $("#" + window.value).val() + text;
      $("#" + window.value).val(value);
    };

    $scope.spacekeySelected = function () {
      // for allowing only one space at a time
      var a = ($('#' + window.value).val()).trim();
      var value = a + ' ';
      $('#' + window.value).val(value);
  }

  $scope.clearkeySelected = function () {
    $('#' + window.value).val('');

   // $scope.filterChannels();
  }

    $scope.removekeySelected = function () {
      var value = $("#" + window.value)
        .val()
        .slice(0, -1);
      $("#" + window.value).val(value);
    };

    $scope.btnGoSelected = function () {
      setTimeout(function () {
        //focusController.setDepth(window.depth, window.group);
        focusController.setDepth(7, "hotelcode");
        focusController.focus($("#searchid"));
      }, 200);
      $("#SlnoKeyboard").hide();
      $(".keypart").addClass("hide");
    };

    $scope.spacekeySelected = function () {
      // for allowing only one space at a time
      var a = $("#" + window.value)
        .val()
        .trim();
      var value = a + " ";
      $("#" + window.value).val(value);
    };
    $scope.selecthotel = function (id, name) {
      $("#searchid").val(name);
      $("#hresult").text(id);
      sessionStorage.setItem("HotelId", '"' + id + '"');
      setTimeout(function () {
        focusController.setDepth(7, "hotelcode");
        focusController.focus($("#btnSend"));
      }, 20);
      $("#drope_box").hide();
    };
    $scope.goToHome = function () {
      $location.path("/");
    };
    $scope.udidfocus = function ($event) {
      $($event.currentTarget).addClass("active-btn");
    };
    $scope.udidblur = function ($event) {
      $($event.currentTarget).removeClass("active-btn");
    };
  
  
    $scope.getUdid = function () {
      /*uncomment below  commented production*/
            //  SeialNo = hcap.property.getProperty({
            //       key: "serial_number",
            //     onSuccess: function (s) {
          
            //         console.log("onSuccess : property value = " + s.value);

            //           SerialNo = s.value;
            //          $scope.newUdid = SerialNo;
            //          setTimeout(function () {
            //            $scope.getRoomInfo();
            //           }, 100);

            //          return SerialNo;
            //      },
            //      onFailure: function (f) {
            //          console.log("onFailure : errorMessage = " + f.errorMessage);
            //           return "12345";
            //    },
            //     });
      /*local test*/
     // SerialNo="1234567"
    SerialNo="12-23-34-56-57-7812398850" //marina
      // SerialNo="12-23-34-56-57-7812399501" // palace
      
                 $scope.newUdid=SerialNo;
                setTimeout(function () {

                 $scope.getRoomInfo();

                     }, 100);
    };
    $scope.btnregisterDevice = function () {
      if ($("#txtrmno").val() == "") {
        alert("enter room number");
      } else {
        $scope.roomnum = $("#txtrmno").val();
        $scope.hotelid = $("#hresult").text();
        var data = {
          UDID: $scope.newUdid,
          RoomNo: $scope.roomnum || "0",
          Status: true,
        };
        //alert('Inside Device Register');
        // $scope.register['DeviceId'] = $scope.newUdid;
        // $scope.register['RoomNo'] = $scope.roomnum;
        // $scope.register['HotelId'] = $scope.hotelid;

        $('.loader-wrapper').show();
        landingService.getDeviceIdRegister(data).then(
          function success(response) {
            $('.loader-wrapper').hide();
            // console.log(response);
            //$location.path('/');
            //$route.reload();
            location.reload();
          },
          function error(response) {
            $('.loader-wrapper').hide();
            // console.log(response);
          }
        );
      }
    };
    $scope.registerUdid = function () {
      var data = {
        UDID: $scope.newUdid,
        RoomNo: $scope.roomNumber || "0",
        Status: true,
      };

      $('.loader-wrapper').show();
      landingService.registerUdid(data).then(
        function success(res) {
          $('.loader-wrapper').hide();
          if (res.ResponseCode != "M1001") {
            $(".error-popup")
              .find(".message")
              .text("Oops ! " + res.ResponseMessage);
            $(".error-popup").show();
           
            setTimeout(function() {
              focusController.focus($(".error-close"));
            }, 500);
          } else {
            $(".udid-popup").hide();
            setTimeout(function() {
              $scope.getRoomInfo();
            }, 500);
          }
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
        }
      );
    };

    $scope.roomNoSelected = function (event) {
      $scope.isRoomNoSelected = true;
      $(event.currentTarget).addClass("selected");
      setTimeout(function () {
        focusController.focus($("#btn_a"));
      }, 20);
    };

    $scope.udidSubmitted = function () {
      $scope.registerUdid();
    };

    $scope.udidCancelled = function () {
      $(".udid-popup").cancel();
      setTimeout(function () {
        focusController.focus($(".arrow-button").eq(0));
      }, 500);
    };

    $scope.hotelFocused = function (event) {
      $scope.isRoomNoSelected = false;
      var parent = $(".hotel-list-wrapper");
      var child = $(event.currentTarget);
      $scope.scroll(parent, child);
    };

    $scope.hotelSelected = function (event) {
      $(".hotel-list>.item").removeClass("selected");
      $(event.currentTarget).addClass("selected");
      $scope.selectHotel($(event.currentTarget).attr("data-hotelid"));
    };

    $scope.keyboardClicked = function (event) {
      var id = $(event.currentTarget).attr("id");
      if (id == "btn_clear") {
        $scope.clearButton();
      } else if (id == "btn_space") {
        $scope.spaceButton();
      } else if (id == "btn_backspace") {
        $scope.backspaceButton();
      } else {
        $scope.inputButton($(event.currentTarget).text());
      }
    };

    $scope.clearButton = function () {
      if ($scope.isRoomNoSelected) {
        $scope.roomNumber = "";
      }
    };

    $scope.spaceButton = function () {
      if ($scope.isRoomNoSelected) {
        $scope.roomNumber += " ";
      }
    };

    $scope.backspaceButton = function () {
      if ($scope.isRoomNoSelected) {
        $scope.roomNumber = $scope.roomNumber.substring(
          0,
          $scope.roomNumber.length - 1
        );
      }
    };

    $scope.inputButton = function () {
      if ($scope.isRoomNoSelected) {
        $scope.roomNumber += character;
        $scope.$apply();
      }
    };

    $scope.scroll = function (parent, child) {
      parent.animate({ scrollTop: $(child).index() * $(child).height() }, 500);
      return false;
    };

    $scope.errorClosed = function () {
      $(".error-popup").hide();
      setTimeout(function () {
        focusController.focus($(".error-close"));
      }, 500);
    };

    $scope.getRandomId = function () {
      var chars = "0123456789abcdef".split("");
      var uuid = [],
        rnd = Math.random,
        r;
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = "-";
      uuid[14] = "4"; // version 4

      for (var i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | (rnd() * 16);

          uuid[i] = chars[i == 19 ? (r & 0x3) | 0x8 : r & 0xf];
        }
      }

      return uuid.join("");
    };

    $scope.init();
  },
]);

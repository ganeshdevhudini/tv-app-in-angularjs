app.controller("liveTvSearchController", [
  "GlobalService",
  "$route",
  "$rootScope",
  "$scope",
  "$location",
  "$timeout",
  "focusController",
  "configService",
  "$translate",
  "liveTvService",
  function (
    GlobalService,
    $route,
    $rootScope,
    $scope,
    $location,
    $timeout,
    focusController,
    configService,
    $translate,
    liveTvService
  ) {
    $rootScope.authLocalStorageToken =
      configService.appVersion + "-" + configService.USERDATA_KEY;
    $scope.imgURL = configService.imageUrl;
    $scope.searchText = '';

    focusController.addBeforeKeydownHandler(function (context, controller) {
      if (context.event.keyCode) {
        switch (context.event.keyCode) {
          case Keys.ReturnKey:
            $location.path('\livetv');
            break;
          default:
            break;
        }
      }
    });

    $scope.init = function () {
      $rootScope.isNavMenuRequired = false;
      $scope.randomId = 's' + $scope.getRandomInt(0, 100000);
      $scope.getGenreInfo();
      setTimeout(function(){
        focusController.focus($('#btn_a'));
      })
    };

    $scope.getGenreInfo = function () {
      $scope.genreDetails = [];

      var data = JSON.parse(
        sessionStorage.getItem($rootScope.authLocalStorageToken)
      );

      if (!data) {
        return false;
      }

      var payload = {
        RoomNo: data.RoomNumber,
        ImageSize: configService.ImageSize,
      };

      $('.loader-wrapper').show();
      liveTvService.getGenreInfo(payload).then(
        function success(responseData) {
          $('.loader-wrapper').hide();
          var res = responseData.data.ResponseData;
          if (res.TVGenres) {
            $scope.genreDetails = res.TVGenres;
            localStorage.setItem(
              "channels",
              JSON.stringify($scope.genreDetails)
            );
            $scope.setchannels();
          }
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
        }
      );
    };

    $scope.setchannels = function () {
      $scope.channelsList = [];
      $scope.genreDetails.forEach(function(element)  {
        element.Channels.forEach(function(channel)  {
          $scope.channelsList.push(channel);
        });
      });
    };

    $scope.keyboardClicked = function (event) {
        var id = $(event.currentTarget).attr('id');
          if (id == 'btn_clear') {
            $scope.clearButton();
          } else if (id == 'btn_space') {
            $scope.spaceButton();
          } else if (id == 'btn_backspace') {
            $scope.backspaceButton();
          } else {
            $scope.searchText += $(event.currentTarget).text();
            $scope.ngFilterChannel();
          }
      };

      $scope.clearButton = function () {
        $scope.searchText = '';
        $scope.ngFilterChannel();
    };

    $scope.spaceButton = function () {
        $scope.searchText += ' ';
        $scope.ngFilterChannel();
    };

    $scope.backspaceButton = function () {
        $scope.searchText = $scope.searchText.substring(0, $scope.searchText.length - 1);
        $scope.ngFilterChannel();
    };

    $scope.ngFilterChannel = function () {
      debugger;
      $scope.searchchannel=[];
        var x=localStorage.getItem("channels");
        $scope.currentGenre = JSON.parse(x);
      //  $scope.currentchannel=$scope.currentchannel.push($scope.currentGenre.Channels)
        for (i = 0; i < $scope.currentGenre.length; i++) {
          $scope.searchchannel = $scope.searchchannel.concat($scope.currentGenre[i].Channels);
      }
        if ($scope.searchText != '') {
        $scope.nochannelsFound = true;
        $scope.currentGenre = $scope.currentGenre.filter(function(x) {
            x.Channels = x.Channels.filter(function(y) {
            return y.ChannelName.toLowerCase().indexOf($scope.searchText) == 0 || y.ChannelNumber.toLowerCase().indexOf($scope.searchText) == 0
            })
            return true
        });
        $scope.searchchannel =$scope.searchchannel.filter(function (x) {return  x.ChannelName.toLowerCase().indexOf($scope.searchText) == 0 || x.ChannelNumber.toLowerCase().indexOf($scope.searchText) == 0 });
      //   $scope.searchchannel = $scope.searchchannel.filter((x) => {
         
      //     return x.ChannelName.toLowerCase().indexOf($scope.searchText) == 0 || x.ChannelNumber.toLowerCase().indexOf($scope.searchText) == 0
         
      // });
        $scope.currentGenre.forEach(function(x){
            if(x.Channels.length > 0){
            $scope.nochannelsFound = false;
            }
        });
        }
    };

    $scope.channelFocused = function (event) {
        var child = $(event.currentTarget);
        $scope.scroll($('.channel-list'), child);
    };
    focusController.addAfterKeydownHandler(function (context, controller) {
      context.event.stopImmediatePropagation();
      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='liveTvSearch')
          {
            if (context.event.keyCode) {
              var currentFocusableGroup = controller.getCurrentGroup();
              var currenturl = window.location.href;
              currenturl = currenturl.split("#/").pop();
               if (currenturl == "liveTvSearch" ) {
                switch (context.event.keyCode) {
                  
                 
                    case 461:
                      
                        
                         
                         
                         
                           $location.path('/livetv');
      
                        
                      
                   
                    break;
                    case 9:
                      
                         
                         
                         
                           $location.path('/livetv');
      
                       
                    break;
                  default:
                    // console.log('Key code : ' + context.event.keyCode);
                    break;
                }
              }
            }
          }
    
    });
    $scope.channelSelected = function (event) {
        var ip = $(event.currentTarget).find('.channel-ip').text();
        var port = $(event.currentTarget).find('.channel-port').text();
        var chnumber = $(event.currentTarget).find('.channel-number').text();
        $scope.currentchannelname = $(event.currentTarget).find('.channel-name').text();
        $scope.playIPChannel(ip, port,chnumber);
    };

    $scope.playIPChannel = function (ip, port,chnumber) {
        GlobalService.ip = ip;
        GlobalService.port = port;
        GlobalService.chnumber = chnumber;
        GlobalService.chname= $scope.currentchannelname;
        setTimeout(function () {
            $location.path('/livetv');
        }, 500);
    };

    $scope.scroll = function (parent, child) {
      parent.stop();
      parent.animate(
        { scrollTop: $(child).index() * $(child).height() - (3*$(child).height())+15 }, 50
      );
    };

    $scope.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    };

    $scope.init();
  },
]);

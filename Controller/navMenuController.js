app.controller("navMenuController", [
  "GlobalService",
  "$route",
  "$rootScope",
  "$scope",
  "$location",
  "$timeout",
  "focusController",
  "configService",
  "$translate",
  function (
    GlobalService,
    $route,
    $rootScope,
    $scope,
    $location,
    $timeout,
    focusController,
    configService,
    $translate
  ) {
    debugger;
    $scope._stateMedia = null;
 
    $scope._media = null;
    $scope._media_status = 0; //stop
    $scope.roomNumber = JSON.parse(sessionStorage.getItem("RoomNumber"));
    $scope.landingTiles = [
      {
        title: "Live TV",
        id: "0",
        iconUrl: "img/icons/live_tv_icon.svg",
        iconUrlActive: "img/icons/live_tv_icon_active.svg",
      },
      {
        title: "Hotel Info",
        id: "6",
        iconUrl: "img/icons/hotel_info_icon.svg",
        iconUrlActive: "img/icons/hotel_info_icon_active.svg",
      },
      {
        title: "Offers",
        id: "7",
        iconUrl: "img/icons/offers.png",
        iconUrlActive: "img/icons/offers_active.svg",
      },
      // {
      //   title: 'In Room Dining',
      //   id: '1',
      //   iconUrl: 'img/icons/ird_icon.svg',
      //   iconUrlActive: 'img/icons/ird_icon_active.svg',
      // },
      //  {
      //  title: 'Service Request',
      //   id: '2',
      //    iconUrl: 'img/icons/service_request_icon.svg',
      //    iconUrlActive: 'img/icons/service_request_icon_active.svg',
      // },
       
      {
        title: "Restaurant",
        id: "4",
        iconUrl: "img/icons/restaurant_icon.svg",
        iconUrlActive: "img/icons/restaurant_icon_active.svg",
      },
      {
        title: 'Wellness',
        id: '3',
     iconUrl: 'img/icons/wellness_icon.svg',
        iconUrlActive: 'img/icons/wellness_icon_active.svg',
      },
      {
        title: "City Guide",
        id: "5",
        iconUrl: "img/icons/city_guide_icon.svg",
        iconUrlActive: "img/icons/city_guide_icon_active.svg",
      },
    
      {
        title: "Screen Cast",
        id: "8",
        iconUrl: "img/icons/cast.svg",
        iconUrlActive: "img/icons/cast_active.svg",
      },
      {
        title: "Prayer Timing",
        id: "9",
        iconUrl: "img/icons/prayer-time.svg",
        iconUrlActive: "img/icons/prayer-time_active.svg",
      },
     
      // {
      //   title: 'My Account',
      //   id: '10',
      //   iconUrl: 'img/icons/my-account.svg',
      //   iconUrlActive: 'img/icons/my-account_active.svg',
      // },
      // {
      //   title: 'View Bill',
      //   id: '11',
      //   iconUrl: 'img/icons/viewbill.svg',
      //   iconUrlActive: 'img/icons/viewbill_active.svg',
      // },
    ];

    // window.addEventListener("keydown", function (e) { 
    //   debugger;      
    //   alert('keydown');
    //   var currenturl = window.location.href;

    //   ipURL = currenturl.split("#/").pop();
    //   $scope.isModuleHasMenuTab = false;
    //   $scope.isIRDModule = false;
    //   $scope.isSpaModule = false;
    //   $scope.isLightModule = false;
      
    //   if (ipURL == "livetv") {
    //       $rootScope.livetvkeydown(e);
    //   }
    // })
    focusController.addAfterKeydownHandler(function (context, controller) {
     
     
      if (context.event.keyCode) {
        var currentFocusableGroup = controller.getCurrentGroup();
        if (currentFocusableGroup == "menu-item") {
          $(".left-menu").addClass("expanded");
          $(".default-hide").show(150);
        } else {
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        }

        switch (context.event.keyCode) {
          case 9:
            if (portchange == true) {
              // document.body.style.visibility = "visible";
              $("html").show();
              portchange = false;
              $rootScope.$emit("stopCurrentMediaIPTV", {});
            
              hcap.mode.setHcapMode({
                mode: hcap.mode.HCAP_MODE_1,
                onSuccess: function () {
                  hcap.property.setProperty({
                    "key" : "boot_sequence_option", 
                    "value" : "1", 
                    "onSuccess" : function() {
                        console.log("onSuccess");
                    }, 
                    "onFailure" : function(f) {
                        console.log("onFailure : errorMessage = " + f.errorMessage);
                    }
                });
                  hcap.externalinput.setCurrentExternalInput({
                    type: hcap.externalinput.ExternalInputType.HDMI,
                    index: 1,
                    onSuccess: function () {
                    
                      $location.path("/");
                      setTimeout(function () {
                        focusController.setDepth(0, "home");
                        focusController.focus("hometile0");
                      }, 200);
                    },
                    onFailure: function (f) {
                      console.log(
                        "onFailure : errorMessage = " + f.errorMessage
                      );
                    },
                  });
                },
              });
            }
            break;
          case 461:
            if (portchange == true) {
              // document.body.style.visibility = "visible";
              $("html").show();
              portchange = false;
              $rootScope.$emit("stopCurrentMediaIPTV", {});
              hcap.mode.setHcapMode({
                mode: hcap.mode.HCAP_MODE_1,
                onSuccess: function () {
                 
                  hcap.externalinput.setCurrentExternalInput({
                    type: hcap.externalinput.ExternalInputType.HDMI,
                    index: 1,
                    onSuccess: function () {
                      $rootScope.$emit("stopCurrentMediaIPTV", {});
                      $location.path("/");
                      setTimeout(function () {
                        focusController.setDepth(0, "home");
                        focusController.focus("hometile0");
                      }, 200);
                    },
                    onFailure: function (f) {
                      console.log(
                        "onFailure : errorMessage = " + f.errorMessage
                      );
                    },
                  });
                },
              });
            }
            break;
          default:
            break;
        }
      }
    });

    $scope.init = function () {
      //  setTimeout(function () {
      //    focusController.setDepth(0, "menu-item");
      //    focusController.focus("menu-item-0");
      //  }, 20);
    };

    $scope.isSelected = function (item) {
      var url = $location.path();
      console.log("url" + url);
      var section = url.split("/")[1];
      if (item == "Live TV") {
        if (section == "livetv") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "In Room Dining") {
        if (section == "ird") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Service Request") {
        if (section == "housekeeping") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Wellness") {
        if (section == "spa") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Restaurant") {
        if (section == "restaurant") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "City Guide") {
        if (section == "cityguide") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Hotel Info") {
        if (section == "hotelinfo") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Offers") {
        if (section == "offers") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Screen Cast") {
        if (section == "screencast") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "Prayer Timing") {
        if (section == "prayertiming") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "My Account") {
        if (section == "myaccount") {
          return true;
        } else {
          return false;
        }
      }
      if (item == "View Bill") {
        if (section == "viewbill") {
          return true;
        } else {
          return false;
        }
      }
      return true;
    };

    function stopMedia() {
      if ($scope._media != null) {
        $scope._media.stop({
          onSuccess: function () {
            console.log("onSuccess");
            $scope._media.destroy({
              onSuccess: function () {
                console.log("onSuccess");
                hcap.Media.shutDown({
                  onSuccess: function () {
                    console.log("Media stopped succesfully");
                    $scope._media_status = 0; //stop
                  },
                  onFailure: function (f) {
                    console.log("onFailure : errorMessage = " + f.errorMessage);
                  },
                });
              },
              onFailure: function (f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
              },
            });
          },
          onFailure: function (f) {
            console.log("onFailure : errorMessage = " + f.errorMessage);
          },
        });
      } else {
        console.log(" _media is null");
      }
    }

    //----- Stop current channel -----
    function stopChannel() {
      hcap.channel.stopCurrentChannel({
        onSuccess: function () {
          console.log("onSuccess : stopCurrentChannel");
        },
        onFailure: function (f) {
          console.log("onFailure : errorMessage = " + f.errorMessage);
        },
      });
    }
    $scope.scroll = function (parent, child) {
      parent.animate(
        {
          scrollTop:
            $(child).index() * $(child).height() - $(child).height() * 2,
        },
        0
      );
      return false;
    };

    $scope.onMenuFocus = function (event) {
      var actualParent = $(".menu-list-wrapper");
      var actualChild = $(event.currentTarget).parent().parent();
      $scope.scroll(actualParent, actualChild);
      $(".menu-button").removeClass("focused");
      $(event.currentTarget).parent().addClass("focused");
      var activeIcon = $(event.currentTarget).attr("data-active-icon");
      $(".icon").css("filter", "invert(0)");
      $(event.currentTarget).css("filter", "invert(0.8)");

      $(".left-menu").addClass("expanded");
      $(".default-hide").show(150);
    };
    $scope.notificationselect=function()
    {
      
      $location.path('/notification');
      $(".left-menu").removeClass("expanded");
      $(".default-hide").hide(150);
    }
    $scope.onMenuSelected = function (event) {
      $(".menu-button").removeClass("selected");
      $(".menu-button").removeClass("focused");
      $(event.currentTarget).css("filter", "invert(0)");
      $(event.currentTarget).parent().addClass("selected");
      var attr = $(event.currentTarget).attr("data-focusable-group");
      if (attr == "menu-item") {
        $(".loader-section").show();
        // this.stopChannel();
        if ($(event.currentTarget).attr("title") == "Live TV") {
          path = "/livetv";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "In Room Dining") {
          path = "/ird";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Service Request") {
          path = "/serviceRequest";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Wellness") {
          path = "/spa";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Restaurant") {
          path = "/restaurant";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "City Guide") {
          path = "/cityguide";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Hotel Info") {
          path = "/hotelinfo";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Offers") {
          path = "/offers";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Prayer Timing") {
          path = "/prayertiming";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "My Account") {
          path = "/myaccount";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "View Bill") {
          path = "/viewbill";
          $(".left-menu").removeClass("expanded");
          $(".default-hide").hide(150);
        } else if ($(event.currentTarget).attr("title") == "Screen Cast") {
          path = "/";

          $("html").hide();
          // document.body.style.visibility = "hidden";

          portchange = true;

          hcap.mode.setHcapMode({
            mode: hcap.mode.HCAP_MODE_0,
            onSuccess: function () {
              //connect to HDMI1
              hcap.externalinput.setCurrentExternalInput({
                type: hcap.externalinput.ExternalInputType.HDMI,
                index: 0,
                onSuccess: function () {
                  console.log("onSuccess");
                },
                onFailure: function (f) {
                  console.log("onFailure : errorMessage = " + f.errorMessage);
                },
              });
            },
            onFailure: function (f) {
              console.log("onFailure : errorMessage = " + f.errorMessage);
            },
          });
          return false;
        }

        $location.path(path);
      }
    };

    $scope.stopChannel = function () {
      GlobalService.isTvPlaying = false;
      var pluginIPTV = document.getElementById("pluginSefIPTV");
      pluginIPTV.Open("IPTV", 1.0, "IPTV");
      pluginIPTV.Execute("StopCurrentChannel", 0);
      pluginIPTV.Execute("FreeNowPlayingInfo", 0);
      pluginIPTV.Close();
      $("#pluginSefIPTV").empty();
    };

    $scope.init();
  },
]);

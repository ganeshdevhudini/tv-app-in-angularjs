app.controller("offerDetailsController", [
  "offersService",
  "$location",
  "$filter",
  "$scope",
  "$rootScope",
  "focusController",
  "GlobalService",
  "configService",
  function (
    offersService,
    $location,
    $filter,
    $scope,
    $rootScope,
    focusController,
    GlobalService,
    configService
  ) {
    debugger;
    $scope.imgURL = configService.imageUrl;
    var currenturl = window.location.href;
    currenturl = currenturl.split("#/").pop();
    $scope.init = function () {
      debugger;
      $rootScope.isNavMenuRequired = false;
      $scope.setData();
      setTimeout(function () {
        var item = $("#backbutton");
        focusController.focus(item);
      }, 20);
    };

    $scope.convertToDate = function (datestring, isYear) {
      debugger;
      var date = new Date(datestring);
      var result;
      if (isYear) {
        // result = $scope.datepipe.transform(date, 'dd MMM YYYY');
        result = $filter("date")(date, "dd MMM yyyy");
      } else {
        // result = $scope.datepipe.transform(date, 'dd MMM');
        result = $filter("date")(date, "dd MMM");
      }
      return result;
    };

    $scope.setData = function () {
      debugger;
      var offerDetail = JSON.parse(sessionStorage.getItem('offerDetail'));

      if (offerDetail && offerDetail != '') {
        $scope.data = offerDetail;
      }
      else {
        $location.path('/offers');
      }
    };

    $scope.backButtonClicked = function () {
      $location.path('/offers');
    
    };
  
    focusController.addAfterKeydownHandler(function (context, controller) {
      context.event.stopImmediatePropagation();
      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='offerDetails')
          {
            if (context.event.keyCode) {
              var currentFocusableGroup = controller.getCurrentGroup();
              var currenturl = window.location.href;
              currenturl = currenturl.split("#/").pop();
               if (currenturl == "offerDetails" ) {
                switch (context.event.keyCode) {
                  
                 
                    case 461:
                      
                        
                         
                         
                         
                           $location.path('/offers');
      
                        
                      
                   
                    break;
                    case 9:
                      
                         
                         
                         
                           $location.path('/offers');
      
                       
                    break;
                  default:
                    // console.log('Key code : ' + context.event.keyCode);
                    break;
                }
              }
            }
          }
      
    });
   
    
  
    $scope.init();
  },
]);

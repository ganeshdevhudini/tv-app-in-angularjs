app.controller("paginationController", [
  "GlobalService",
  "$route",
  "$rootScope",
  "$scope",
  "$location",
  "$timeout",
  "focusController",
  "configService",
  "$translate",
  function (
    GlobalService,
    $route,
    $rootScope,
    $scope,
    $location,
    $timeout,
    focusController,
    configService,
    $translate
  ) {
    $scope.items;
    $scope.randomId;
    $scope.goDown = false;
    $scope.initialPage = 1;
    $scope.pageSize = 15;
    $scope.maxPages = 10;

    $scope.pager;
    $scope.random;

    $scope.init = function () {
      $scope.random = 's' + $scope.getRandomInt(0, 1000);
      if ($scope.items && $scope.items.length) {
        $scope.setPage($scope.initialPage);

        setTimeout(function () {
          var item = $('.right-arrow')[0];
          focusController.focus(item);
        }, 20);
      }
    };


    $scope.setPage = function (page) {
      // get new pager object for specified page
      $scope.pager = $scope.paginate($scope.items.length, page, $scope.pageSize, $scope.maxPages);
      // get new page of items from items array
      var pageOfItems = $scope.items.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);

      // call change page function in parent component
      $scope.$emit('child', pageOfItems);
    };

    $scope.getRandomInt = function (min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    };

    $scope.paginate = function (totalItems, currentPage, pageSize, maxPages) {
      // calculate total pages
      var totalPages = Math.ceil(totalItems / pageSize);

      // ensure current page isn't out of range
      if (currentPage < 1) {
        currentPage = 1;
      } else if (currentPage > totalPages) {
        currentPage = totalPages;
      }

      var startPage, endPage;
      if (totalPages <= maxPages) {
        // total pages less than max so show all pages
        startPage = 1;
        endPage = totalPages;
      } else {
        // total pages more than max so calculate start and end pages
        var maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
        var maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
        if (currentPage <= maxPagesBeforeCurrentPage) {
          // current page near the start
          startPage = 1;
          endPage = maxPages;
        } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
          // current page near the end
          startPage = totalPages - maxPages + 1;
          endPage = totalPages;
        } else {
          // current page somewhere in the middle
          startPage = currentPage - maxPagesBeforeCurrentPage;
          endPage = currentPage + maxPagesAfterCurrentPage;
        }
      }

      // calculate start and end item indexes
      var startIndex = (currentPage - 1) * pageSize;
      var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

      // create an array of pages to ng-repeat in the pager control
      var pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

      // return object with all pager properties required by the view
      return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
      };
    };

    $scope.prevClicked = function () {
      if($scope.pager.currentPage !== 1){
        var prev = $scope.pager.currentPage - 1;
        $scope.setPage(prev);
      }
    };

    $scope.nextClicked = function () {
      if($scope.pager.currentPage != $scope.pager.totalPages){
        var next = $scope.pager.currentPage + 1;
        $scope.setPage(next);
      }
    };

    $scope.init();
  },
]);

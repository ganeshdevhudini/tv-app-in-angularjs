﻿app.controller("prayertimeController", [
  "prayertimeService",
  "GlobalService",
  "$route",
  "$scope",
  "$location",
  "$timeout",
  "focusController",
  "configService",
  "$translate",
  "$http",
  "$rootScope",
  "$route",
  "$filter",
  function (
    prayertimeService,
    GlobalService,
    $route,
    $scope,
    $location,
    $timeout,
    focusController,
    configService,
    $translate,
    $http,
    $rootScope,
    $route,
    $filter
  ) {
    $scope.now = new Date();
    $scope.timingData = [
      {
        name: 'Fajr',
        time: '',
        isActive: false,
      },
      {
        name: 'Sunrise',
        time: '',
        isActive: false,
      },
      {
        name: 'Dhuhr',
        time: '',
        isActive: false,
      },
      {
        name: 'Asr',
        time: '',
        isActive: false,
      },
      {
        name: 'Maghrib',
        time: '',
        isActive: false,
      },
      {
        name: 'Isha',
        time: '',
        isActive: false,
      },
    ];
    
    $scope.init = function () {
      $scope.getPrayerTime();
    };

    $scope.getPrayerTime = function () {
      
      var location = JSON.parse(sessionStorage.getItem('GeoLocation'));

      $('.loader-wrapper').show();
      prayertimeService.getPrayertime(location).then(
        function success(responseData) {
          $('.loader-wrapper').hide();
          var res = responseData.data.responsedata;
          if (res) {
            var timing = res.prayertimes;
            $scope.timingData.map(function(x) {
              x.time = timing.filter(function(y){return x.name == y.name})[0].time;
            });
            setTimeout(function() {
              $scope.getUpcomingPrayerTime();
            }, 500);
          }
          
        },
        function error(response) {
          $('.loader-wrapper').hide();
          console.log(response);
          $location.path('/');
          // chromeGlobalService.focus = true;
          setTimeout(function () {
              debugger;
              // $location.path('/');

              // window.location.reload()

              location.reload();
              // focusController.setDepth(0, "home");
              // focusController.focus('hometile0')
          }, 200);
        }
      );
    }

    $scope.getUpcomingPrayerTime = function () {
      var date = new Date();
      // var localeSpecificTime = $scope.datepipe.transform(date, 'HH:mm');
      var localeSpecificTime = $filter('date')(date, 'HH:mm');
      var isUpcomingFound = false;
      $scope.timingData.forEach(function (y) {
        if (y.time != '' && y.time && !isUpcomingFound) {
          if (
            Date.parse('01/01/2011 ' + localeSpecificTime) <
            Date.parse('01/01/2011 ' + y.time + ':00')
          ) {
            y.isActive = true;
            isUpcomingFound = true;
          }
          setTimeout(function(){
            var item = $('#card-0').eq(0);
            focusController.focus(item);
          },200)
        }
      });
      if (!isUpcomingFound){
        $scope.timingData[0].isActive = true;
      }
    }

    $scope.convert = function (value) {
      var time = value;
      if (value) {
        var hour = +value.split(':')[0];
        if (hour == 0) {
          time = 12 + ":" + value.split(':')[1] + " AM"
        }
        else if (hour > 0 && hour < 12) {
          time = hour + ":" + value.split(':')[1] + " AM"
        }
        else if (hour == 12) {
          time = hour + ":" + value.split(':')[1] + " PM"
        }
        else if (hour > 12) {
          hour = hour - 12;
          time = hour + ":" + value.split(':')[1] + " PM"
        }
      }
      return time;
    }
    $rootScope.prayertimekeydown = function (e) {
      debugger;
      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
       switch (e.keyCode) {
     
       
  case 461:
            
              
                
                  $location.path('/');
       
                  $scope.$apply();
           
           break; 
           case Keys.ReturnKey: //backspace
           //debugger;
           
            
            
            
              // GlobalService.livtvalready=true;
              
                $location.path('/');
                $scope.$apply();
     
            
            
           
           break;
         default:
           // console.log('Key code : ' + context.event.keyCode);
           break;
          
   
       }
     }
    $scope.init();
  },
]);

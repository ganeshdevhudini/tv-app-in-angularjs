app.controller("restaurantDetailsController", [
    "offersService",
    "$location",
    "$filter",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
      offersService,
      $location,
      $filter,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.imgURL = configService.imageUrl;
      var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
      $scope.init = function () {
        $rootScope.isNavMenuRequired = false;
        $scope.setData();
        setTimeout(function () {
          var item = $("#backbutton");
          focusController.focus(item);
        }, 20);
      };
  
      $scope.convertToDate = function (datestring, isYear) {
        var date = new Date(datestring);
        var result;
        if (isYear) {
          // result = $scope.datepipe.transform(date, 'dd MMM YYYY');
          result = $filter("date")(date, "dd MMM YYYY");
        } else {
          // result = $scope.datepipe.transform(date, 'dd MMM');
          result = $filter("date")(date, "dd MMM");
        }
        return result;
      };
  
      $scope.setData = function () {
        var restaurantDetail = JSON.parse(sessionStorage.getItem('restDetail'));
  
        if (restaurantDetail && restaurantDetail != '') {
          $scope.data = restaurantDetail;
        }
        else {
          $location.path('/restaurant');
        }
      };
  
      $scope.backButtonClicked = function () {
        $location.path('/restaurant');
      
      };
    
      focusController.addAfterKeydownHandler(function (context, controller) {
        context.event.stopImmediatePropagation();
        var currenturl = window.location.href;
        currenturl = currenturl.split("#/").pop();
            if(currenturl=='restaurantDetails')
            {
              if (context.event.keyCode) {
                var currentFocusableGroup = controller.getCurrentGroup();
                var currenturl = window.location.href;
                currenturl = currenturl.split("#/").pop();
                 if (currenturl == "restaurantDetails" ) {
                  switch (context.event.keyCode) {
                    
                   
                      case 461:
                        
                          
                           
                           
                           
                             $location.path('/restaurant');
        
                          
                        
                     
                      break;
                      case 9:
                        
                           
                           
                           
                             $location.path('/restaurant');
        
                         
                      break;
                    default:
                      // console.log('Key code : ' + context.event.keyCode);
                      break;
                  }
                }
              }
            }
        
      });
     
      
    
      $scope.init();
    },
  ]);
  
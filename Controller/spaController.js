app.controller("spaController", [
    "spaService",
    "$location",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
      spaService,
      $location,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.showDetail = false;
      $scope.imageSize = configService.ImageSize;
      $scope.imgURL = configService.imageUrl;
      $scope.spa = [];
      $scope.offersTypes = ['All'];
      $scope.randomId = '';
      
    
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.randomId = 's' + $scope.getRandomInt(0, 100000);
        sessionStorage.setItem('spaDetail', JSON.stringify(''));
        $scope.getspaInfo();
      };
  
          $scope.getspaInfo = function () {
            debugger;
        $scope.spa = [];
        // $scope.offersTypes = ['All'];
        // $scope.offers = [];
  
        $('.loader-wrapper').show();
        spaService.getspa().then(function success(responseData) {
          debugger;
            $('.loader-wrapper').hide();
            var res = responseData.data.responsedata;
              $scope.spa = res;
              if($scope.spa.length==1)
              {
                var spaid = $scope.spa[0].id;
                $rootScope.spalength=1;
                sessionStorage.setItem('spaDetail', JSON.stringify($scope.spa.filter(function(i){return i.id === spaid} )));
                sessionStorage.setItem('spaId', "\""+spaid+"\"");
                $location.path('/treatment');

              }
              else{
                $rootScope.spalength=0;
                $scope.updatespaImage();
              }
              
              
          //     $scope.allOffers.forEach((element) => {
          //       $scope.offersTypes.push(element.Name);
          //     element.Details.forEach((item) => {
          //         $scope.offers.push(item);
          //     });
          //     debugger;
          //     if(JSON.parse(sessionStorage.getItem('OfferId')) && JSON.parse(sessionStorage.getItem('OfferId'))!=="") {
          //       setTimeout(function () {
          //         $('.offerType').eq(0).addClass('selected');
          //         focusController.focus($('#'+JSON.parse(sessionStorage.getItem('OfferId'))))
          //       }, 200);
          //     } else {
          //       setTimeout(function(){
          //         $('.offerType').removeClass('selected');
          //         $('.offerType').eq(0).addClass('selected');
  
          //         var item = $('.focusable-offer').eq(0);
          //         focusController.focus(item);
          //       },200)
          //     }
          // });
              }, function error(response) {
          $('.loader-wrapper').hide();
                  console.log(response);
                  $location.path('/');
                  // chromeGlobalService.focus = true;
                  setTimeout(function () {
                      debugger;
                      // $location.path('/');
  
                      // window.location.reload()
  
                      location.reload();
                      // focusController.setDepth(0, "home");
                      // focusController.focus('hometile0')
                  }, 200);
              });
      };
      $scope.updatespaImage = function () {
        var item=0;
        $scope.spa.forEach(function(element)  {
            if (element.webimages.length > 0 && element.webimages[0].fileattachmentid) {
                element.webimages[0].metadata = $scope.getspaImage(item, element.webimages[0].fileattachmentid);
            }
            item=item+1;
        });
    }
   
    $scope.getspaImage = function (index, id) {
      spaService.getSpaImage(id).then(function success(response) {
          if (response) {
              debugger;
             // var urlCreator = window.URL || window.webkitURL;
              //var imageUrl = urlCreator.createObjectURL(new Blob([response.data],{type:'image/png'}));
              var reader = new FileReader();
             reader.readAsDataURL(new Blob([response.data], {type:'image/png'}));
             //reader.readAsDataURL(new Blob([response.data],{type:'image/png'}));
             //let v=URL.createObjectURL(new Blob([response.data],{type:'image/png'}));
             reader.onload = function(e) {
                  $scope.$apply(function() {
                      $scope.spa[index].webimages[0].metadata = reader.result;
                  });
              };
            }
      }, function error(response) {
          console.log(response);
      });
  }
      $scope.offerFocused = function (event) {
        var parent = $('.offer-list');
        var child = $(event.currentTarget);
        $scope.scrollHorizontal(parent, child);
      };
  
      $scope.spaSelected = function (event) {
        debugger;
        var actualChild = $(event.currentTarget);
        var spaid = $(actualChild).attr('id');
        sessionStorage.setItem('spaDetail', JSON.stringify($scope.spa.filter(function(i) {return i.id === spaid} )));
        sessionStorage.setItem('spaId', "\""+spaid+"\"");
        $location.path('/treatment');
      };
      
        focusController.addAfterKeydownHandler(function (context, controller) {
          context.event.stopImmediatePropagation();
          var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='spa')
          {
          if (context.event.keyCode) {
            var currentFocusableGroup = controller.getCurrentGroup();
            var currenturl = window.location.href;
            currenturl = currenturl.split("#/").pop();
            if (currenturl=="spa" ) {
              switch (context.event.keyCode) {
                
               
                  case 461:
                    
                      
                       
                       
                       
                         $location.path('/');
    
                      
                    
                 
                  break;
                  case 9:
                    
                       
                       
                       
                         $location.path('/');
    
                     
                  break;
                default:
                  // console.log('Key code : ' + context.event.keyCode);
                  break;
              }
            }
          }
        }
        });
      
      
    
      $scope.offerlifocus=function(event)
      {
        if($('.focusable-offer').hasClass('focused'))
        {
          $(".offer-list").scrollCenter(".focused", 700);
        }
      }
      $scope.offerTypeSelected = function (event) {
        $('.offerType').removeClass('selected');
        $(event.currentTarget).addClass('selected');
        var actualChild = $(event.currentTarget);
        var id = $(actualChild).attr('id');
        $scope.offers = [];
        if (id == 'All') {
          $scope.showAllOffers();
        } else {
          $scope.offers = $scope.allOffers.find(function(i) {return i.Name === id} )[0].Details;
        }
  
        // $scope.$apply();
  
        setTimeout(function () {
          var item = $('.focusable-offer').eq(0);
          focusController.focus(item);
        }, 20);
      };
  
      $scope.scrollHorizontal = function (parent, child) {
        parent.animate(
          { scrollLeft: $(child).index() * $(child).width() - $(child).width() },
          500
        );
        return false;
      };
  
      $scope.scroll = function () {
        var outerContent = $('.offer-scroll-list');
        var innerContent = $('.offer3');
        outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);
      };
  
      // $scope.showAllOffers = function () {
      //   $scope.allOffers.forEach((element) => {
      //     element.Details.forEach((item) => {
      //       $scope.offers.push(item);
      //     });
      //   });
      // };
  
      $scope.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
      };
  
          $scope.init();
    },
  ]);
  
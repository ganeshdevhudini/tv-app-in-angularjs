app.controller("treatmentController", [
    "treatmentService",
    "$location",
    "$scope",
    "$rootScope",
    "focusController",
    "GlobalService",
    "configService",
    function (
        treatmentService,
      $location,
      $scope,
      $rootScope,
      focusController,
      GlobalService,
      configService
    ) {
      $scope.showDetail = false;
      $scope.imageSize = configService.ImageSize;
      $scope.imgURL = configService.imageUrl;
      $scope.spa = [];
      $scope.offersTypes = ['All'];
      $scope.randomId = '';
      
    
      $scope.init = function () {
        $rootScope.isNavMenuRequired = true;
        $scope.randomId = 's' + $scope.getRandomInt(0, 100000);
        sessionStorage.setItem('spaDetail', JSON.stringify(''));
        $scope.gettreatmenttypeInfo();
      };
  
          $scope.gettreatmenttypeInfo = function () {
            debugger;
        $scope.treatmenttype = [];
        $scope.treatment=[];
        $scope.spaId = JSON.parse(sessionStorage.getItem('spaId'));
        //$scope.spaDetail = $scope.spa.filter(function(x) { return x.Id == spaId })[0];
        ////function(x) { return x.id === role.id; }
        //$scope.treatment = $scope.spa.filter(function(x) { return x.Id == spaId })[0].TreatmentType[0].Treatment;
        //$scope.treatmentType = $scope.spa.filter(function(x) { return x.Id == spaId })[0].TreatmentType[0];
        // console.log($scope.spaDetail);
        // console.log($scope.treatment);
        // $('#spaMain').hide();
        $('.loader-wrapper').show();
        treatmentService.getTreatementType($scope.spaId).then(function success(response) {
            $scope.treatmenttype = response.data.responsedata;

            console.log($scope.treatmenttype);
            if ($scope.treatmenttype.length != 0)
            {
              debugger;
                $scope.treatmenttypeId = $scope.treatmenttype[0].id;
                $scope.treatId = $scope.treatmenttypeId;
                $scope.currency=$rootScope.currency;
                treatmentService.getTreatement( $scope.treatmenttypeId, $scope.spaId).then(function success(response) {
                    $scope.treatment = response.data.responsedata;
                   // $scope.treatment=
                    console.log($scope.treatment);
                    $scope.treatment = $scope.treatment.filter(function(x) { return x.active == true });
                    $('.loader-wrapper').hide();
                   $scope.updateImage();
                    // setTimeout(function () {
                    //     $('.page').removeClass('focused');
                    //     focusController.setDepth(0, "spaMenuGroup");
                    //     focusController.focus($('#menuItemId0'));
                    //     //jQuery('#TreatmentMain').trigger('reload');
                    // }, 200);
                    // setTimeout(function () {
                    //     $('.top_menu').find('li').each(function () {
                    //         $(this).removeClass('active');
                    //         $('#menuItemId0').addClass('active');
                    //     });
                    // }, 400);
                    setTimeout(function(){
                              $('.spaType').removeClass('selected');
                               $('.spaType').eq(0).addClass('selected');
              
                               var item = $('.focusable-spa').eq(0);
                              focusController.focus(item);
                             },200)

                }, function error(response) {
                    console.log(response);
                    $('.loader-wrapper').hide();
                    $location.path('/');
                    // chromeGlobalService.focus = true;
                    setTimeout(function () {
                        debugger;
                        // $location.path('/');
    
                        // window.location.reload()
    
                        location.reload();
                        // focusController.setDepth(0, "home");
                        // focusController.focus('hometile0')
                    }, 200);
                });
            }
            


        }, function error(response) {
            console.log(response);
            $('.loader-wrapper').hide();
            $location.path('/');
            // chromeGlobalService.focus = true;
            setTimeout(function () {
                debugger;
                // $location.path('/');

                // window.location.reload()

                location.reload();
                // focusController.setDepth(0, "home");
                // focusController.focus('hometile0')
            }, 200);
        });
        // $scope.offersTypes = ['All'];
        // $scope.offers = [];
  
      
       
      };
      $scope.updatespaImage = function () {
        var item=0;
        $scope.spa.forEach(function(element) {
            if (element.webimages.length > 0 && element.webimages[0].fileattachmentid) {
                element.webimages[0].metadata = $scope.getspaImage(item, element.webimages[0].fileattachmentid);
            }
            item=item+1;
        });
    }
    $scope.updateImage = function () {
      debugger;
      var item=0;
      $scope.treatment.forEach(function(element)  {
          if (element.webimages.length > 0 && element.webimages[0].fileattachmentid) {
              element.webimages[0].metadata = $scope.getImage(item, element.webimages[0].fileattachmentid);
          }
          item=item+1;
      });
  }

  $scope.getImage = function (index, id) {
    treatmentService.getSpaImage(id).then(function success(response) {
        if (response) {
            var reader = new FileReader();
            reader.readAsDataURL(new Blob([response.data], {type:'image/png'}));
           //reader.readAsDataURL(response);
            reader.onload = function(e) {
                $scope.$apply(function() {
                    $scope.treatment[index].webimages[0].metadata = reader.result;
                });
            };
          }
    }, function error(response) {
        console.log(response);
    });
}
    $scope.getspaImage = function (index, id) {
      spaService.getSpaImage(id).then(function success(response) {
          if (response) {
              debugger;
             // var urlCreator = window.URL || window.webkitURL;
              //var imageUrl = urlCreator.createObjectURL(new Blob([response.data],{type:'image/png'}));
              var reader = new FileReader();
             reader.readAsDataURL(new Blob([response.data], {type:'image/png'}));
             //reader.readAsDataURL(new Blob([response.data],{type:'image/png'}));
             //let v=URL.createObjectURL(new Blob([response.data],{type:'image/png'}));
             reader.onload = function(e) {
                  $scope.$apply(function() {
                      $scope.spa[index].webimages[0].metadata = reader.result;
                  });
              };
            }
      }, function error(response) {
          console.log(response);
      });
  }
      $scope.offerFocused = function (event) {
        var parent = $('.spa-list');
        var child = $(event.currentTarget);
        $scope.scrollHorizontal(parent, child);
      };
  
      $scope.TypeSelected = function (TypeId, spaId, menuid, index) {
        debugger;
        $scope.selectMenuIndex = menuid;
      
        $scope.spaId = spaId
        $scope.TypeId = TypeId;
        //$scope.treatment = $scope.spa.filter(function(x) { return x.Id == spaId })[0].TreatmentType.filter(function(x) { return x.TypeId == TypeId })[0].Treatment;
        //$scope.treatmentType = $scope.spa.filter(function(x) { return x.Id == spaId })[0].TreatmentType.filter(function(x) { return x.TypeId == TypeId })[0];

        treatmentService.getTreatement( TypeId, $scope.spaId).then(function success(response) {
            $scope.treatment = response.data.responsedata;
           
            console.log($scope.treatment);
            $scope.treatment = $scope.treatment.filter(function(x) { return x.active == true });
            $scope.updateImage();
            debugger;
            setTimeout(function(){
              $('.spaType').removeClass('selected');
              $('.'+ $scope.selectMenuIndex).addClass('selected');

               var item = $('.focusable-spa').eq(0);
              focusController.focus(item);
             },200)

        }, function error(response) {
            console.log(response);
            $location.path('/');
            // chromeGlobalService.focus = true;
            setTimeout(function () {
                debugger;
                // $location.path('/');

                // window.location.reload()

                location.reload();
                // focusController.setDepth(0, "home");
                // focusController.focus('hometile0')
            }, 200);
        });

    }
      
        focusController.addAfterKeydownHandler(function (context, controller) {
          context.event.stopImmediatePropagation();
          var currenturl = window.location.href;
      currenturl = currenturl.split("#/").pop();
          if(currenturl=='treatment')
          {
          if (context.event.keyCode) {
            var currentFocusableGroup = controller.getCurrentGroup();
            var currenturl = window.location.href;
            currenturl = currenturl.split("#/").pop();
            if (currenturl=="treatment" ) {
              switch (context.event.keyCode) {
                
               
                  case 461:
                    
                      
                       
                       if($rootScope.spalength==1)
                       {
                        $location.path('/');
                       }
                       else{
                        $location.path('/spa');
                       }
                       
                       
    
                      
                    
                 
                  break;
                  case 9:
                    
                       
                       
                       
                    if($rootScope.spalength==1)
                    {
                     $location.path('/');
                    }
                    else{
                     $location.path('/spa');
                    }
    
                     
                  break;
                default:
                  // console.log('Key code : ' + context.event.keyCode);
                  break;
              }
            }
          }
        }
        });
      
      
    
      $scope.offerlifocus=function(event)
      {
        if($('.focusable-offer').hasClass('focused'))
        {
          $(".offer-list").scrollCenter(".focused", 700);
        }
      }
      $scope.offerTypeSelected = function (event) {
        $('.offerType').removeClass('selected');
        $(event.currentTarget).addClass('selected');
        var actualChild = $(event.currentTarget);
        var id = $(actualChild).attr('id');
        $scope.offers = [];
        if (id == 'All') {
          $scope.showAllOffers();
        } else {
          $scope.offers = $scope.allOffers.filter(function(i) {return i.Name === id} )[0].Details;
        }
  
        // $scope.$apply();
  
        setTimeout(function () {
          var item = $('.focusable-offer').eq(0);
          focusController.focus(item);
        }, 20);
      };
  
      $scope.scrollHorizontal = function (parent, child) {
        parent.animate(
          { scrollLeft: $(child).index() * $(child).width() - $(child).width() },
          500
        );
        return false;
      };
  
      $scope.scroll = function () {
        var outerContent = $('.offer-scroll-list');
        var innerContent = $('.offer3');
        outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2);
      };
  
      // $scope.showAllOffers = function () {
      //   $scope.allOffers.forEach((element) => {
      //     element.Details.forEach((item) => {
      //       $scope.offers.push(item);
      //     });
      //   });
      // };
  
      $scope.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
      };
  
          $scope.init();
    },
  ]);
  
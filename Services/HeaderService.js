﻿app.factory('HeaderService', function ($http, Service) {
    return {
       RegisterDevice: function (token, requestData) {
           return $http.post(Service.RequestUrl() + "QRDevice/RegisterRoomDevice", requestData,
                {
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }
                })
        },
    }
});
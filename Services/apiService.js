app.factory('apiService',function($http, configService, GlobalService){
    return {
        getSignature: function (RequestData) {
            var xDeviceId = this.getItem('macId');
            var AppId = configService.AppId;
            var RequestTime = '' + Math.floor(Date.now() / 1000);
            var RequestId = configService.getRandomId();
            var HudiniRefId = configService.AppKey;

            if(RequestData) {
                var concat = AppId + RequestTime + RequestId + JSON.stringify(RequestData);
            } else {
                var concat = AppId + RequestTime + RequestId;
            }
            
            concat = concat.replace(/"/g, '');
            concat = concat.replace(/'/g, '');
            concat = concat.replace(/(\r\n|\n|\r)/gm, '');
            concat = concat.replace(/\s/g, '');

            var result = window.btoa(unescape(encodeURIComponent(concat)));
            var secret = window.btoa(unescape(encodeURIComponent(HudiniRefId)));
            var hash = CryptoJS.HmacSHA256(result, secret);
            var sig64 = (hash.toString(CryptoJS.enc.Hex));
            var signature = window.btoa(sig64);
            return {
                
                'Content-Type': 'application/json; charset=utf-8',
                'X-Signature': signature,
                'X-Deviceid': xDeviceId,
                'X-Macid': xDeviceId,
                'X-Appid': AppId,
                'X-Devicetype': configService.Device.DeviceType,
                'X-Devicename': configService.Device.DeviceName,
                'X-Requestid': RequestId,
                'X-Requesttime': RequestTime,
                'X-Languagecode': sessionStorage.getItem("LanguageCode") ? JSON.parse(sessionStorage.getItem("LanguageCode")) : 'en-IN',
                'X-Hotel': this.getItem('HotelId'),
                'X-Brand': this.getItem('BrandId'),
                'X-Group': this.getItem('GroupId'),
                'Authorization': 'Bearer ' + JSON.parse(sessionStorage.getItem(configService.AuthToken)),
                'X-Userid': this.getItem('pish'),
            };
        },
        getItem: function (key) {
            var item = sessionStorage.getItem(key);
            var res = item !== null ? JSON.parse(item) : "";
            return res;
        },
    }
});
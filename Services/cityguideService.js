app.factory('cityguideService',function($http, configService, apiService){
    return {
        getLandingOffers:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/offers/landing/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getcityguideInfo:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/cityguide/info/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
    }
});
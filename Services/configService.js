app.factory('configService', function () {
    return {
        appVersion: 'v717demo1',
        USERDATA_KEY: 'authf649fc9a5f55',
        // apiUrl : 'http://20.203.4.169:82/api/v2', /* Staging */
        // imageUrl:'http://20.203.4.169:82',
        // apiUrl : 'https://internalapi-emaar.hudini.io/api/v2', /* Staging */
        // imageUrl:'https://internalapi-emaar.hudini.io',
       apiUrl : 'https://mytv.emaar.com/api/v2', /* Production */
       imageUrl:'https://mytv.emaar.com',
        LanguageCode: "en",
        LanguageName:"English",
        //UDID: "12-23-34-56-57-781239950",//for palace
        UDID: SerialNo, //for marina
        UserId: "LkTnkGfLWJNh/GjlmMvTMFHUWb8vYUr+FhSpOx8L3PU=",
        ImageSize: "3x",
        HomeQRData:"https://internalapi-emaar.hudini.io/api/v2",
        TemperatureUnit: "C",
        Device:{
            "DeviceType": "Admin",
            "DeviceName": "Hudini.Admin.Product",
          },
        AppId: "489f609de01e418fa2601fca52df6aa3",
        AppKey: "A93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=",
        membershipId : "",
        BuildVersion:"Version v1.0.34",
        AuthToken:"auth",
        getRandomId: function () {
            var chars = '0123456789abcdef'.split('');

            var uuid = [], rnd = Math.random, r;
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4'; // version 4
    
            for (var i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | rnd() * 16;
    
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
                }
            }
    
            return uuid.join('');
        },
        macId: '',
    }
});
﻿app.factory('homeService', function ($http, Service) {
    return {
        getLandingTile: function (token, requestData) {
            return $http.post(Service.RequestUrl() + "LandingTileDefinition/GetAll", requestData,
			{
			    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }
			})
        },
        getRegisterDevice: function (token, requestData) {
            return $http.post(Service.RequestUrl() + "QRDevice/RegisterRoomDevice", requestData,
                 {
                     headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }
                 })
        },
        getqrcode: function (token, requestData) {
            return $http.post(Service.RequestUrl() + "QRCode/CheckinQRData", requestData,
			{
			    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }
			})
        },
        GuestCheckOut: function (payload) {
            return $http.post(Service.RequestUrl() + "Reservation/checkout", requestData,
			{
                headers: apiService.getSignature(payload)
			})
        },
    }
});
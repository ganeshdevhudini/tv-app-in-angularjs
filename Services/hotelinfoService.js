app.factory('hotelinfoService',function($http, configService, apiService){
    return {
        getHotelInfo:function() {
            var imageSize = configService.ImageSize;
            return $http.get(configService.apiUrl + "/hotel/info/" + imageSize, 
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getAmenities:function() {
            var imageSize = configService.ImageSize;
            return $http.get(configService.apiUrl + "/amenities/info/" + imageSize, 
			{
			    headers: apiService.getSignature(null)
			})	
        },
    }
});
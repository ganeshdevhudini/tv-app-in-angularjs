app.factory('landingService',function($http, configService, apiService){
    return {
        getRoomInfo:function(payload) {
            return $http.post(configService.apiUrl + "/RoomDevice/Info", payload,
			{
			    headers: apiService.getSignature(payload)
			})
        },
        getDeviceIdRegister:function(payload) {
            return $http.post(configService.apiUrl + "/roomdevice/register", payload,
			{
			    headers: apiService.getSignature(payload)
			})
        },
        getHotelInfo:function() {
            var imageSize = configService.ImageSize;
            var payload='';
            return $http.get(configService.apiUrl + "/hotel/info/" + imageSize,
			{
			    headers: apiService.getSignature(payload)
			})
        },
        getWeather:function(payload) {
            return $http.post(configService.apiUrl + "/weather/info", payload,
			{
			    headers: apiService.getSignature(payload)
			})
        },
    }
});
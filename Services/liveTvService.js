app.factory('liveTvService',function($http, configService, apiService){
    return {
        getGenreInfo:function(payload) {
            return $http.post(configService.apiUrl + "/television/info", payload,
			{
			    headers: apiService.getSignature(payload)
			})
        },
    }
});
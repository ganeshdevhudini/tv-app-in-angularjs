app.factory('offersService',function($http, configService, apiService){
    return {
        getLandingOffers:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/offers/landing/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getOffers:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/offers/info/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
        GuestCheckOut: function (payload) {
            return $http.post(configService.apiUrl + "Reservation/checkout",
			{
                headers: apiService.getSignature(payload)
			})
        },
    }
});
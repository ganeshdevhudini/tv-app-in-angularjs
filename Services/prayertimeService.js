app.factory('prayertimeService',function($http, configService, apiService){
    return {
        getPrayertime:function(payload) {
            return $http.post(configService.apiUrl + "/prayertime", payload,
			{
			    headers: apiService.getSignature(payload)
			})	
        },
    }
});
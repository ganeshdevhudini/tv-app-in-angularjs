app.factory('restaurantService',function($http, configService, apiService){
    return {
        getLandingOffers:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/offers/landing/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getrestaurantInfo:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/restaurant/info/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
    }
});
app.factory('servicerequestService',function($http, configService, apiService){
    return {
        getServiceInfo:function(payload) {
            return $http.post(configService.apiUrl + "/roomservices/info", payload,
			{
			    headers: apiService.getSignature(payload)
			})
        },
    }
});
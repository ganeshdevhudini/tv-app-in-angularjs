app.factory('spaService',function($http, configService, apiService){
    return {
        getLandingOffers:function() {
            var imageSize = '3x';

            return $http.get(configService.apiUrl + "/offers/landing/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getspa:function() {
           // var imageSize = '2x';

            return $http.get(configService.apiUrl + "/spa",
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getSpaImage:function(id) {
            // var imageSize = '2x';
 debugger;
             return $http.get(configService.apiUrl + "/file/image/"+id+"/1200/1200", {responseType: 'arraybuffer'},
             {
                 headers: apiService.getSignature(null)
             })	
         },
    }
});
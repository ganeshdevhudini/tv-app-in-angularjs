app.factory('treatmentService',function($http, configService, apiService){
    return {
        getLandingOffers:function() {
            var imageSize = '2x';

            return $http.get(configService.apiUrl + "/offers/landing/"+imageSize,
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getTreatementType:function(id) {
           // var imageSize = '2x';

            return $http.get(configService.apiUrl +"/spa/"+id+ "/treatmenttype",
			{
			    headers: apiService.getSignature(null)
			})	
        },
        getTreatement:function(id,spaid) {
            // var imageSize = '2x';
 
             return $http.get(configService.apiUrl + "/spa/"+spaid+"/treatmenttype/"+id+"/treatment",
             {
                 headers: apiService.getSignature(null)
             })	
         },
         getSpaImage:function(id) {
            // var imageSize = '2x';
 debugger;
             return $http.get(configService.apiUrl + "/file/image/"+id+"/1200/1200", {responseType: 'arraybuffer'},
             {
                 headers: apiService.getSignature(null)
             })	
         },
    }
});
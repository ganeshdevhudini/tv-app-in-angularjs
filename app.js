var SerialNo;
var ipback = false;
var portchange = false
var app = angular.module('app', [
    'ngRoute',
    //'caph.focus',
    'caph.ui',
    'pascalprecht.translate',
]);

app.config(['$routeProvider', '$translateProvider', function ($routeProvider,$translateProvider) {
    $routeProvider
    .when("/", { templateUrl: "Views/home.html", controller: "homeController" })
    .when("/landing", { templateUrl: "Views/landing.html", controller: "landingController" })
    .when("/hotelinfo", { templateUrl: "Views/hotelinfo.html", controller: "hotelinfoController" })
    .when("/offers", { templateUrl: "Views/offers.html", controller: "offersController" })
    .when("/cityguide", { templateUrl: "Views/cityguide.html", controller: "cityguideController" })
    .when("/offerDetails", { templateUrl: "Views/offerDetails.html", controller: "offerDetailsController" })
    .when("/cityguideDetails", { templateUrl: "Views/cityguideDetails.html", controller: "cityguideDetailsController" })
    .when("/restaurant", { templateUrl: "Views/restaurant.html", controller: "RestaurantController" })
    .when("/restaurantDetails", { templateUrl: "Views/restaurantDetail.html", controller: "restaurantDetailsController" })

    .when("/livetv", { templateUrl: "Views/liveTv.html", controller: "liveTvController" })
    .when("/serviceRequest", { templateUrl: "Views/Servicerequest.html", controller: "ServicerequestController" })
    .when("/liveTvSearch", { templateUrl: "Views/liveTvSearch.html", controller: "liveTvSearchController" })
    .when("/spa", { templateUrl: "Views/spa.html", controller: "spaController" })
    .when("/treatment", { templateUrl: "Views/treatment.html", controller: "treatmentController" })
    .when("/notification", { templateUrl: "Views/notification.html", controller: "notificationController" })
    .when("/prayertiming", { templateUrl: "Views/prayertime.html", controller: "prayertimeController" })

    var en_translations = {
        "Global": {
            "Ok" : "Ok",
            "Add": "Add",
            "Home": "Home",
            "Submit": "Submit",
            "Confirm": "Confirm",
            "Cancel": "Cancel",
            "Today": "Today",
            "Tomorrow": "Tomorrow",
            "SelectAvailableSlots": "Select available slots",
            "NoSlotsAvailable": "No Slots Available",
            "NoDataFound": "No Data Found"
        },
        "Welcome": {
            "Hi": "Dear",
            "message1":"Greetings from Address Downtown.",
            "message2":"Wishing you a pleasant stay with us.",
            "selectLanguage": "Select Language"
        },
        "Home": {
            "RoomNo": "Room No",
            "QRText": "Scan the QR Code to Download Emmar Mobile App"
        },
        "LiveTV": {
            "ChannelGuide": "Channel Guide",
            "All": "All", 
            "AllChannels": "All Channels",
            "SearchResult": "Search Result"
        },
        "IRD": {
            "YouMayLikeThis": "You may like this",
            "NoRefundText": "Orders once placed cannot be cancelled and are non-refundable.",
            "Total": "Total",
            "Order": "Order",
            "AddNew": "Add New",
            "AddToCart": "Add to cart",
            "ThankyouTitle": "Thank you for your order",
            "ThankyouMessage": "Your order has been received and will be delivered soon.",
            "BackToDining": "Back to Dining"
        },
        "HouseKeeping": {
            "ThankyouTitle": "Thank you for your request",
            "ThankyouPreMessage": "Your request",
            "ThankyouPostMessage1": "has been received and will be delivered shortly.",
            "ThankyouPostMessage2": "has been received and will be delivered shortly"
        },
        "Spa": {
            "BookNow": "Book Now",
            "ThankyouTitle": "Thank you for your booking",
            "SelectedTherapy": "Selected Therapy",
            "NoOfPeople": "No. of Guests",
            "Guest": "Guest",
            "Guests": "Guests",
            "RoomNo": "Room No",
            "BackToSpa": "Back to Spa"
        },
        "Restaurant": {
            "ViewDetails": "View Details",
            "TableBooking": "Table booking",
            "NoOfPeople": "No. of Guests",
            "ViewMenu": "View Menu",
            "BookATable": "Book a table",
            "ThankyouTitle": "Thank you for your booking",
            "Resturant": "Restaurant",
            "Guest": "Guest",
            "Guests": "Guests",
            "RoomNo": "Room No",
            "BackToResturant": "Back to Restaurant"
        },
        "PrayerTiming": {
            "PrayerTimes": "PRAYER TIMES",
            "Upcomingprayer": "Upcoming prayer"
        },
        "MyAccount": {
            "GuestDetails": "Guest Details",
            "MyBooking": "My Booking",
            "RoomNo": "Room No",
            "GuestName": "Guest Name",
            "Language": "Language",
            "CheckedIn": "Checked-in",
            "CheckOut": "Check-out",
            "LateCheckOut": "Late check-out",
            "ExtendStay": "extend stay",
            "BookingHistory": "Booking History",
            "Guest": "Guest",
            "Guests": "Guests",
            "Modify": "Modify",
            "ConfirmMessage": "Kindly confirm your cancellation",
            "ModifyBookings": "Modify bookings",
            "BookedDate": "Booked Date",
            "BookedTime": "Booked Time",
            "ExtendStayTitle": "Extend Stay",
            "ExtendStayMessage": "Extend stay request is subject to availability and daily rate may apply, our team member will contact to shortly"
        }
    }

    var ar_translations = {
        "Global": {
          "Ok": "تمام",
          "Add": "يضيف",
          "Home": "منزل، بيت",
          "Submit": "إرسال",
          "Confirm": "يتأكد",
          "Cancel": "يلغي",
          "Today": "اليوم",
          "Tomorrow": "غدا",
          "SelectAvailableSlots": "حدد الفتحات المتاحة",
          "NoSlotsAvailable": "لا توجد فتحات متاحة",
          "NoDataFound": "لاتوجد بيانات"
        },
        "Welcome": {
          "Hi": "أهلا",
          "message": "نتمنى لك إقامة ممتعة معنا!",
          "selectLanguage": "اختار اللغة"
        },
        "Home": {
          "RoomNo": "رقم الغرفة",
          "QRText": "امسح رمز الاستجابة السريعة لتنزيل"
        },
        "LiveTV": {
            "ChannelGuide": "دليل القناة",
            "All": "الجميع", 
            "AllChannels": "جميع القنوات",
            "SearchResult": "نتيجة البحث"
        },
        "IRD": {
            "YouMayLikeThis": "قد يعجبك هذا",
            "NoRefundText": "الطلبات بمجرد تقديمها لا يمكن إلغاؤها وغير قابلة للاسترداد.",
            "Total": "مجموع",
            "Order": "ترتيب",
            "AddNew": "اضف جديد",
            "AddToCart": "أضف إلى السلة",
            "ThankyouTitle": "شكرا لطلبك",
            "ThankyouMessage": "تم استلام طلبك وسيتم تسليمه قريبًا.",
            "BackToDining": "العودة إلى الطعام"
        },
        "HouseKeeping": {
            "ThankyouTitle": "شكرا لطلبك",
            "ThankyouPreMessage": "طلبك",
            "ThankyouPostMessage1": "تم استلامه وسيتم تسليمه قريبًا.",
            "ThankyouPostMessage2": "تم استلامه وسيتم تسليمه "
        },
        "Spa": {
            "BookNow": "احجز الآن",
            "ThankyouTitle": "شكرا لك على الحجز",
            "SelectedTherapy": "العلاج المختار",
            "NoOfPeople": "عدد الاشخاص",
            "Guest": "زائر",
            "Guests": "زائر",
            "RoomNo": "رقم الغرفة",
            "BackToSpa": "العودة إلى سبا"
        },
        "Restaurant": {
            "ViewDetails": "عرض التفاصيل",
            "TableBooking": "حجز طاولة",
            "NoOfPeople": "عدد الاشخاص",
            "ViewMenu": "عرض القائمة",
            "BookATable": "احجز طاولة",
            "ThankyouTitle": "شكرا لك على الحجز",
            "Resturant": "مطعم",
            "Guest": "زائر",
            "Guests": "زائر",
            "RoomNo": "رقم الغرفة",
            "BackToResturant": "العودة للمطعم"
        },
        "PrayerTiming": {
            "PrayerTimes": "أوقات الصلاة",
            "Upcomingprayer": "الصلاة القادمة"
        },
        "MyAccount": {
            "GuestDetails": "تفاصيل الضيف",
            "MyBooking": "الحجز",
            "RoomNo": "رقم الغرفة",
            "GuestName": "اسم الضيف",
            "Language": "لغة",
            "CheckedIn": "تحقق في",
            "CheckOut": "الدفع",
            "LateCheckOut": "تحقق متأخر",
            "ExtendStay": "تمديد البقاء",
            "BookingHistory": "سجل الحجز",
            "Guest": "زائر",
            "Guests": "زائر",
            "Modify": "تعديل",
            "ConfirmMessage": "يرجى تأكيد الإلغاء الخاص بك",
            "ModifyBookings": "تعديل الحجوزات",
            "BookedDate": "تاريخ الحجز",
            "BookedTime": "الوقت المحجوز",
            "ExtendStayTitle": "تمديد البقاء",
            "ExtendStayMessage": "طلب تمديد الإقامة يخضع للتوافر وقد يتم تطبيق السعر اليومي ، سيتصل أعضاء فريقنا به قريبًا"
        }
      }
    
    var fr_translations = {
        "Hi": "salut"
    }

    $translateProvider.translations('en-IN', en_translations);
    $translateProvider.translations('ar-AE', ar_translations);
    $translateProvider.translations('fr-FR', fr_translations);
    $translateProvider.translations('en', en_translations);
    $translateProvider.preferredLanguage('en-IN');
  

}]);
//a directive to 'enter key press' in elements with the "ng-enter" attribute
app.directive('ngEnter', function () { 
    return function (scope, element, attrs) {
        element.bind("keydown", function (event) {
            if (event.which === Keys.EnterKey) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
})
app.directive('ngFocus', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngFocus']);
        element.on('focus', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    };
}])
app.directive('ngBlur', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngBlur']);
        element.on('blur', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    };
}]);
app.directive('autoFocus', function ($timeout) {
    return {
        restrict: 'AC',
        link: function (_scope, _element) {
            $timeout(function () {
                _element[0].focus();
            }, 0);
        }
    };
});
app.directive('onErrorSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.onErrorSrc) {
                    attrs.$set('src', attrs.onErrorSrc);
                }
            });
        }
    }
});


jQuery.fn.scrollCenter = function (elem, speed) {
    debugger;
    var active = jQuery(this).find(elem); // find the active element
    //var activeWidth = active.width(); // get active width
    if (active.length != 0) {
        var activeWidth = active.width() / 2; // get active width center

        var pos = active.position().left + activeWidth; //get left position of active li + center position
        var elpos = jQuery(this).scrollLeft(); // get current scroll position
        var elW = jQuery(this).width(); //get div width
        //var divwidth = jQuery(elem).width(); //get div width
        pos = pos + elpos - elW / 2; // for center position if you want adjust then change this

        jQuery(this).animate({
            scrollLeft: pos
        }, speed == undefined ? 1000 : speed);
        return this;
    }

};
jQuery.fn.cityscrollCenter = function (elem, speed) {
    debugger;
    var active = jQuery(this).find(elem); // find the active element
    //var activeWidth = active.width(); // get active width
    if (active.length != 0) {
        var activeWidth = active.width() / 2; // get active width center

        var pos = active.position().left + activeWidth; //get left position of active li + center position
        var elpos = jQuery(this).scrollLeft(); // get current scroll position
        var elW = jQuery(this).width(); //get div width
        //var divwidth = jQuery(elem).width(); //get div width
        pos = pos + elpos - elW/2; // for center position if you want adjust then change this

        jQuery(this).animate({
            scrollLeft: pos
        }, speed == undefined ? 1000 : speed);
        return this;
    }

};
jQuery.fn.scrollCenterVertical = function (elem, speed) {

    // this = #timepicker
    // elem = .active
    var active = jQuery(this).find(elem); // find the active element
    //var activeWidth = active.width(); // get active width
    if (active.length != 0) {
        var activeHeight = active.height() / 2; // get active width center

        //alert(activeWidth)

        //var pos = jQuery('#timepicker .active').position().left; //get left position of active li
        // var pos = jQuery(elem).position().left; //get left position of active li
        //var pos = jQuery(this).find(elem).position().left; //get left position of active li
        var pos = active.position().top + activeHeight; //get left position of active li + center position
        var elpos = jQuery(this).scrollTop(); // get current scroll position
        var elW = jQuery(this).height(); //get div width
        //var divwidth = jQuery(elem).width(); //get div width
        pos = pos + elpos - elW /2; // for center position if you want adjust then change this

        jQuery(this).animate({
            scrollTop: pos
        }, speed == undefined ? 1000 : speed);
        return this;
    }

};
// http://podzic.com/wp-content/plugins/podzic/include/js/podzic.js
jQuery.fn.scrollCenterORI = function (elem, speed) {

    jQuery(this).animate({
        scrollLeft: jQuery(this).scrollLeft() - jQuery(this).offset().left + jQuery(elem).offset().left
    }, speed == undefined ? 1000 : speed);
    return this;
};
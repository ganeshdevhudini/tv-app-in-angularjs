/* CAPH v3.0.1-160315 @ 2016-03-15 19:12:36 */

/**
 * Copyright (c) 2014 Samsung Electronics Co., Ltd All Rights Reserved
 * PROPRIETARY/CONFIDENTIAL
 *
 * This software is the confidential and proprietary information of SAMSUNG
 * ELECTRONICS ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the terms of
 * the license agreement you entered into with SAMSUNG ELECTRONICS. SAMSUNG make
 * no representations or warranties about the suitability of the software, either
 * express or implied, including but not limited to the implied warranties of
 * merchantability, fitness for a particular purpose, or non-infringement.
 * SAMSUNG shall not be liable for any damages suffered by licensee as a result
 * of using, modifying or distributing this software or its derivatives.
 */

"use strict";
function FocusControllerProvider(a) {
    function b(a, b) {
        return angular.isFunction(b) ? (a.push(b), !0) : !1;
    }
    function c(a, b) {
        var c = a.indexOf(b);
        return c > -1 ? (a.splice(c, 1), !0) : !1;
    }
    function d(a, c) {
        return b(o, a) ? ((a.order = c || 0), o.sort(e), !0) : !1;
    }
    function e(a, b) {
        return a.order - b.order;
    }
    function f(a) {
        return b(p, a);
    }
    function g(a) {
        return c(o, a);
    }
    function h(a) {
        return c(p, a);
    }
    function i() {
        return angular.copy(n);
    }
    var j = ".focus-controller",
        k = "group:",
        l = a.DEFAULT.DEPTH,
        m = {},
        n = a.DEFAULT.KEY_MAP,
        o = [],
        p = [],
        q = !1;
    (m[a.DEFAULT.DEPTH] = a.DEFAULT.GROUP),
        (this.setInitialDepth = function (a) {
            return (l = a), this;
        }),
        (this.setInitialGroupOfDepth = function (b, c) {
            var d = {};
            return void 0 === c && ((c = b), (b = a.DEFAULT.DEPTH)), angular.isObject(c) ? (d = c) : (d[b] = c), angular.extend(m, d), this;
        }),
        (this.setKeyMap = function (a) {
            return angular.extend(n, a), this;
        }),
        (this.getKeyMap = i),
        (this.addBeforeKeydownHandler = d),
        (this.addAfterKeydownHandler = f),
        (this.removeBeforeKeydownHandler = g),
        (this.removeAfterKeydownHandler = h),
        (this.setFocusWhenDisabled = function (a) {
            return (q = a), this;
        }),
        (this.isFocusWhenDisabled = function () {
            return q;
        }),
        (this.$get = [
            "$rootScope",
            "$window",
            "$document",
            "$timeout",
            "$animate",
            "FocusUtil",
            "nearestFocusableFinder",
            function (b, c, e, r, s, t, u) {
                function v(a) {
                    var b, c, d, e;
                    if (H) {
                        if (((c = t.getData(H).nextFocus[a]), void 0 === c)) return u.getNearest(H, a);
                        if (c) {
                            if (0 === c.indexOf(k)) return c.replace(k, "");
                            if (((d = u.$$get(c)), t.isVisible(d) && ((e = t.getData(d)), (!e.disabled || q) && t.getData(H).depth === e.depth))) return d;
                        }
                        return null;
                    }
                    return (b = C(L)), b ? b : w();
                }
                function w(a, b) {
                    return C(K, a, b) || u.getInitial(D(a), E(b));
                }
                function x(a, b) {
                    var c;
                    t.isFocusable((a = t.getElement(a))) && ((c = t.getData(a)), c.depth === I && (A(a, "blurred", [b]), a === H && (B(L, a), (H = null))));
                }
                function y(a, b) {
                    var c;
                    t.isFocusable((a = t.getElement(a))) && a !== H && ((c = t.getData(a)), c.depth !== I || (c.disabled && !q) || (H && x(H, b), A(a, "focused", [b]), (J = c.group), (H = a)));
                }
                function z(a, b) {
                    t.isFocusable((a = t.getElement(a))) && t.isVisible(a) ? (a !== H || t.getData(a).disabled ? y(a, b) : A(a, "selected", [b])) : ((a = C(L)), a ? y(a, b) : y((a = w()), b));
                }
                function A(a, b, c) {
                    a && jQuery(a).trigger(b, c);
                }
                function B(a, b, c, d) {
                    (c = D(c)), (d = E(d)), (a[c] = a[c] || {}), (a[c][d] = b);
                }
                function C(a, b, c) {
                    var d;
                    return (b = D(b)), (c = E(c)), !t.isVisible((d = a[b] && a[b][c])) || (t.getData(d).disabled && !q) ? null : d;
                }
                function D(a) {
                    return void 0 === a ? I : a;
                }
                function E(a) {
                    return void 0 === a ? J : a;
                }
                function F(a) {
                    return a ? (angular.isString(a) ? u.$$get(a) : a) : H;
                }
                function G(a, b, c) {
                    var d;
                    t.isFocusable((a = t.getElement(a))) && ((d = t.getData(a)), d.disabled !== b && ((d.disabled = b), t.setData(a, d), c && c(a)));
                }
                var H,
                    I = l,
                    J = m[l] || a.DEFAULT.GROUP,
                    K = {},
                    L = {},
                    M = {
                        getCurrentDepth: function () {
                            return I;
                        },
                        getCurrentGroup: function () {
                            return J;
                        },
                        getCurrentFocusItem: function () {
                            return H;
                        },
                        setDepth: function (b, c, d) {
                            var e, f;
                            return angular.isNumber(b) && I !== b && ((e = d === !1 ? w(b, (f = m[b] || a.DEFAULT.GROUP)) : C(L, b, (f = c || m[b] || a.DEFAULT.GROUP)) || w(b, f)), e && (x(H), (I = b), (J = f), y(e))), this;
                        },
                        setGroup: function (a) {
                            var b;
                            return a && J !== a && (b = C(L, I, a) || w(I, a)) && (x(H), (J = a), y(b)), this;
                        },
                        focus: function (a, b) {
                            return y(F(a), b), this;
                        },
                        blur: function (a, b) {
                            return x(F(a), b), this;
                        },
                        select: function (a, b) {
                            return z(F(a), b), this;
                        },
                        enable: function (a) {
                            return (
                                G(F(a), !1, function (a) {
                                    s.removeClass(a, "disabled");
                                }),
                                this
                            );
                        },
                        disable: function (a) {
                            return (
                                G(F(a), !0, function (a) {
                                    a === H && x(H, jQuery.Event("disabled")), s.addClass(a, "disabled");
                                }),
                                this
                            );
                        },
                        addBeforeKeydownHandler: d,
                        addAfterKeydownHandler: f,
                        removeBeforeKeydownHandler: g,
                        removeAfterKeydownHandler: h,
                        getKeyMap: i,
                        $$getNextFocusItem: v,
                        $$getInitialFocusItem: w,
                        $$setInitialFocusItem: function (a) {
                            var b;
                            return t.isFocusable((a = t.getElement(a))) && ((b = t.getData(a)), b.initialFocus && B(K, a, b.depth, b.group)), this;
                        },
                    };
                return (
                    jQuery(c).on("load", function (a) {
                        r(function () {
                            y(C(K), a);
                        });
                    }),
                    e.on("keydown" + j, function (c) {
                        var d,
                            e = c.keyCode || c.which || c.charCode;
                        if (
                            o.some(function (a) {
                                return a({ event: c, previousFocusedItem: C(L), currentFocusItem: H }, M) === !1;
                            })
                        )
                            return void b.$applyAsync();
                        switch (e) {
                            case n.LEFT:
                                d = v(a.DIRECTION.LEFT);
                                break;
                            case n.RIGHT:
                                d = v(a.DIRECTION.RIGHT);
                                break;
                            case n.UP:
                                d = v(a.DIRECTION.UP);
                                break;
                            case n.DOWN:
                                d = v(a.DIRECTION.DOWN);
                                break;
                            case n.ENTER:
                                z(H, c);
                        }
                        d && (angular.isString(d) ? M.setGroup(d) : (x(H, c), y(d, c))),
                            p.forEach(function (a) {
                                a({ event: c, previousFocusedItem: C(L), currentFocusItem: H }, M);
                            }),
                            b.$applyAsync();
                    }),
                    b.$on("$destroy", function () {
                        e.off(j);
                    }),
                    M
                );
            },
        ]);
}
function NearestFocusableFinderProvider(a, b) {
    function c(a) {
        var b = jQuery(a),
            c = b.offset();
        return { left: c.left, top: c.top, width: b.width(), height: b.height() };
    }
    var d = {},
        e = [],
        f = a.DEFAULT.DISTANCE_CALCULATION_STRATEGY;
    (d[a.DEFAULT.DISTANCE_CALCULATION_STRATEGY] = (function () {
        function b(b, c) {
            var d = { x: b.left, y: b.top };
            switch (c) {
                case a.DIRECTION.RIGHT:
                    d.x += b.width;
                case a.DIRECTION.LEFT:
                    d.y += b.height / 2;
                    break;
                case a.DIRECTION.DOWN:
                    d.y += b.height;
                case a.DIRECTION.UP:
                    d.x += b.width / 2;
            }
            return d;
        }
        function c(b, c, d) {
            switch (d) {
                case a.DIRECTION.LEFT:
                    return b.x >= c.x;
                case a.DIRECTION.RIGHT:
                    return b.x <= c.x;
                case a.DIRECTION.UP:
                    return b.y >= c.y;
                case a.DIRECTION.DOWN:
                    return b.y <= c.y;
            }
            return !1;
        }
        var d = { left: "right", right: "left", up: "down", down: "up" };
        return function (a, e, f) {
            var g = b(a, f),
                h = b(e, d[f]);
            return c(g, h, f) ? Math.sqrt(Math.pow(g.x - h.x, 2) + Math.pow(g.y - h.y, 2)) : 1 / 0;
        };
    })()),
        (this.registerDistanceCalculationStrategy = function (a, b, c) {
            if (c === !1 && angular.isFunction(d[a])) throw new Error('The given name "' + a + '" is already registered. If you want to override it, please do not pass the third parameter.');
            return (d[a] = b), this;
        }),
        (this.useDistanceCalculationStrategy = function (b) {
            return b ? (angular.isFunction(d[b]) ? (f = b) : console.warn('The given name "' + b + '" is not registered yet. Using "' + f + '" instead.')) : (f = a.DEFAULT.DISTANCE_CALCULATION_STRATEGY), this;
        }),
        (this.getRegisteredDistanceCalculationStrategies = function () {
            return Object.keys(d);
        }),
        (this.getCurrentDistanceCalculationStrategy = function () {
            return f;
        }),
        (this.addBeforeDistanceCalculationHandler = function (a) {
            return angular.isFunction(a) ? (e.push(a), !0) : !1;
        }),
        (this.removeBeforeDistanceCalculationHandler = function (a) {
            var b = e.indexOf(a);
            return b > -1 ? (e.splice(b, 1), !0) : !1;
        }),
        (this.$get = [
            "$rootScope",
            "$document",
            "FocusUtil",
            function (g, h, i) {
                var j,
                    k = Array.prototype.slice,
                    l = {},
                    m = {
                        getInitial: function (c, d) {
                            var e;
                            return (
                                (c = c || a.DEFAULT.DEPTH),
                                (d = d || a.DEFAULT.GROUP),
                                Object.keys(l).some(function (a) {
                                    var f = l[a],
                                        g = i.getData(f);
                                    return !i.isVisible(f) || (g.disabled && !b.isFocusWhenDisabled()) || g.depth !== c || g.group !== d ? void 0 : ((e = f), !0);
                                }),
                                e
                            );
                        },
                        getNearest: function (a, g) {
                            var h,
                                j,
                                k,
                                m,
                                n,
                                o = 1 / 0;
                            return (
                                i.isFocusable((a = i.getElement(a))) &&
                                    (n = i.getData(a)) &&
                                    ((k = c(a)),
                                    Object.keys(l).forEach(function (p) {
                                        var q = l[p],
                                            r = i.getData(q);
                                        if (q !== a && i.isVisible(q) && (!r.disabled || b.isFocusWhenDisabled()) && n.depth === r.depth) {
                                            if (
                                                e.some(function (a) {
                                                    return a(q) === !1;
                                                })
                                            )
                                                return;
                                            (m = c(q)), (h = d[f](k, m, g)), o > h && ((j = q), (o = h));
                                        }
                                    })),
                                j
                            );
                        },
                        $$put: function (a, b) {
                            var c;
                            return i.isFocusable((a = i.getElement(a))) && (c = i.setData(a, b)) && (l[c] = i.getElement(a)), this;
                        },
                        $$get: function (a) {
                            return l[a];
                        },
                        $$remove: function (a) {
                            var b, c;
                            return angular.isString(a) || (a = i.getData(a)), a && ((b = a.name), (c = l[b]), c && delete l[b]), c;
                        },
                    };
                return (
                    window.MutationObserver &&
                        ((j = new MutationObserver(function (a) {
                            a.forEach(function (a) {
                                "childList" === a.type &&
                                    k.call(a.removedNodes).forEach(function (a) {
                                        i.isElement(a) && i.isFocusable(a) && i.unbindEvent(m.$$remove(a));
                                    });
                            });
                        })),
                        j.observe(h[0], { childList: !0, subtree: !0 }),
                        g.$on("$destroy", function () {
                            j.disconnect();
                        })),
                    m
                );
            },
        ]);
}
function ListItemLoaderProvider(a) {
    function b(a) {
        (this.url = a.url),
            (this.method = a.method || "JSONP"),
            (this.headers = a.headers),
            (this.parameterNames = angular.extend({}, e, a.parameterNames || {})),
            (this.loadingCount = a.loadingCount),
            (this.thresholdCount = a.thresholdCount),
            (this.onGetTotalCount = a.getTotalCount || angular.noop),
            (this.onGetItems = a.getItems || angular.identity),
            (this.onBeforeLoad = a.onBeforeLoad || angular.noop),
            (this.onAfterLoad = a.onAfterLoad || angular.noop),
            (this.items = []),
            this.totalCount,
            (this.loading = !1),
            (this.offset = 0),
            this.http,
            (this.initiailzed = !1);
    }
    function c(a, b, c) {
        var d = 0,
            e = [];
        for (b = b || 0, c = c || a.length; c > b; ) e[d++] = a[b++];
        return e;
    }
    var d = {},
        e = { page: "page", offset: "offset", count: "count", callback: "callback" };
    a.addBeforeKeydownHandler(function () {
        return Object.keys(d).some(function (a) {
            return d[a].isLoading() ? !0 : void 0;
        })
            ? !1
            : void 0;
    }, -1e3),
        (b.prototype.isInitialized = function () {
            return this.initiailzed;
        }),
        (b.prototype.isLoading = function () {
            return this.loading;
        }),
        (b.prototype.getTotalCount = function () {
            return this.totalCount;
        }),
        (b.prototype.getCurrentCount = function () {
            return this.items.length;
        }),
        (b.prototype.getLoadingCount = function () {
            return this.loadingCount;
        }),
        (b.prototype.setLoadingCount = function (a) {
            return this.loadingCount || (this.loadingCount = a), this;
        }),
        (b.prototype.getThresholdCount = function () {
            return this.thresholdCount;
        }),
        (b.prototype.setThresholdCount = function (a) {
            return this.thresholdCount || (this.thresholdCount = a), this;
        }),
        (b.prototype.getOffset = function () {
            return this.offset;
        }),
        (b.prototype.setOffset = function (a) {
            return (this.offset = a), this;
        }),
        (b.prototype.setHttp = function (a) {
            return (this.http = a), this;
        }),
        (b.prototype.initialize = function (a) {
            this.initiailzed ||
                this.loading ||
                this.request(
                    function (b, c, d) {
                        if ((this.append(b), (this.offset += this.loadingCount), (this.totalCount = this.onGetTotalCount(c, d)), null == this.totalCount))
                            throw new Error('Check the "getTotalCount" callback function. It should return total item count as a number.');
                        (this.initiailzed = !0), (a || angular.noop)(b, this.totalCount, c, d, this);
                    }.bind(this)
                );
        }),
        (b.prototype.beforeRequest = function () {
            (this.loading = !0), this.onBeforeLoad(this);
        }),
        (b.prototype.afterRequest = function (a, b) {
            if ((this.onAfterLoad(a, b, this), (this.loading = !1), !this.initiailzed)) throw new Error("Fail to request. Please check the given url or connection.");
        }),
        (b.prototype.request = function (a) {
            this.loading ||
                (this.beforeRequest(),
                this.http({ url: this.url, method: this.method, headers: this.headers, params: this.getParams() })
                    .success(
                        function (b, c) {
                            var d = this.onGetItems(b);
                            d && d.length > 0 && (a || angular.noop)(d, b, c, this), this.afterRequest(b, c);
                        }.bind(this)
                    )
                    .error(
                        function (a, b) {
                            this.afterRequest(a, b);
                        }.bind(this)
                    ));
        }),
        (b.prototype.getParams = function () {
            var a = {};
            return (
                (a[this.parameterNames.page] = Math.floor(this.offset / this.loadingCount) + 1),
                (a[this.parameterNames.offset] = this.offset),
                (a[this.parameterNames.count] = this.loadingCount),
                (a[this.parameterNames.callback] = "JSON_CALLBACK"),
                a
            );
        }),
        (b.prototype.isNeededForPrevious = function (a) {
            var b = this.offset - 2 * this.loadingCount;
            return b > 0 && a < b + this.thresholdCount;
        }),
        (b.prototype.isNeededForNext = function (a) {
            var b = this.getCurrentCount();
            return b < this.getTotalCount() && b - this.thresholdCount < a;
        }),
        (b.prototype.get = function (a) {
            if (!this.initiailzed) throw new Error("Do not ready to use. Please use after initializing properly. Also verify whether the given url is available or not.");
            return this.items[a];
        }),
        (b.prototype.prepend = function (a) {
            var b;
            this.offset > 0 && ((b = []), (b.length = this.offset - 2 * this.loadingCount), (this.items = b.concat(a).concat(c(this.items, this.offset - this.loadingCount, this.offset))));
        }),
        (b.prototype.append = function (a) {
            var b, d;
            this.offset > this.loadingCount ? ((d = this.offset - this.loadingCount), (b = []), (b.length = d), (this.items = b.concat(c(this.items, d)).concat(a))) : (this.items = this.items.concat(a));
        }),
        (this.register = function (a, c) {
            if (d[a]) throw new Error("The given name " + a + " already has been registered.");
            return (d[a] = new b(c || {})), this;
        }),
        (this.unregister = function (a) {
            return delete d[a], this;
        }),
        (this.$get = [
            "$http",
            function (a) {
                return {
                    get: function (b) {
                        var c = d[b];
                        return c && !c.isInitialized() && c.setHttp(a), c;
                    },
                };
            },
        ]);
}
angular.module(
    "caph.focus",
    [],
    [
        "$provide",
        "$compileProvider",
        function (a, b) {
            function c(a) {
                return a && a + "";
            }
            function d(a) {
                var b = jQuery(a).data(),
                    d = {};
                return (
                    Object.keys(i.DIRECTION).forEach(function (a) {
                        var e = i.DIRECTION[a],
                            f = "focusableNextFocus" + h(e),
                            g = b[f];
                        (g || null === g) && (d[e] = c(g));
                    }),
                    { depth: b.focusableDepth, group: c(b.focusableGroup), name: c(b.focusableName), initialFocus: b.focusableInitialFocus, disabled: b.focusableDisabled, nextFocus: d }
                );
            }
            function e(a, b) {
                return (
                    (a = jQuery(a)),
                    a.data({
                        focusableDepth: angular.isNumber(b.depth) ? b.depth : i.DEFAULT.DEPTH,
                        focusableGroup: b.group || i.DEFAULT.GROUP,
                        focusableName: b.name || k.get(),
                        focusableInitialFocus: b.initialFocus === !0,
                        focusableDisabled: b.disabled === !0,
                    }),
                    b.nextFocus &&
                        Object.keys(b.nextFocus).forEach(function (c) {
                            var d = "focusableNextFocus" + h(c),
                                e = b.nextFocus[c];
                            (e || null === e) && a.data(d, e);
                        }),
                    a.data("focusableName")
                );
            }
            function f(a) {
                return a && a instanceof jQuery ? a[0] : a;
            }
            function g(a) {
                return a && a.nodeType === Node.ELEMENT_NODE ? !0 : !1;
            }
            function h(a) {
                return a ? a.charAt(0).toUpperCase() + a.substr(1) : a;
            }
            var i = { DIRECTION: { LEFT: "left", RIGHT: "right", UP: "up", DOWN: "down" }, DEFAULT: { DEPTH: 0, GROUP: "default", KEY_MAP: { LEFT: 37, RIGHT: 39, UP: 38, DOWN: 40, ENTER: 13 }, DISTANCE_CALCULATION_STRATEGY: "default" } },
                j = 0,
                k = {
                    get: function () {
                        return "focusable-" + j++;
                    },
                },
                l = ".focusable",
                m = { mouseover: "focus", mouseout: "blur", click: "select" },
                n = ["focused", "blurred", "selected"],
                o = {
                    NameGenerator: k,
                    isFocusable: function (a) {
                        return g((a = f(a))) && null !== a.getAttribute("focusable");
                    },
                    isElement: function (a) {
                        return g(f(a));
                    },
                    isVisible: function (a) {
                        if (g((a = f(a)))) {
                            for (; a; ) {
                                if ("none" === jQuery.css(a, "display") || "hidden" === jQuery.css(a, "visibility")) return !1;
                                a = a.parentElement;
                            }
                            return !0;
                        }
                        return !1;
                    },
                    getElement: function (a) {
                        return g((a = f(a))) ? a : null;
                    },
                    getData: function (a) {
                        return o.isFocusable(a) ? d(f(a)) : null;
                    },
                    setData: function (a, b) {
                        return o.isFocusable(a) ? e((a = f(a)), (b && angular.extend(d(a), b)) || d(a)) : null;
                    },
                    bindMouseEvent: function (a, b) {
                        return (
                            o.isElement(a) &&
                                Object.keys(m).forEach(function (c) {
                                    jQuery(a).on(c + l, { method: m[c] }, b);
                                }),
                            this
                        );
                    },
                    bindFocusableEvent: function (a, b) {
                        return (
                            o.isElement(a) &&
                                n.forEach(function (c) {
                                    jQuery(a).on(c + l, b);
                                }),
                            this
                        );
                    },
                    unbindEvent: function (a) {
                        return o.isElement(a) && jQuery(a).off(l), this;
                    },
                };
            a.constant("FocusConstant", i), a.constant("FocusUtil", o), a.provider({ focusController: FocusControllerProvider, nearestFocusableFinder: NearestFocusableFinderProvider }), b.directive("focusable", focusableDirective);
        },
    ]
),
    (FocusControllerProvider.$inject = ["FocusConstant"]);
var focusableDirective = [
    "$parse",
    "$interpolate",
    "$animate",
    "FocusUtil",
    "nearestFocusableFinder",
    "focusController",
    function (a, b, c, d, e, f) {
        function g(a, b) {
            return angular.isString(a) && 0 === a.indexOf(b);
        }
        function h(a, b) {
            return angular.isString(a) && a.indexOf(b) === a.length - b.length;
        }
        function i(a, b) {
            switch (b) {
                case "focused":
                    c.addClass(a, j);
                    break;
                case "blurred":
                    c.removeClass(a, j);
            }
        }
        var j = "focused",
            k = "disabled",
            l = [
                {
                    focusableDepth: function (a) {
                        return 1 * a;
                    },
                },
                { focusableGroup: angular.identity },
                { focusableName: angular.identity },
                {
                    focusableInitialFocus: function (a) {
                        return "true" === a;
                    },
                },
                {
                    focusableDisabled: function (a) {
                        return "true" === a;
                    },
                },
                { focusableNextFocusLeft: angular.identity },
                { focusableNextFocusRight: angular.identity },
                { focusableNextFocusUp: angular.identity },
                { focusableNextFocusDown: angular.identity },
            ];
        return {
            restrict: "A",
            scope: !0,
            link: function (j, m, n) {
                j.$applyAsync(function (a) {
                    var d = m.data();
                    l.forEach(function (c) {
                        var e = Object.keys(c)[0],
                            f = d[e];
                        f && g(f, b.startSymbol()) && h(f, b.endSymbol()) && m.data(e, c[e](b(f)(a)));
                    }),
                        e.$$put(m[0], a.$eval(n.focusable)),
                        f.$$setInitialFocusItem(m[0]),
                        d.focusableDisabled && c.addClass(m, k);
                }),
                    d.bindMouseEvent(m[0], function (a) {
                        var b = a.currentTarget || a.target;
                        f.$$ignoreMouseEvent !== !0 && d.getData(b).depth === f.getCurrentDepth() && f[a.data.method](b, a);
                    }),
                    d.bindFocusableEvent(m[0], function (b, c) {
                        j.$applyAsync(function (d) {
                            i(m, b.type), (a(n[n.$normalize("on-" + b.type)]) || angular.noop)(d, { $event: b, $originalEvent: c });
                        });
                    }),
                    j.$on("$destroy", function () {
                        d.unbindEvent(e.$$remove(m[0])), m.remove();
                    });
            },
        };
    },
];
NearestFocusableFinderProvider.$inject = ["FocusConstant", "focusControllerProvider"];
var caphButtonDirective = [
        "$parse",
        "focusController",
        "FocusUtil",
        function (a, b, c) {
            return {
                restrict: "E",
                transclude: !0,
                template: '<div class="{{buttonClass}}" focusable ng-transclude on-focused="focus($event, $originalEvent)" on-selected="select($event, $originalEvent)" on-blurred="blur($event, $originalEvent)"></div>',
                scope: !0,
                link: function (b, d, e) {
                    var f = !1,
                        g = d.find("div"),
                        h = b.$eval(e.toggle) || {};
                    b.buttonClass = e.buttonClass || "caph-button";
                    var i = b.$eval(e.focusOption);
                    e.$observe("buttonClass", function (a) {
                        b.buttonClass = a;
                    }),
                        i && c.setData(g, i),
                        (b.focus = function (c, d) {
                            (a(e.onFocused) || angular.noop)(b.$parent, { $event: c, $originalEvent: d });
                        }),
                        (b.blur = function (c, d) {
                            (a(e.onBlurred) || angular.noop)(b.$parent, { $event: c, $originalEvent: d });
                        }),
                        (b.select = function (c, d) {
                            h
                                ? ((f = !f), (a(e.onSelected) || angular.noop)(b.$parent, { $event: c, $selected: f, $originalEvent: d }), g.toggleClass("selected"))
                                : (a(e.onSelected) || angular.noop)(b.$parent, { $event: c, $originalEvent: d });
                        });
                },
            };
        },
    ],
    caphCheckboxDirective = [
        "$parse",
        "focusController",
        "FocusUtil",
        function (a, b, c) {
            return {
                restrict: "E",
                transclude: !0,
                template:
                    '<div class="{{checkboxClass}}"focusable ng-transclude on-focused="focus($event, $originalEvent)" on-selected="select($event, $originalEvent)" on-blurred="blur($event, $originalEvent)" ng-class="{checked : checked}">{{check}}</div>',
                scope: !0,
                link: function (b, d, e) {
                    (b.checked = b.$eval(e.checked) || !1),
                        (b.checkboxClass = e["class"] || "caph-checkbox"),
                        e.$observe("checkboxClass", function (a) {
                            b.checkboxClass = a;
                        });
                    var f = d.find("div"),
                        g = b.$eval(e.focusOption);
                    g && c.setData(f, g),
                        (b.focus = function (c, d) {
                            (a(e.onFocused) || angular.noop)(b.$parent, { $event: c, $originalEvent: d });
                        }),
                        (b.blur = function (c, d) {
                            (a(e.onBlurred) || angular.noop)(b.$parent, { $event: c, $originalEvent: d });
                        }),
                        (b.select = function (c, d) {
                            (b.checked = !b.checked), (a(e.onSelected) || angular.noop)(b.$parent, { $event: c, $checked: b.checked, $originalEvent: d });
                        });
                },
            };
        },
    ],
    caphTranscludeDirective = [
        function () {
            return {
                restrict: "A",
                link: function (a, b, c, d, e) {
                    var f = c.caphTransclude;
                    e(function (a) {
                        if (f) {
                            var c = "";
                            for (var d in a)
                                if (a[d].tagName && f.toLowerCase() === a[d].tagName.toLowerCase()) {
                                    c = a[d];
                                    break;
                                }
                            b.empty(), b.append(c);
                        } else b.empty(), b.append(a);
                    });
                },
            };
        },
    ],
    caphContextMenuItemTemplateDirective = [
        "$sce",
        "$compile",
        function (a, b) {
            return {
                restrict: "E",
                require: ["?^caphContextMenu"],
                tranclude: !0,
                link: function (c, d) {
                    c.$parent.$watch(
                        "item",
                        function (b) {
                            for (var d in b) "content" === d ? (c[d] = a.trustAsHtml(b[d] + "")) : (c[d] = b[d]);
                        },
                        !0
                    ),
                        d.replaceWith(b("<span>" + d.html() + "</span>")(c));
                },
            };
        },
    ],
    caphContextMenuArrowUpTemplateDirective = function () {
        return { restrict: "E", require: ["?^caphContextMenu"], tranclude: !0 };
    },
    caphContextMenuArrowDownTemplateDirective = function () {
        return { restrict: "E", require: ["?^caphContextMenu"], tranclude: !0 };
    },
    caphContextMenuDirective = [
        "$parse",
        "focusController",
        "FocusConstant",
        "$timeout",
        "$window",
        function (a, b, c, d, e) {
            var f = { FOCUSABLE_DEPTH: 1e4 },
                g = {
                    translateTo: function (a, b, c) {
                        a && a.css("transform", "translate3d(" + b + "px," + c + "px, 0)");
                    },
                    getSuitablePosition: function (a, b, c) {
                        var d = a.offset();
                        for (var f in b) if (d.left + b[f].x > 0 && d.left + b[f].x + a.outerWidth() < angular.element(e).width() && (c || (d.top + b[f].y > 0 && d.top + b[f].y + a.outerHeight() < angular.element(e).height()))) return b[f];
                        return b.length > 0 ? b[0] : void 0;
                    },
                    getCalculatedOffset: function (a, b) {
                        var c = a.offset();
                        return { x: b.x - c.left, y: b.y - c.top };
                    },
                };
            return {
                restrict: "E",
                template:
                    '<div class="caph-context-menu {{menuList[\'root\'].option.class}}" ng-class="{{menuList[\'root\'].option.class}}"><div class="arrow-up" ng-show="menuList[\'root\'].option.visibleItemCount > 0 && menuList[\'root\'].option.visibleItemCount < menuList[\'root\'].itemList.length" ng-click="onSelectUpButton($event)" caph-transclude="caph-context-menu-arrow-up-template"></div><ul ng-style="menuList[\'root\'].option.style" data-visible-item-count="{{menuList[\'root\'].option.visibleItemCount}}" data-scroll-count="0" data-submenu-id="root"><li ng-repeat="item in menuList[\'root\'].itemList" id="{{item.id}}" focusable data-focusable-depth="{{menuList[\'root\'].focusableDepth}}" data-focusable-initial-focus="{{$index===0}}" ng-class="{hasSubMenu:menuList[item.id], opened:(menuList[item.id] && menuList[item.id].show), disabled:item.disabled}"  data-focusable-disabled="{{item.disabled || checkItemExist(menuList[\'root\'].itemList, item)}}" on-selected="onSelectItem($event, (menuList[item.id])?item.id:false)" ng-click="onClickItem((menuList[item.id])?item.id:false)"  on-focused="focusItem($event, $originalEvent)" on-blurred="blurItem($event, $originalEvent)" caph-transclude="caph-context-menu-item-template"></li></ul><div class="arrow-down" ng-show="menuList[\'root\'].option.visibleItemCount > 0 && menuList[\'root\'].option.visibleItemCount < menuList[\'root\'].itemList.length" ng-click="onSelectDownButton($event)" caph-transclude="caph-context-menu-arrow-down-template"></div></div><div class="caph-context-menu {{submenu.option.class}}" ng-repeat="(menuId, submenu) in menuList" ng-if="menuId != \'root\'" ng-show="submenu.show" id="sub_{{menuId}}" data-submenu-id="{{menuId}}"><div class="arrow-up" ng-show="submenu.option.visibleItemCount > 0 && submenu.option.visibleItemCount < submenu.itemList.length" ng-click="onSelectUpButton($event)" caph-transclude="caph-context-menu-arrow-up-template"></div><ul ng-style="submenu.option.style" data-visible-item-count="{{submenu.option.visibleItemCount}}" data-scroll-count="0" data-submenu-id="{{menuId}}"><li ng-repeat="item in submenu.itemList" id="{{item.id}}" focusable data-focusable-depth="{{submenu.focusableDepth}}" data-focusable-initial-focus="{{$index===0}}"  data-focusable-disabled="{{item.disabled || checkItemExist(submenu.itemList, item)}}" on-selected="onSelectItem($event, (menuList[item.id])?item.id:false)" ng-click="onClickItem((menuList[item.id])?item.id:false)"  on-focused="focusItem($event, $originalEvent)" on-blurred="blurItem($event, $originalEvent)" data-previous-depth="{{subMenu.previousDepth}}" ng-class="{hasSubMenu:menuList[item.id], opened:(menuList[item.id] && menuList[item.id].show), disabled:item.disabled}" caph-transclude="caph-context-menu-item-template"></li></ul><div class="arrow-down" ng-show="submenu.option.visibleItemCount > 0 && submenu.option.visibleItemCount < submenu.itemList.length" ng-click="onSelectDownButton($event)" caph-transclude="caph-context-menu-arrow-down-template"></div></div>',
                transclude: !0,
                scope: !0,
                link: function (e, h, i) {
                    (e.focusableDepth = angular.isNumber(1 * i.focusableDepth) ? 1 * i.focusableDepth : f.FOCUSABLE_DEPTH), (e.show = !0), (e.menuList = {});
                    var j,
                        k = b.getCurrentDepth();
                    (e.checkItemExist = function (a, b) {
                        return $.inArray(b, a) < 0;
                    }),
                        (e.onSelectItem = function (b, c) {
                            var d = angular.element(b.currentTarget);
                            c ? (e.menuList[c].show ? y(c) : x(c, d)) : (a(i.onSelectItem) || angular.noop)(e.$parent, { $itemId: d.attr("id"), $event: b });
                        }),
                        (e.focusItem = function (b, c) {
                            (a(i.onFocusItem) || angular.noop)(e.$parent, { $event: b, $originalEvent: c });
                        }),
                        (e.blurItem = function (b, c) {
                            (a(i.onBlurItem) || angular.noop)(e.$parent, { $event: b, $originalEvent: c });
                        }),
                        (e.onClickItem = function (a) {
                            a &&
                                (w[a] = d(function () {
                                    y(a), delete w[a];
                                }, 700));
                        }),
                        (e.onSelectUpButton = function (a) {
                            var b = angular.element(a.currentTarget).next();
                            z(b, "up", b.data("visibleItemCount"));
                        }),
                        (e.onSelectDownButton = function (a) {
                            var b = angular.element(a.currentTarget).prev();
                            z(b, "down", b.data("visibleItemCount"));
                        });
                    var l = function (a) {
                            b.setDepth(angular.element(a.currentTarget).data("focusableDepth")), b.focus(a.currentTarget);
                        },
                        m = function (a) {
                            var b = angular.element(a.currentTarget).data("submenuId");
                            b &&
                                (w[b] = d(function () {
                                    y(b), delete w[b];
                                }, 700));
                        },
                        n = function (a) {
                            var b = angular.element(a.currentTarget).data("submenuId"),
                                c = e.menuList[b] ? e.menuList[b].parentId : void 0;
                            b && w[b] && (d.cancel(w[b]), delete w[b]), c && w[c] && (d.cancel(w[c]), delete w[c]);
                        },
                        o = function () {
                            B();
                        },
                        p = function (a) {
                            var b, d, e;
                            if (a.event.keyCode === c.DEFAULT.KEY_MAP.LEFT) {
                                var f = angular.element(a.currentFocusItem).parent().data("submenuId");
                                "root" === f ? B() : y(f);
                            } else if (a.event.keyCode === c.DEFAULT.KEY_MAP.DOWN) {
                                if (((b = angular.element(a.currentFocusItem).parent()), b.data("visibleItemCount"))) {
                                    (d = b.find("li")), (e = jQuery.inArray(a.currentFocusItem, d));
                                    var g = e - b.data("scrollCount") - b.data("visibleItemCount") + 1;
                                    e - b.data("scrollCount") >= b.data("visibleItemCount") && z(b, "down", b.data("visibleItemCount"), g);
                                }
                            } else
                                a.event.keyCode === c.DEFAULT.KEY_MAP.UP &&
                                    ((b = angular.element(a.currentFocusItem).parent()),
                                    b.data("visibleItemCount") && ((d = b.find("li")), (e = jQuery.inArray(a.currentFocusItem, d)), e - b.data("scrollCount") < 0 && z(b, "up", b.data("visibleItemCount"), b.data("scrollCount") - e)));
                        },
                        q = function () {
                            angular.element(document).off("click", o);
                        },
                        r = function () {
                            angular.isNumber(k) && (b.setDepth(k), angular.element(document).on("click", o));
                        },
                        s = function (a, b) {
                            var c = {};
                            for (var d in a) {
                                if ((a[d].parent || (a[d].parent = "root"), !a[d].id || !a[d].content)) throw new Error("Each data should have properties 'id' and 'content'");
                                if (c[a[d].id]) throw new Error("Item's id is duplicated. (id = '" + a[d].id + "')");
                                c[a[d].id] = a[d];
                            }
                            var e = {},
                                f = function (a, b) {
                                    e[a] = { focusableDepth: b + Object.keys(e).length, previousDepth: void 0, menuDepth: void 0, itemList: [], option: {}, show: !1 };
                                };
                            f("root", b);
                            for (var g in a) e[a[g].parent] || f(a[g].parent, b), e[a[g].parent].itemList.push(a[g]);
                            for (var h in e)
                                if ("root" === h) e[h].menuDepth = 0;
                                else {
                                    if (!c[h]) throw Error("Parent item is not exist for the item (id = '" + h + "')");
                                    for (var i = h, j = 1, k = e[c[i].parent].focusableDepth; c[i] && "root" !== c[i].parent; ) (i = c[i].parent), j++;
                                    (e[h].previousDepth = k), (e[h].menuDepth = j), (e[h].parentId = c[h].parent);
                                }
                            return e;
                        },
                        t = function (a) {
                            if (a) for (var b in e.menuList) a[e.menuList[b].menuDepth] && (e.menuList[b].option = a[e.menuList[b].menuDepth]);
                        };
                    e.$watch(
                        i.items,
                        function (a) {
                            (e.menuList = s(a, e.focusableDepth)), (j = e.$eval(i.menuOption) || {}), t(j);
                        },
                        !0
                    ),
                        e.$watch(
                            i.menuOption,
                            function (a) {
                                (j = a), t(j);
                            },
                            !0
                        );
                    var u;
                    e.$watch(i.position, function (a) {
                        u = a;
                        var b = h.find(".caph-context-menu:first-child");
                        if (u && angular.isNumber(u.x) && angular.isNumber(u.y)) {
                            g.translateTo(b, 0, 0);
                            var c = g.getCalculatedOffset(b, u);
                            g.translateTo(b, c.x, c.y);
                        }
                    });
                    var v = h.offset(),
                        w = {},
                        x = function (a, c) {
                            e.menuList[a].show = !0;
                            var d = angular.element(c).offset(),
                                f = e.menuList[a].option.offset || { x: 0, y: 0 },
                                h = angular.element("#sub_" + a);
                            h.removeClass("ng-hide");
                            var i = g.getSuitablePosition(
                                h,
                                [
                                    { x: angular.element(c).outerWidth() + d.left - v.left + f.x, y: d.top - v.top + f.y },
                                    { x: d.left - v.left - h.outerWidth() - f.x, y: d.top - v.top + f.y },
                                ],
                                !0
                            );
                            g.translateTo(h, i.x, i.y), b.setDepth(e.menuList[a].focusableDepth);
                            var j = angular.element("#sub_" + a + ">ul>li");
                            (e.menuList[a].option.visibleItemCount = Math.min(j.length, e.menuList[a].option.visibleItemCount)),
                                e.menuList[a].option.visibleItemCount && ((e.menuList[a].option.style.height = j.eq(0).outerHeight() * e.menuList[a].option.visibleItemCount), (e.menuList[a].option.style.overflow = "hidden"));
                        },
                        y = function (a) {
                            if (a) {
                                e.menuList[a].show = !1;
                                for (var c in e.menuList) e.menuList[c].parentId === a && (e.menuList[c].show = !1);
                                b.setDepth(e.menuList[a].previousDepth);
                            } else {
                                for (var f in e.menuList) (e.menuList[f].show = !1), w[f] && (d.cancel(w[f]), delete w[f]);
                                b.setDepth(e.menuList.root.focusableDepth);
                            }
                        },
                        z = function (a, b, c, d) {
                            var e = a.find("li");
                            (d = Math.abs(d) || 1),
                                "up" === b && a.data("scrollCount") > 0 ? a.data("scrollCount", a.data("scrollCount") - d) : "down" === b && a.data("scrollCount") < e.length - c && a.data("scrollCount", a.data("scrollCount") + d),
                                g.translateTo(e, 0, a.data("scrollCount") * e.eq(0).outerHeight() * -1);
                        };
                    e.$watch(i.show, function (a) {
                        a === !0 ? A() : B();
                    });
                    var A = function () {
                            if (!e.$eval(i.show)) {
                                var c = a(i.show);
                                c.assign(e, !0), e.$applyAsync();
                            }
                            h.removeClass("ng-hide");
                            var d = h.find(".caph-context-menu:first-child");
                            if (u && angular.isNumber(u.x) && angular.isNumber(u.y)) {
                                g.translateTo(d, 0, 0);
                                var f = g.getCalculatedOffset(d, u);
                                g.translateTo(d, f.x, f.y);
                            }
                            var j = d.find(">ul>li");
                            (e.menuList.root.option.visibleItemCount = Math.min(j.length, e.menuList.root.option.visibleItemCount)),
                                e.menuList.root.option.visibleItemCount && ((e.menuList.root.option.style.height = j.eq(0).outerHeight() * e.menuList.root.option.visibleItemCount), (e.menuList.root.option.style.overflow = "hidden")),
                                angular.isNumber(e.focusableDepth) && b.setDepth(e.focusableDepth),
                                b.addAfterKeydownHandler(p),
                                angular.element(document).on("click", o);
                            var k = h.find("div.caph-context-menu");
                            k.on("mouseover", n).on("mouseleave", m), k.find("> ul > li").on("mouseover", l), h.on("mouseover", q).on("mouseleave", r), e.$applyAsync();
                        },
                        B = function () {
                            h.addClass("ng-hide"), y(), angular.isNumber(k) && b.setDepth(k), b.removeAfterKeydownHandler(p), angular.element(document).off("click", o);
                            var c = h.find("div.caph-context-menu");
                            if ((c.off("mouseover", n).off("mouseleave", m), c.find("> ul > li").off("mouseover", l), h.off("mouseover", q).off("mouseleave", r), e.$eval(i.show))) {
                                var d = a(i.show);
                                d.assign(e, !1), e.$applyAsync();
                            }
                        };
                },
            };
        },
    ],
    caphDialogTitleDirective = function () {
        return { restrict: "E", require: "?^caphDialog", template: '<div class="title-area"><div class="title-wrapper"><div class="title" ng-transclude></div></div></div>', transclude: !0 };
    },
    caphDialogContentDirective = function () {
        return { restrict: "E", require: "?^caphDialog", transclude: !0, template: '<div class="content-area"><div class="content" ng-transclude></div></div>' };
    },
    caphDialogButtonsDirective = [
        "DialogUtil",
        "$parse",
        function (a, b) {
            return {
                restrict: "E",
                require: "^caphDialog",
                scope: !0,
                template:
                    '<div class="caph-dialog-button-area"><div class="button-wrapper" ng-if="btnTypeConfirm"><div class="caph-dialog-button" on-selected="onSelectButton(0, $event)" focusable="{depth: {{focusOption.depth}}, initialFocus: true}" data-focusable-group="{{focusOption.group}}" on-focused="focus($event)" on-blurred="blur($event)"><div class="button-text">Yes</div></div><div class="caph-dialog-button" on-selected="onSelectButton(1, $event)" focusable="{depth: {{focusOption.depth}}}" on-focused="focus($event)" data-focusable-group="{{focusOption.group}}" on-blurred="blur($event)"><div class="button-text">No</div></div></div><div class="button-wrapper" ng-if="btnTypeAlert"><div class="caph-dialog-button" on-selected="onSelectButton(0, $event)" focusable="{depth: {{focusOption.depth}}, initialFocus: true}" data-focusable-group="{{focusOption.group}}" on-focused="focus($event)" on-blurred="blur($event)"><div class="button-text">OK</div></div></div></div>',
                link: function (a, c, d, e) {
                    var f = d.buttonType;
                    f && "confirm" === f ? (a.btnTypeConfirm = !0) : f && "alert" === f && (a.btnTypeAlert = !0),
                        (a.focusOption = e.getFocusOption()),
                        (a.onSelectButton = function (c, e) {
                            (b(d.onSelectButton) || angular.noop)(a.$parent, { $buttonIndex: c, $event: e });
                        });
                },
            };
        },
    ],
    caphDialogDirective = [
        "dialogManager",
        "$parse",
        "DialogUtil",
        "DialogConstant",
        "focusController",
        "$timeout",
        function (a, b, c, d, e, f) {
            return {
                restrict: "E",
                template: '<div class="caph-dialog" ng-transclude></div>',
                transclude: !0,
                scope: !1,
                controller: [
                    "$scope",
                    "$element",
                    "$attrs",
                    function (a, b, c) {
                        var e = a.$eval(c.focusOption) || { depth: d.DEFAULT.focusableDepth };
                        (e.group = e.group || "default"),
                            (this.getFocusOption = function () {
                                return e;
                            });
                    },
                ],
                link: function (g, h, i) {
                    var j = {};
                    (j.timeout = g.$eval(i.timeout)),
                        (j.center = g.$eval(i.center)),
                        (j.position = g.$eval(i.position)),
                        (j.focusOption = g.$eval(i.focusOption) || { depth: d.DEFAULT.focusableDepth }),
                        (j.focusOption.group = j.focusOption.group || "default"),
                        (j.modal = !0);
                    var k,
                        l = a.generateDialogId();
                    a.addDialog(l, h, j);
                    var m = e.getCurrentDepth(),
                        n = h.find("> :first-child"),
                        o = function () {
                            if (!g.$eval(i.show)) {
                                var d = b(i.show);
                                d.assign(g, !0);
                            }
                            j.center === !0 ? c.moveToCenter(n) : j.position && c.translateTo(n, j.position.x, j.position.y),
                                e.setDepth(j.focusOption.depth, j.focusOption.group),
                                a.manageModal(l, !0),
                                (b(i.onOpen) || angular.noop)(g),
                                k && (f.cancel(k), (k = null)),
                                angular.isNumber(j.timeout) &&
                                    (k = f(function () {
                                        h.addClass("ng-hide"), p(), (k = null);
                                    }, j.timeout));
                        },
                        p = function () {
                            if (g.$eval(i.show)) {
                                var c = b(i.show);
                                c.assign(g, !1);
                            }
                            k && (f.cancel(k), (k = null)), angular.isNumber(m) && e.setDepth(m), a.manageModal(l, !1), (b(i.onClose) || angular.noop)(g);
                        };
                    g.$watch(i.center, function (a, b) {
                        a && b && a === !0 && c.moveToCenter(n);
                    }),
                        g.$watch(i.position, function (a, b) {
                            a && b && a && c.translateTo(n, a.x, a.y);
                        }),
                        h.addClass("caph-dialog"),
                        g.$watch(i.show, function (a) {
                            a === !0 ? (h.removeClass("ng-hide"), o()) : a === !1 && (h.addClass("ng-hide"), p());
                        });
                },
            };
        },
    ],
    dialogManagerProvider = function () {
        var a,
            b = {},
            c = 0,
            d = {
                generateDialogId: function () {
                    return c++ + "_" + new Date().getTime();
                },
                addDialog: function (a, c, d) {
                    if (this.getDialog(a)) throw new Error("Dialog ID (" + a + ") is already registered.");
                    b[a] = { element: c, option: d, isOpened: !1 };
                },
                getDialog: function (a) {
                    return b[a];
                },
                createDim: function () {
                    a || ((a = document.createElement("div")), a.classList.add("caph-dim-init"), document.getElementsByTagName("body")[0].appendChild(a));
                },
                activateDim: function () {
                    a || this.createDim(), a.classList.add("caph-dim-translucent");
                },
                deactivateDim: function () {
                    a && a.classList.remove("caph-dim-translucent");
                },
                manageModal: function (a, c) {
                    var e = this.getDialog(a);
                    if (!e) throw new Error("Dialog (" + a + ") is not registered. Is this id correct?");
                    if (((e.isOpened = c), e.option.modal)) {
                        c ? d.activateDim() : d.deactivateDim();
                        for (var f in b) f !== a && b[f].option.modal && b[f].isOpened && (b[f].element.addClass("ng-hide"), (b[f].isOpened = !1));
                    }
                },
            };
        return {
            $get: function () {
                return { generateDialogId: d.generateDialogId, addDialog: d.addDialog, getDialog: d.getDialog, manageModal: d.manageModal };
            },
        };
    },
    caphDropdownListItemTemplateDirective = [
        "$sce",
        "$compile",
        function (a, b) {
            return {
                restrict: "E",
                require: ["?^caphDropdownList"],
                tranclude: !0,
                link: function (c, d) {
                    c.$parent.$watch(
                        "item",
                        function (b) {
                            for (var d in b) "content" === d ? (c[d] = a.trustAsHtml(b[d] + "")) : (c[d] = b[d]);
                        },
                        !0
                    ),
                        d.replaceWith(b("<span>" + d.html() + "</span>")(c));
                },
            };
        },
    ],
    caphDropdownListPlaceholderTemplateDirective = [
        "$compile",
        function (a) {
            return {
                restrict: "E",
                require: "?^caphDropdownList",
                tranclude: !0,
                link: function (b, c) {
                    c.replaceWith(a("<span>" + c.html() + "</span>")(b));
                },
            };
        },
    ],
    caphDropdownArrowUpTemplateDirective = [
        function () {
            return { restrict: "E", require: "?^caphDropdownList", tranclude: !0 };
        },
    ],
    caphDropdownArrowDownTemplateDirective = [
        function () {
            return { restrict: "E", require: "?^caphDropdownList", tranclude: !0 };
        },
    ],
    caphDropdownListDirective = [
        "$parse",
        "focusController",
        "FocusConstant",
        "FocusUtil",
        "$timeout",
        function (a, b, c, d, e) {
            var f = { FOCUSABLE_DEPTH: 2e4 },
                g = {
                    translateTo: function (a, b, c) {
                        a && a.css("transform", "translate3d(" + b + "px," + c + "px, 0)");
                    },
                };
            return {
                restrict: "E",
                template:
                    '<div class="caph-dropdown-list"><div class="placeholder" ng-class="{focused: focused, selected: showList}" focusable="focusForPlaceholder" caph-transclude="caph-dropdown-list-placeholder-template"></div><div class="list-container" ng-show="showList"><div class="textbox arrow-up" ng-show="showArrowUp" ng-click="onSelectUpButton($event)"><div class="label center" caph-transclude="caph-dropdown-list-arrow-up-template"></div></div><div class="list-items" ng-style="listContainerStyle" data-scroll-count="0" data-visible-item-count="{{visibleItemCount}}"><div class="textbox" ng-repeat="item in items" focusable="focusForList" data-disabled="{{item.disabled}}" on-selected="onSelectItem($event, item.id)"><div class="label" caph-transclude="caph-dropdown-list-item-template"></div></div></div><div class="textbox arrow-down" ng-show="showArrowDown" ng-click="onSelectDownButton($event)"><div class="label center" caph-transclude="caph-dropdown-list-arrow-down-template"></div></div></div></div>',
                transclude: !0,
                scope: !0,
                link: function (d, h, i) {
                    (d.visibleItemCount = d.$eval(i.visibleItemCount) || 0),
                        (d.focusForPlaceholder = d.$eval(i.focusOption) || { depth: b.getCurrentDepth() }),
                        (d.focusForList = { depth: d.focusForPlaceholder.depth + f.FOCUSABLE_DEPTH || f.FOCUSABLE_DEPTH, group: d.focusForPlaceholder.group, disabled: !0 }),
                        (d.showList = d.focused = d.selected = d.disabled = !1);
                    var j = h.find(".placeholder"),
                        k = h.find(".list-container"),
                        l = function () {
                            d.$applyAsync(function (a) {
                                a.showList = !0;
                            }),
                                d.visibleItemCount > 0 &&
                                    d.visibleItemCount < d.items.length &&
                                    ((d.listContainerStyle = { height: d.visibleItemCount * h.find(".list-items>.textbox").outerHeight(), overflow: "hidden" }),
                                    (d.showArrowUp = h.find("caph-dropdown-list-arrow-up-template").length > 0),
                                    (d.showArrowDown = h.find("caph-dropdown-list-arrow-down-template").length > 0)),
                                b.addBeforeKeydownHandler(o),
                                b.addAfterKeydownHandler(p),
                                k.on("mouseover", r).on("mouseleave", s),
                                e(function () {
                                    h.find(".list-items>.textbox").each(function () {
                                        angular.element(this).data("disabled") !== !0 && b.enable(this);
                                    }),
                                        b.setDepth(d.focusForList.depth),
                                        angular.element(document).on("click", q);
                                }, 0);
                        },
                        m = function () {
                            h.find(".list-items>.textbox").each(function () {
                                b.disable(this);
                            }),
                                (d.showList = !1),
                                b.setDepth(d.focusForPlaceholder.depth),
                                b.removeBeforeKeydownHandler(o),
                                b.removeAfterKeydownHandler(p),
                                k.off("mouseover", r).off("mouseleave", s),
                                angular.element(document).off("click", q);
                        },
                        n = function (a, b, c, d) {
                            var e = a.find(".textbox");
                            (d = Math.abs(d) || 1),
                                "up" === b && a.data("scrollCount") > 0 ? a.data("scrollCount", a.data("scrollCount") - d) : "down" === b && a.data("scrollCount") < e.length - c && a.data("scrollCount", a.data("scrollCount") + d),
                                g.translateTo(e, 0, a.data("scrollCount") * e.eq(0).outerHeight() * -1);
                        };
                    d.$watch(
                        i.items,
                        function (a) {
                            d.items = a;
                        },
                        !0
                    );
                    var o = function (a) {
                            return a.event.keyCode === c.DEFAULT.KEY_MAP.LEFT || a.event.keyCode === c.DEFAULT.KEY_MAP.RIGHT ? (m(), !1) : void 0;
                        },
                        p = function (a) {
                            var b, d, e;
                            if (a.event.keyCode === c.DEFAULT.KEY_MAP.DOWN) {
                                if (((b = angular.element(a.currentFocusItem).parent()), b.data("visibleItemCount"))) {
                                    (d = b.find(".textbox")), (e = jQuery.inArray(a.currentFocusItem, d));
                                    var f = e - b.data("scrollCount") - b.data("visibleItemCount") + 1;
                                    e - b.data("scrollCount") >= b.data("visibleItemCount") && n(b, "down", b.data("visibleItemCount"), f);
                                }
                            } else
                                a.event.keyCode === c.DEFAULT.KEY_MAP.UP &&
                                    ((b = angular.element(a.currentFocusItem).parent()),
                                    b.data("visibleItemCount") && ((d = b.find(".textbox")), (e = jQuery.inArray(a.currentFocusItem, d)), e - b.data("scrollCount") < 0 && n(b, "up", b.data("visibleItemCount"), b.data("scrollCount") - e)));
                        },
                        q = function () {
                            m();
                        },
                        r = function () {
                            angular.element(document).off("click", q);
                        },
                        s = function () {
                            angular.element(document).on("click", q);
                        };
                    (d.onSelectItem = function (b, c) {
                        (a(i.onSelectItem) || angular.noop)(d.$parent, { $itemId: c, $event: b }), m();
                    }),
                        (d.onSelectUpButton = function (a) {
                            var b = angular.element(a.currentTarget).next();
                            n(b, "up", b.data("visibleItemCount"));
                        }),
                        (d.onSelectDownButton = function (a) {
                            var b = angular.element(a.currentTarget).prev();
                            n(b, "down", b.data("visibleItemCount"));
                        }),
                        j.on("focused", function () {
                            d.focused = !0;
                        }),
                        j.on("blurred", function () {
                            d.focused = !1;
                        }),
                        j.on("selected", function () {
                            d.showList ? m() : l();
                        });
                },
            };
        },
    ],
    caphInputDirective = [
        "$parse",
        "focusController",
        "FocusConstant",
        "FocusUtil",
        function (a, b, c, d) {
            return {
                restrict: "E",
                transclude: !0,
                template:
                    '<input type="{{inputType}}" placeholder={{placeHolder}} value={{inputValue}} maxLength={{maxLength}} class="{{inputClass}}" focusable on-focused="focus($event, $originalEvent)" on-selected="select($event, $originalEvent)" on-blurred="blur($event, $originalEvent)">',
                scope: !0,
                link: function (e, f, g) {
                    (e.inputClass = g.inputClass || "caph-input"),
                        (e.inputType = g.type || "text"),
                        (e.maxLength = e.$eval(g.maxLength) || 40),
                        (e.placeHolder = g.placeHolder || ""),
                        g.$observe("inputClass", function (a) {
                            e.inputClass = a;
                        }),
                        e.$watch("inputValue", function () {
                            a(g.value).assign(e.$parent, e.inputValue);
                        }),
                        e.$parent.$watch(g.value, function () {
                            e.inputValue = e.$eval(g.value);
                        }),
                        b.addBeforeKeydownHandler(function (a) {
                            var b = h[0],
                                d = a.currentFocusItem;
                            if (b === d) {
                                var e = b.selectionStart,
                                    f = b.value.length,
                                    g = a.event.keyCode;
                                return (g === c.DEFAULT.KEY_MAP.RIGHT && f > e) || (g === c.DEFAULT.KEY_MAP.LEFT && e > 0) ? !1 : void 0;
                            }
                        });
                    var h = f.find("> input"),
                        i = e.$eval(g.focusOption);
                    i && d.setData(h, i),
                        (e.focus = function (b, c) {
                            (a(g.onFocused) || angular.noop)(e.$parent, { $event: b, $originalEvent: c }), c && "keydown" === c.type && h.focus();
                        }),
                        (e.blur = function (b, c) {
                            (a(g.onBlurred) || angular.noop)(e.$parent, { $event: b, $originalEvent: c }), c && "keydown" === c.type && h.blur();
                        }),
                        (e.select = function (b, c) {
                            h.blur(), h.focus(), (a(g.onSelected) || angular.noop)(e.$parent, { $event: b, $originalEvent: c });
                        }),
                        h.on("change", function (b) {
                            e.$applyAsync(function (c) {
                                var d = b.target.value;
                                (c.inputValue = d), (a(g.onChanged) || angular.noop)(e.$parent, { $event: b, value: e.inputValue });
                            });
                        });
                },
            };
        },
    ],
    caphListDirective = [
        "$parse",
        "$document",
        "$window",
        "$timeout",
        "$interval",
        "VendorUtil",
        "FocusConstant",
        "FocusUtil",
        "focusController",
        "listItemLoader",
        function (a, b, c, d, e, f, g, h, i, j) {
            function k(a) {
                return a > 0 && a !== 1 / 0;
            }
            var l = ".caph-list",
                m = {
                    ROW_COUNT: 1,
                    COLUMN_COUNT: 1,
                    DIRECTION: "horizontal",
                    CONTAINER_CLASS: "caph-list-container",
                    WRAPPER_CLASS: "caph-list-wrapper",
                    DURATION: "0.5s",
                    EASE: "ease-out",
                    LOOP: !1,
                    DELAY: 100,
                    MOUSE_SCROLL_AREA_SIZE: 20,
                    PAGE_BUFFER_SIZE: 1,
                    KEY: "item",
                },
                n = [
                    { name: "containerClass", defaultValue: m.CONTAINER_CLASS },
                    { name: "wrapperClass", defaultValue: m.WRAPPER_CLASS },
                    { name: "initialized", defaultValue: !1 },
                ],
                o = i.getKeyMap();
            return {
                restrict: "E",
                transclude: !0,
                scope: !0,
                template: '<div class="{{containerClass}}" ng-show="initialized"><div class="{{wrapperClass}}" style="position: absolute"></div></div>',
                compile: function (g, p) {
                    function q(a, b) {
                        var c,
                            d,
                            e,
                            f,
                            g = B.width,
                            h = B.height,
                            i = C.width,
                            j = C.height,
                            l = h / j,
                            m = g / i;
                        if (!k(l) || !k(m)) throw new Error("The caph-list's container and template view should have their own size such as width and height.");
                        if (((c = Math.floor(l)), (d = Math.floor(m)), 1 > c || 1 > d)) throw new Error("Please check the container and template view size. The calculated row or column count is less than 1.");
                        (e = Math.ceil(l)), (f = Math.ceil(m));
                        var n = {
                            getMaxItemViewCount: function () {
                                return this.getItemViewCountPerPage() * (2 * b + 1);
                            },
                            getMaxPageIndex: function (a) {
                                return Math.floor((a - 1) / this.getItemViewCountPerPage());
                            },
                            getPageIndex: function (a) {
                                return Math.floor(a / this.getItemViewCountPerPage());
                            },
                            getStartItemIndex: function (a) {
                                return (a - b) * this.getItemViewCountPerPage();
                            },
                            getNextStartItemIndex: function (a) {
                                return (a + b + 1) * this.getItemViewCountPerPage();
                            },
                            getPosition: function (a) {
                                var b = this.getRowIndex(a),
                                    c = this.getColumnIndex(a);
                                return { top: b * j, left: c * i, rowIndex: b, columnIndex: c };
                            },
                            getNextPosition: function (a, b) {
                                var c = this.getPosition(a);
                                switch (b) {
                                    case o.LEFT:
                                        (c.left -= i), c.columnIndex--;
                                        break;
                                    case o.RIGHT:
                                        (c.left += i), c.columnIndex++;
                                        break;
                                    case o.UP:
                                        (c.top -= j), c.rowIndex--;
                                        break;
                                    case o.DOWN:
                                        (c.top += j), c.rowIndex++;
                                }
                                return c;
                            },
                        };
                        return {
                            horizontal: angular.extend({}, n, {
                                getItemViewCountPerPage: function () {
                                    return c * (d + 1);
                                },
                                getRowIndex: function (a) {
                                    return a % c;
                                },
                                getColumnIndex: function (a) {
                                    return Math.floor(a / c);
                                },
                                getTranslate: function (a, b) {
                                    return "translate3d(" + b * -i + "px, 0px, 0px)";
                                },
                                getScrollIndex: function (a, b) {
                                    return Math.max(0, this.getColumnIndex(a) - b);
                                },
                                isVisibleItem: function (a, b, c) {
                                    return b >= c && c + f > b;
                                },
                                getMouseScrollKeyCode: function (b, c) {
                                    var d = b - A.left;
                                    return a > d ? o.LEFT : d > g - a ? o.RIGHT : 0;
                                },
                                isScrollDirection: function (a) {
                                    switch (a) {
                                        case o.LEFT:
                                        case o.RIGHT:
                                            return !0;
                                    }
                                    return !1;
                                },
                                getRotateItemIndex: function (a, b, d) {
                                    return b > 0 ? a : Math.min(d - 1, this.getMaxColumnIndex(d) * c + a);
                                },
                                getMaxRowIndex: function () {
                                    return c - 1;
                                },
                                getMaxColumnIndex: function (a) {
                                    return Math.floor((a - 1) / c);
                                },
                                getScrollStartCount: function () {
                                    return d;
                                },
                                getInitialScrollCount: function (a) {
                                    return Math.min(this.getScrollStartCount(), this.getColumnIndex(a));
                                },
                                isPreviousDirection: function (a) {
                                    return a === o.LEFT;
                                },
                                isNextDirection: function (a) {
                                    return a === o.RIGHT;
                                },
                                getScrollCountVariation: function (a, b) {
                                    return this.getColumnIndex(b) - this.getColumnIndex(a);
                                },
                                isReachedStart: function (a) {
                                    return 0 === this.getColumnIndex(a);
                                },
                                isReachedEnd: function (a, b) {
                                    return this.getColumnIndex(a) === this.getMaxColumnIndex(b);
                                },
                            }),
                            vertical: angular.extend({}, n, {
                                getItemViewCountPerPage: function () {
                                    return (c + 1) * d;
                                },
                                getRowIndex: function (a) {
                                    return Math.floor(a / d);
                                },
                                getColumnIndex: function (a) {
                                    return a % d;
                                },
                                getTranslate: function (a, b) {
                                    return "translate3d(0px, " + b * -j + "px, 0px)";
                                },
                                getScrollIndex: function (a, b) {
                                    return Math.max(0, this.getRowIndex(a) - b);
                                },
                                isVisibleItem: function (a, b, c) {
                                    return a >= c && c + e > a;
                                },
                                getMouseScrollKeyCode: function (b, c) {
                                    var d = c - A.top;
                                    return a > d ? o.UP : d > h - a ? o.DOWN : 0;
                                },
                                isScrollDirection: function (a) {
                                    switch (a) {
                                        case o.UP:
                                        case o.DOWN:
                                            return !0;
                                    }
                                    return !1;
                                },
                                getRotateItemIndex: function (a, b, c) {
                                    return a > 0 ? b : Math.min(c - 1, this.getMaxRowIndex(c) * d + b);
                                },
                                getMaxRowIndex: function (a) {
                                    return Math.floor((a - 1) / d);
                                },
                                getMaxColumnIndex: function () {
                                    return d - 1;
                                },
                                getScrollStartCount: function () {
                                    return c;
                                },
                                getInitialScrollCount: function (a) {
                                    return Math.min(this.getScrollStartCount(), this.getRowIndex(a));
                                },
                                isPreviousDirection: function (a) {
                                    return a === o.UP;
                                },
                                isNextDirection: function (a) {
                                    return a === o.DOWN;
                                },
                                getScrollCountVariation: function (a, b) {
                                    return this.getRowIndex(b) - this.getRowIndex(a);
                                },
                                isReachedStart: function (a) {
                                    return 0 === this.getRowIndex(a);
                                },
                                isReachedEnd: function (a, b) {
                                    return this.getRowIndex(a) === this.getMaxRowIndex(b);
                                },
                            }),
                        };
                    }
                    function r(a, b, c) {
                        function d(a, b) {
                            var d;
                            w.append(a.addClass("ng-hide")),
                                (d = a
                                    .filter(function () {
                                        return this.nodeType === Node.ELEMENT_NODE;
                                    })
                                    .on("focused" + l, c)),
                                b.$on("$destroy", function () {
                                    d.off(l);
                                }),
                                f.push({ element: d, scope: b });
                        }
                        var e,
                            f = [];
                        for (e = 0; b > e; e++) a(d);
                        return f;
                    }
                    var s,
                        t,
                        u,
                        v,
                        w,
                        x,
                        y,
                        z,
                        A,
                        B,
                        C,
                        D = p.items,
                        E = p.itemLoader,
                        F = m.KEY;
                    if (D && E) throw new Error('The "items" and "item-loader" option cannot be used together.');
                    if (D) {
                        if (((t = D.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)\s*$/)), !t)) throw new Error('Expected expression in form of "_item_ in _array_" but got "' + D + '".');
                        (F = t[1]), (u = t[2]);
                    }
                    return (
                        (v = g.find("> :first-child")),
                        (w = v.find("> :first-child")),
                        (x = p.direction || m.DIRECTION),
                        (y = p.duration || m.DURATION),
                        (z = p.ease || m.EASE),
                        function (g, k, p, t, D) {
                            function G() {
                                (xa = s.getItemViewCountPerPage()), (ya = s.getMaxItemViewCount()), (Na = s.getScrollStartCount());
                            }
                            function H(a) {
                                (za = a), (Aa = a.length);
                            }
                            function I() {
                                return { width: v.width(), height: v.height() };
                            }
                            function J() {
                                var a, b;
                                return (
                                    D(function (b, c) {
                                        (a = { element: b, scope: c }), ha(a), w.append(a.element);
                                    }),
                                    (b = w.find("> :first-child")),
                                    g.$applyAsync(function () {
                                        a.scope.$destroy(), a.element.remove();
                                    }),
                                    { width: b.outerWidth(!0), height: b.outerHeight(!0) }
                                );
                            }
                            function K(a) {
                                Aa > Ra &&
                                    ((A = k.offset()),
                                    (Ea = s.getMaxPageIndex(Aa)),
                                    (Oa = s.getInitialScrollCount(Ra)),
                                    (wa = r(D, ya, function (a, b) {
                                        var c, d, e, f;
                                        b &&
                                            ((c = b.type),
                                            (d = jQuery(a.currentTarget).data()),
                                            (e = d.listItemIndex),
                                            Ra !== e && ("mouseover" === c || "keydown" === c) && ((f = s.getPageIndex(e)), qa(e), la(Da, f), U(e, d.listItemRowIndex, d.listItemColumnIndex, f), "keydown" === c && T(e)),
                                            ea(e));
                                    })),
                                    U(Ra),
                                    N(s.getStartItemIndex(Da), s.getNextStartItemIndex(Da), a),
                                    T(Ra),
                                    (g.initialized = !0));
                            }
                            function L() {
                                return (ya + (Pa - 1)) % ya;
                            }
                            function M() {
                                return (Pa + 1) % ya;
                            }
                            function N(a, b, c) {
                                for (var d; b > a && ((d = wa[Pa]), Q(d, F, Qa.get(a), a++, Qa.getTotalCount()), (Pa = M()), (c || angular.noop)(d) !== !1); );
                            }
                            function O(a, b, c) {
                                for (var d; a > b; )
                                    if (((Pa = L()), (d = wa[Pa]), (c || angular.noop)(d, a--) === !1)) {
                                        Pa = M();
                                        break;
                                    }
                            }
                            function P(a, b, c) {
                                O(a, b, function (a, b) {
                                    return Q(a, F, Qa.get(b), b, Qa.getTotalCount()), (c || angular.noop)(a);
                                });
                            }
                            function Q(a, b, c, d, e) {
                                (c = d > -1 && e > d ? c : null), R(a.scope, b, c, d, e), S(a.element, d, s.getPosition(d), c), c && ha(a);
                            }
                            function R(a, b, c, d, e) {
                                (a[b] = c || {}), (a.$index = d), (a.$first = 0 === d), (a.$last = d === e - 1), (a.$odd = !(a.$even = 0 === (1 & d)));
                            }
                            function S(a, b, c, d) {
                                a.css({ position: "absolute", transform: "translate3d(" + c.left + "px, " + c.top + "px, 0px)" }).data({
                                    listItem: !0,
                                    listItemIndex: b,
                                    listItemRowIndex: c.rowIndex,
                                    listItemColumnIndex: c.columnIndex,
                                    isVisibleListItem: function (a, b) {
                                        return s.isVisibleItem(a, b, Ma);
                                    },
                                }),
                                    d ? a.removeClass("ng-hide") : a.addClass("ng-hide");
                            }
                            function T(a) {
                                var b;
                                0 === Oa ? (Ma = s.getScrollIndex(a, Oa)) : Oa === Na && (Ma = s.getScrollIndex(a, Oa - 1)),
                                    va || (va = s.getTranslate(a, Ma)),
                                    (b = s.getTranslate(a, Ma)),
                                    b !== va && (w.css("transform", b), (va = b), Ja || ((Ja = !0), ga("on-scroll-start")));
                            }
                            function U(a, b, c, d) {
                                (Ra = a), (Ba = b || s.getRowIndex(a)), (Ca = c || s.getColumnIndex(a)), (Da = d || s.getPageIndex(a));
                            }
                            function V(a) {
                                var b,
                                    c = -1;
                                (Ea = s.getMaxPageIndex(Aa)),
                                    H(a),
                                    Da >= Ea - 1 &&
                                        ((b = s.getNextStartItemIndex(Da)),
                                        O(b - 1, -1, function (a, b) {
                                            return a.element.hasClass("ng-hide") ? void 0 : ((c = b), !1);
                                        }),
                                        N(c + 1, b));
                            }
                            function W(a) {
                                var b,
                                    c,
                                    d,
                                    e = !1;
                                H(a),
                                    (b = Aa - 1),
                                    (Ea = s.getMaxPageIndex(Aa)),
                                    Da > Ea
                                        ? (N(s.getStartItemIndex(Ea), s.getNextStartItemIndex(Ea), function (a) {
                                              var b;
                                              c || ((b = a.element), b.data().listItemIndex + 1 === Aa && (c = b));
                                          }),
                                          qa(b),
                                          U(b),
                                          T(b),
                                          Y(i.getCurrentFocusItem()) && i.focus(c))
                                        : Da >= Ea - 1 &&
                                          ((d = s.getNextStartItemIndex(Da)),
                                          O(d - 1, -1, function (a, d) {
                                              var f = a.element;
                                              return f.hasClass("focused") && ((e = !0), d !== b && i.blur(f)), d === b ? ((c = f), !1) : void 0;
                                          }),
                                          N(b + 1, d),
                                          Ra > b ? (qa(b), U(b), T(b), e && i.focus(c), ea(b)) : s.isReachedEnd(Ra, Aa) && ga("on-reach-end"));
                            }
                            function X() {
                                var a = Y(i.getCurrentFocusItem());
                                a && i.blur(),
                                    na(),
                                    K(function (b) {
                                        var c = b.element;
                                        a &&
                                            Ra === c.data().listItemIndex &&
                                            d(function () {
                                                i.focus(c);
                                            });
                                    });
                            }
                            function Y(a) {
                                return a ? ((a = jQuery(a)), a.data().listItem && w[0] === a.parent()[0]) : !1;
                            }
                            function Z() {
                                return Ka;
                            }
                            function $() {
                                (Ka = !0), ca();
                            }
                            function _() {
                                ta = d(function () {
                                    Ka = !1;
                                }, La);
                            }
                            function aa() {
                                return ua;
                            }
                            function ba(a) {
                                aa() ||
                                    (ua = e(function () {
                                        var b = Ra;
                                        Z() || Qa.isLoading() || (ra(), da(s.getNextPosition(Ra, a), !0), b !== Ra && fa(Ra));
                                    }, La));
                            }
                            function ca() {
                                aa() && (e.cancel(ua), (ua = null));
                            }
                            function da(a, b) {
                                var c = ka(a);
                                if (c) {
                                    if (c.index === Ra) return !1;
                                    if (
                                        (U(c.index, c.rowIndex, c.columnIndex, c.pageIndex),
                                        T(c.index),
                                        Qa.isNeededForPrevious(Ra)
                                            ? (Qa.setOffset(Qa.getOffset() - 3 * Qa.getLoadingCount()),
                                              Qa.request(function (a, b) {
                                                  Qa.setOffset(Qa.getOffset() + 2 * Qa.getLoadingCount()), Qa.prepend(a);
                                              }))
                                            : Qa.isNeededForNext(Ra) &&
                                              Qa.request(function (a, b) {
                                                  Qa.append(a), Qa.setOffset(Qa.getOffset() + Qa.getLoadingCount());
                                              }),
                                        (b || c.force) && c.element)
                                    )
                                        return i.focus(c.element), !1;
                                } else {
                                    if (Ja) return !1;
                                    Ha &&
                                        !b &&
                                        (na(),
                                        (Ra = s.getRotateItemIndex(Ba, Ca, Aa)),
                                        K(function (a) {
                                            var b = a.element;
                                            Ra === b.data().listItemIndex &&
                                                d(function () {
                                                    i.focus(b);
                                                });
                                        }));
                                }
                            }
                            function ea(a) {
                                ga("on-focus-item-view"), fa(a);
                            }
                            function fa(a) {
                                s.isReachedStart(a) ? ga("on-reach-start") : s.isReachedEnd(a, Aa) && ga("on-reach-end");
                            }
                            function ga(b) {
                                g.$applyAsync(function (c) {
                                    (a(p[p.$normalize(b)]) || angular.noop)(c, { $context: { itemIndex: Ra, rowIndex: Ba, columnIndex: Ca, pageIndex: Da, maxPageIndex: Ea, itemCount: Aa, itemViewCountPerPage: xa } });
                                });
                            }
                            function ha(b) {
                                (a(p[p.$normalize("on-decorate-item-view")]) || angular.noop)(g, { $itemView: b });
                            }
                            function ia(a) {
                                return 0 > a || s.getMaxRowIndex(Aa) < a;
                            }
                            function ja(a) {
                                return 0 > a || s.getMaxColumnIndex(Aa) < a;
                            }
                            function ka(a) {
                                var b,
                                    c,
                                    d,
                                    e,
                                    f,
                                    g,
                                    h,
                                    i,
                                    j = Aa - 1;
                                return (
                                    (b = a.rowIndex),
                                    (c = a.columnIndex),
                                    ia(b) || ja(c)
                                        ? i
                                        : (wa.forEach(function (a) {
                                              (d = a.element),
                                                  (h = d.data()),
                                                  h.listItemIndex === j && (g = d),
                                                  b === h.listItemRowIndex &&
                                                      c === h.listItemColumnIndex &&
                                                      (d.hasClass("ng-hide")
                                                          ? ((e = j), (i = { rowIndex: s.getRowIndex(e), columnIndex: s.getColumnIndex(e), force: !0 }))
                                                          : ((e = h.listItemIndex), (i = { rowIndex: b, columnIndex: c, element: d })),
                                                      (f = s.getPageIndex(e)),
                                                      la(Da, f),
                                                      (i.index = e),
                                                      (i.pageIndex = f));
                                          }),
                                          i.force && (i.element = g),
                                          i)
                                );
                            }
                            function la(a, b) {
                                var c;
                                b > a ? ((c = (b + Ia) * xa), N(c, c + xa)) : a > b && ((c = (b - Ia + 1) * xa - 1), P(c, c - xa));
                            }
                            function ma() {
                                var a = I(),
                                    b = J();
                                (B.width !== a.width || B.height !== a.height || C.width !== b.width || C.height !== b.height) &&
                                    g.$applyAsync(function () {
                                        $(), (B = a), (C = b), (s = q(Ga, Ia)[x]), G(), X(), _();
                                    });
                            }
                            function na() {
                                wa.forEach(function (a) {
                                    a.scope.$destroy(), a.element.remove();
                                }),
                                    (wa = null);
                            }
                            function oa(a) {
                                b.trigger({ type: "keydown", keyCode: a });
                            }
                            function pa(a) {
                                s.isPreviousDirection(a) && Oa > 0 ? Oa-- : s.isNextDirection(a) && Na > Oa && Oa++;
                            }
                            function qa(a) {
                                Oa = Math.max(0, Math.min(Na, Oa + s.getScrollCountVariation(Ra, a)));
                            }
                            function ra() {
                                1 === Oa ? (Oa = 0) : Oa === Na - 1 && (Oa = Na);
                            }
                            function sa(a) {
                                var b,
                                    c = a.event.keyCode;
                                if (Z() || Qa.isLoading()) return !1;
                                if (Y(a.currentFocusItem) && s.isScrollDirection(c)) return $(), pa(c), (b = da(s.getNextPosition(Ra, c))), _(), b;
                                if (Ja) {
                                    if (h.isVisible(w)) return !1;
                                    (Ja = !1), console.warn("The list is hidden before finishing scroll animation.");
                                }
                            }
                            var ta,
                                ua,
                                va,
                                wa,
                                xa,
                                ya,
                                za,
                                Aa,
                                Ba,
                                Ca,
                                Da,
                                Ea,
                                Fa = g.$eval(p.initialItemIndex) || 0,
                                Ga = g.$eval(p.mouseScrollAreaSize) || m.MOUSE_SCROLL_AREA_SIZE,
                                Ha = g.$eval(p.loop) || m.LOOP,
                                Ia = g.$eval(p.pageBufferSize) || m.PAGE_BUFFER_SIZE,
                                Ja = !1,
                                Ka = !1,
                                La = g.$eval(p.delay) || m.DELAY,
                                Ma = 0,
                                Na = 0,
                                Oa = 0,
                                Pa = 0,
                                Qa = {
                                    getTotalCount: function () {
                                        return Aa;
                                    },
                                    get: function (a) {
                                        return za[a];
                                    },
                                    isNeededForPrevious: angular.noop,
                                    isNeededForNext: angular.noop,
                                    isLoading: angular.noop,
                                    request: angular.noop,
                                    prepend: angular.noop,
                                    append: angular.noop,
                                },
                                Ra = Fa;
                            if (E && Ha) throw new Error('Cannot use continuous circular loop mode when using "item-loader".');
                            i.addBeforeKeydownHandler(sa),
                                n.forEach(function (a) {
                                    g[a.name] = p[a.name] || a.defaultValue;
                                }),
                                u &&
                                    g.$watchCollection(u, function (a) {
                                        var b = a.length;
                                        $(), g.initialized ? (b > Aa ? V(a) : Aa > b ? W(a) : H(a)) : b > 0 && (H(a), (B = I()), (C = J()), (s = q(Ga, Ia)[x]), G(), K()), _();
                                    }),
                                k
                                    .on("moveleft" + l, function (a) {
                                        oa(o.LEFT), a.stopPropagation();
                                    })
                                    .on("moveright" + l, function (a) {
                                        oa(o.RIGHT), a.stopPropagation();
                                    })
                                    .on("moveup" + l, function (a) {
                                        oa(o.UP), a.stopPropagation();
                                    })
                                    .on("movedown" + l, function (a) {
                                        oa(o.DOWN), a.stopPropagation();
                                    })
                                    .on("reload" + l, function (a) {
                                        g.$applyAsync(function () {
                                            $(), X(), _();
                                        }),
                                            a.stopPropagation();
                                    })
                                    .on("resize" + l, ma),
                                v
                                    .on("mousemove" + l, function (a) {
                                        var b = s.getMouseScrollKeyCode(a.clientX, a.clientY);
                                        0 !== b ? ba(b) : ca();
                                    })
                                    .on("mouseout" + l, function (a) {
                                        Y(a.relatedTarget) || (ca(), i.blur());
                                    }),
                                w.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend", function () {
                                    (Ja = !1), ga("on-scroll-finish");
                                }),
                                jQuery(c).on("resize" + l, ma),
                                d(function () {
                                    if ((w.css("transition", [f.getVendorStyle("transform"), y, z].join(" ")), E)) {
                                        if (((Qa = j.get(E)), !Qa)) throw new Error("No such item loader: " + E);
                                        (B = I()),
                                            (C = J()),
                                            (s = q(Ga, Ia)[x]),
                                            G(),
                                            Qa.setLoadingCount(ya + xa),
                                            Qa.setThresholdCount(xa * Ia + xa),
                                            Qa.initialize(function (a, b, c, d) {
                                                $(), (Aa = b), K(), _();
                                            });
                                    }
                                }),
                                g.$on("$destroy", function () {
                                    (Ja = !1), (Ka = !1), na(), v.off(l), k.off(l), jQuery(c).off(l), i.removeBeforeKeydownHandler(sa);
                                });
                        }
                    );
                },
            };
        },
    ];
ListItemLoaderProvider.$inject = ["focusControllerProvider"];
var caphRadioButtonDirective = [
        "radiobuttonManager",
        "$parse",
        "focusController",
        "FocusUtil",
        function (a, b, c, d) {
            return {
                restrict: "E",
                transclude: !0,
                template: '<div ng-class="radiobuttonClass" focusable ng-transclude on-focused="focus($event, $originalEvent)" on-selected="select($event, $originalEvent)" on-blurred="blur($event, $originalEvent)"></div>',
                scope: !0,
                link: function (c, e, f) {
                    c.radiobuttonClass = f.radiobuttonclass || "caph-radiobutton";
                    var g = c.$eval(f.focusOption),
                        h = f.group || "default",
                        i = e.find("> div");
                    a.addRadio(i, h),
                        void 0 !== f.selected && a.selectRadio(i, h),
                        g && d.setData(i, g),
                        f.$observe("radiobuttonClass", function (a) {
                            c.radiobuttonClass = a;
                        }),
                        (c.focus = function (a, d) {
                            (b(f.onFocused) || angular.noop)(c.$parent, { $event: a, $originalEvent: d });
                        }),
                        (c.blur = function (a, d) {
                            (b(f.onBlurred) || angular.noop)(c.$parent, { $event: a, $originalEvent: d });
                        }),
                        (c.select = function (d, e) {
                            a.selectRadio(i, h), (b(f.onSelected) || angular.noop)(c.$parent, { $event: d, $originalEvent: e });
                        });
                },
            };
        },
    ],
    radiobuttonManagerProvider = function () {
        var a = {},
            b = {
                addRadio: function (b, c) {
                    (a[c] = a[c] || []), a[c].push(b);
                },
                selectRadio: function (b, c) {
                    for (var d in a[c]) void 0 !== a[c][d].parent().attr("selected") && (a[c][d].removeClass("selected"), a[c][d].parent().removeAttr("selected"));
                    void 0 === b.parent().attr("selected") && b.parent().attr("selected", ""), b.addClass("selected");
                },
            };
        return {
            $get: function () {
                return { addRadio: b.addRadio, selectRadio: b.selectRadio };
            },
        };
    };
angular.module(
    "caph.ui",
    ["caph.focus"],
    [
        "$provide",
        "$compileProvider",
        "nearestFocusableFinderProvider",
        function (a, b, c) {
            function d(a) {
                var b,
                    c,
                    d = a.charAt(0).toUpperCase() + a.substr(1);
                return null === g
                    ? ((b = f.style),
                      a in b
                          ? ((g = ""), a)
                          : (e.some(function (a) {
                                return (c = a + d), c in b ? ((g = a), !0) : void 0;
                            }),
                            (f = null),
                            c))
                    : g
                    ? g + d
                    : a;
            }
            var e = ["Webkit", "Moz", "O", "ms"],
                f = document.createElement("div"),
                g = null,
                h = { DEFAULT: { focusableDepth: 9999 } },
                i = {
                    translateTo: function (a, b, c) {
                        a && a.css("transform", "translate3d(" + b + "px," + c + "px, 0)");
                    },
                    moveToCenter: function (a) {
                        if (a) {
                            var b = window.innerWidth / 2 - a[0].clientWidth / 2,
                                c = window.innerHeight / 2 - a[0].clientHeight / 2;
                            0 > c && (c = 0), 0 > b && (b = 0), (b -= 1 * a[0].offsetLeft), this.translateTo(a, b, c);
                        }
                    },
                },
                j = {
                    getVendorProperty: d,
                    getVendorStyle: function (a) {
                        var b = d(a);
                        return b !== a
                            ? "-" +
                                  b.replace(/[A-Z]/g, function (a, b) {
                                      var c = a.toLowerCase();
                                      return 0 === b ? c : "-" + c;
                                  })
                            : b;
                    },
                };
            ["transform", "transition"].forEach(function (a) {
                var b = d(a);
                b !== a &&
                    (jQuery.cssHooks[a] = {
                        get: function (a) {
                            return jQuery.css(a, b);
                        },
                        set: function (a, c) {
                            a.style[b] = c;
                        },
                    });
            }),
                a.constant("DialogConstant", h),
                a.constant("DialogUtil", i),
                a.constant("VendorUtil", j),
                a.provider({ dialogManager: dialogManagerProvider, radiobuttonManager: radiobuttonManagerProvider, listItemLoader: ListItemLoaderProvider }),
                b.directive({
                    caphDialogTitle: caphDialogTitleDirective,
                    caphDialogContent: caphDialogContentDirective,
                    caphDialogButtons: caphDialogButtonsDirective,
                    caphDialog: caphDialogDirective,
                    caphList: caphListDirective,
                    caphButton: caphButtonDirective,
                    caphTransclude: caphTranscludeDirective,
                    caphContextMenuItemTemplate: caphContextMenuItemTemplateDirective,
                    caphContextMenuArrowUpTemplate: caphContextMenuArrowUpTemplateDirective,
                    caphContextMenuArrowDownTemplate: caphContextMenuArrowDownTemplateDirective,
                    caphContextMenu: caphContextMenuDirective,
                    caphCheckbox: caphCheckboxDirective,
                    caphRadiobutton: caphRadioButtonDirective,
                    caphDropdownListItemTemplate: caphDropdownListItemTemplateDirective,
                    caphDropdownListPlaceholderTemplate: caphDropdownListPlaceholderTemplateDirective,
                    caphDropdownListArrowUpTemplate: caphDropdownArrowUpTemplateDirective,
                    caphDropdownListArrowDownTemplate: caphDropdownArrowDownTemplateDirective,
                    caphDropdownList: caphDropdownListDirective,
                    caphInput: caphInputDirective,
                }),
                c.addBeforeDistanceCalculationHandler(function (a) {
                    var b = jQuery(a).data() || {};
                    return b.listItem !== !0 || b.isVisibleListItem(b.listItemRowIndex, b.listItemColumnIndex) ? void 0 : !1;
                });
        },
    ]
);
